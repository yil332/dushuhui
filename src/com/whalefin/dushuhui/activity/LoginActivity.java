package com.whalefin.dushuhui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.util.PreferencesUtils;

public class LoginActivity extends MyActionBarActivity implements View.OnClickListener {

    // UI组件
    private TextView login_by_phone, login_as_tourist;
    private ImageView image_dark_null;
    private LinearLayout login_type_content;
    private RelativeLayout layout_login_edit, layout_content;
    private EditText et_login_username, et_login_password;
    private boolean login_inProcess = false;

    public LoginActivity() {
        super(R.string.login);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);
        // 登录框
        et_login_username = (EditText) findViewById(R.id.et_login_username);
        et_login_password = (EditText) findViewById(R.id.et_login_password);
        login_by_phone = (TextView) findViewById(R.id.login_by_phone);
        login_as_tourist = (TextView) findViewById(R.id.login_as_tourist);
        login_type_content = (LinearLayout) findViewById(R.id.login_type_content);
        layout_login_edit = (RelativeLayout) findViewById(R.id.layout_login_edit);
        layout_content = (RelativeLayout) findViewById(R.id.layout_content);
        // 隐藏图层
        image_dark_null = (ImageView) findViewById(R.id.image_dark_null);
        login_by_phone.setOnClickListener(this);
        login_as_tourist.setOnClickListener(this);
        image_dark_null.setOnClickListener(this);

        String username = PreferencesUtils.getString(this, "username");
        if (username != null && username.length() > 1) {
            et_login_username.setText(username);
        }

        setActionBarUpButton(0, R.string.back);
        getActionBarUpButton().setOnClickListener(this);

    }


    // onclick切换tab
    public void onLoginClick(boolean isStart) {
        TranslateAnimation translateAnimation = null;
        AlphaAnimation alphaAnimation = null;
        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation loginAlphaAnimation = null;
        TranslateAnimation loginTranslateAnimation = null;

        int isVISIBLE = View.INVISIBLE;
        int allWidth = login_type_content.getWidth();
        int allHeight = layout_content.getHeight();
        int moveWidth = allWidth / 4;
        int moveHeight = (int) (allHeight - layout_login_edit.getHeight() - login_type_content
                .getHeight());
        if (isStart) {
            loginAlphaAnimation = new AlphaAnimation(0, 1);
            loginTranslateAnimation = new TranslateAnimation(0, 0,
                    moveHeight, 0);
            alphaAnimation = new AlphaAnimation(1, 0.1f);
            translateAnimation = new TranslateAnimation(0, moveWidth, 0, 0);
            isVISIBLE = View.INVISIBLE;
            image_dark_null.setVisibility(View.VISIBLE);
        } else {
            loginAlphaAnimation = new AlphaAnimation(1, 0);
            alphaAnimation = new AlphaAnimation(0.1f, 1);
            translateAnimation = new TranslateAnimation(moveWidth, 0, 0, 0);
            loginTranslateAnimation = new TranslateAnimation(0, 0, 0,
                    moveHeight);
            isVISIBLE = View.VISIBLE;
            image_dark_null.setVisibility(View.GONE);
        }

        alphaAnimation.setFillAfter(true);
        alphaAnimation.setDuration(200);
        translateAnimation.setFillAfter(true);
        translateAnimation.setDuration(200);
        animationSet.setDuration(200);
        animationSet.addAnimation(loginAlphaAnimation);
        animationSet.addAnimation(loginTranslateAnimation);
        animationSet.setFillAfter(true);

        login_by_phone.startAnimation(translateAnimation);
        login_as_tourist.startAnimation(alphaAnimation);
        login_as_tourist.setVisibility(isVISIBLE);
        layout_login_edit.startAnimation(animationSet);
        if (isStart) {
            layout_login_edit.setVisibility(View.VISIBLE);
        } else {
            layout_login_edit.setVisibility(View.INVISIBLE);
        }
    }

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1) {
                case 1:
                    login_inProcess = false;
                    if (msg.obj != null && msg.obj.toString().length() < 1) {
                        Toast.makeText(LoginActivity.this, "登录失败！", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        PreferencesUtils.putString(LoginActivity.this, "username", et_login_username.getText().toString());
                        PreferencesUtils.putString(LoginActivity.this, "password", et_login_password.getText().toString());
                        ApplicationParam.isLogin = true;
                        Intent intent = new Intent();
                        intent.putExtra("type", "member");
                        LoginActivity.this.setResult(RESULT_OK, intent);
                        LoginActivity.this.finish();
                    }
                    break;
            }
        }
    };

    Runnable login = new Runnable() {
        public void run() {
            login_inProcess = true;
            Message msg = handler.obtainMessage();
            String username = et_login_username.getText().toString();
            String password = et_login_password.getText().toString();
            String loginResult = "";
            try {
                Log.e("yxw", "logining");
                loginResult = NetworkUtil.login(username, password);
                ApplicationParam.token = loginResult;
                if (NetworkUtil.testVIP(username).equals("1")) {
                    ApplicationParam.VIP = true;
                } else {
                    ApplicationParam.VIP = false;
                }
            } catch (Exception e) {
                for (int i = 0; i < e.getStackTrace().length; i++) {
                    Log.e("yxw", e.toString());
                }
                e.printStackTrace();
            }
//			Log.e("yxw", loginResult);
            msg.arg1 = 1;
            msg.obj = loginResult;
            handler.sendMessage(msg);
        }
    };


    public void btn_login(View v) {
        if (!login_inProcess) {
            if (et_login_username.getText().toString().length() < 1) {
                Toast.makeText(LoginActivity.this, "用户名不能为空!", Toast.LENGTH_SHORT)
                        .show();
            } else if (et_login_password.getText().toString().length() < 1) {
                Toast.makeText(LoginActivity.this, "密码不能为空!", Toast.LENGTH_SHORT)
                        .show();
            } else {
                new Thread(login).start();
            }
        } else {
            Toast.makeText(LoginActivity.this, "登陆中,请稍候", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public void btn_regist(View v) {
        Intent intent = new Intent(LoginActivity.this, Register.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_by_phone:
                onLoginClick(true);
                break;
            case R.id.login_as_tourist:
                Intent intent = new Intent();
                intent.putExtra("type", "tourist");
                LoginActivity.this.setResult(RESULT_OK, intent);
                LoginActivity.this.finish();
                break;
            case R.id.image_dark_null:
                onLoginClick(false);
                break;
            case R.id.ActionBarUpButton:
                finish();
                break;
        }

    }
}
