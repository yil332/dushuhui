package com.whalefin.dushuhui.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.adaptor.ImageAdapter;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.domain.BookInfo;
import com.whalefin.dushuhui.domain.BookItems;
import com.whalefin.dushuhui.util.HtmlUtil;


/**
 * Created by tongwanglin on 14-4-3.
 */
public class BookListActivity extends MyActionBarActivity implements View.OnClickListener{

	//参数
	private String url="";
	
	//UI组件
	private ListView listview;
	
	//适配器
	private ImageAdapter bookAdapter;
	
	//数据
	private List<Map<String, Object>> data;
	private List<BookItems> booklist;
	private String Category;
	
	//监听
	private OnItemClickListener itemClickListener =new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			Intent intent=new Intent(BookListActivity.this, BookDetail.class);
			intent.putExtra("url", booklist.get(0).getBookInfoList().get((int)arg2).getUrl());
			intent.putExtra("bookCategory", Category);
			intent.putExtra("book-id", booklist.get(0).getBookInfoList().get((int)arg2).getBookId());
			Log.v("checkurl2",booklist.get(0).getBookInfoList().get((int)arg2).getUrl()+"checkurl");
			startActivity(intent);
		}
	};
    public BookListActivity() {
        super(R.string.book_list);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url=this.getIntent().getStringExtra("url");
        Category = this.getIntent().getStringExtra("thisCategory");
        setContentView(R.layout.layout_book_list);
        listview=(ListView) this.findViewById(R.id.listview);
        listview.setOnItemClickListener(itemClickListener);
        getBookListRequest();
        setActionBarUpButton(0,R.string.back);
        getActionBarUpButton().setOnClickListener(this);
    }

    private void initBookListView(Document doc){
    	booklist = new ArrayList<BookItems>();
    	data=new ArrayList<Map<String,Object>>();
    	
		BookItems CategoryGroup = new BookItems();
		List<BookInfo> category = new ArrayList<BookInfo>();

		Elements ele = doc.select(".book-row");
		for (int i = 0; i < ele.size() ; i++) {
			if(ele.get(i).getElementsByClass("title")!=null&&!"".equals(ele.get(i).getElementsByClass("title").text().trim())){
				Map<String,Object> map=new HashMap<String, Object>();
				BookInfo bookInfo = new BookInfo();
				bookInfo.setName(ele.get(i).getElementsByClass("title").text());
				bookInfo.setAuthor(ele.get(i).getElementsByClass("author").text());
				bookInfo.setContent(ele.get(i).getElementsByClass("desc").text());
				bookInfo.setUrl(ele.get(i).getElementsByClass("clickable-box").attr("href"));
				bookInfo.setImage(ele.get(i).getElementsByTag("img").attr("data-src"));
				bookInfo.setBookId(ele.get(i).attr("book-id"));
				Log.v("ID",bookInfo.getBookId());
				category.add(bookInfo);
				map.put("image", bookInfo.getImage());
				map.put("name", bookInfo.getName());
				map.put("author", bookInfo.getAuthor());
				map.put("RecReason", bookInfo.getContent());
				data.add(map);
			}
			
		}
		CategoryGroup.setBookInfoList(category);
		booklist.add(CategoryGroup);
		
		bookAdapter=new ImageAdapter(this, data, R.layout.list_item_booksrecomend_child, 
				new String[]{"image","name","author","RecReason"},
				new int[]{R.id.image,R.id.name,R.id.author,R.id.RecReason});
		listview.setAdapter(bookAdapter);
		
//		Category=doc.select(".category-title").first().text();
		
    }
    private void getBookListRequest(){
    	try {
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader("APPCODE", "mobile_reading");
			client.get(HtmlUtil.urlBase+url, new AsyncHttpResponseHandler() {

				@Override
				public void onSuccess(int statusCode, String content) {
					// TODO Auto-generated method stub
					super.onSuccess(statusCode, content);
					
					Document doc = Jsoup.parse(content);
					initBookListView(doc);
				}

			});

		} catch (Exception e) {
			Log.e("yxw", "getRequestCategoryinfo");
		}

    }
    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.ActionBarUpButton){
            this.finish();
        }
    }
}