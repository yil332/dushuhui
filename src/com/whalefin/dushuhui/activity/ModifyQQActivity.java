package com.whalefin.dushuhui.activity;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.util.PreferencesUtils;

public class ModifyQQActivity extends MyActionBarActivity implements OnClickListener {
    private EditText et;

    private String userName;
    private String passWord;

    private String qq;

    public ModifyQQActivity() {
        super(R.string.modify_qq);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        qq = getIntent().getStringExtra("qq");

        setContentView(R.layout.layout_modifyqq);
        et = (EditText) findViewById(R.id.modify_qq_value);
        et.setText(qq);
        setActionBarUpButton(0, R.string.back);

        getActionBarUpButton().setOnClickListener(this);
        getActionBarRightButton().setVisibility(View.VISIBLE);
        setActionBarRightpButton(0, R.string.save);
        getActionBarRightButton().setOnClickListener(this);

        userName = PreferencesUtils.getString(this, "username");
        passWord = PreferencesUtils.getString(this, "password");
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.ActionBarUpButton:
                this.finish();
                break;

            case R.id.ActionBarRightButton:
                new Thread(modify).start();
                break;
        }

    }

    Runnable modify = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            String code = "";
            Message msg = handler.obtainMessage();
            try {
                ApplicationParam.token = NetworkUtil.login(userName, passWord);
                code = NetworkUtil.modifyQQ(et.getText().toString());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = code;
            handler.sendMessage(msg);
        }

    };
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.arg1) {
                case 1:
                    String returnCode = msg.obj.toString();
                    if (returnCode.equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }

    };

}
