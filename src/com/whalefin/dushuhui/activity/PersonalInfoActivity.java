package com.whalefin.dushuhui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.value.ebook.protobuf.MemberUserInfoProtos.MemberUserInfo;
import com.value.ebook.protobuf.ReturnProtos.Return;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.util.PreferencesUtils;

public class PersonalInfoActivity extends MyActionBarActivity implements
        OnClickListener {
    private MemberUserInfo mui;
    private String username;
    private String password;
    private String password_new;
    private TextView person_username;
    private TextView person_nickname;
    private TextView person_qq;
    private TextView person_email;
    private TextView person_vip;
    private ImageView person_image;
    private Button change_password;
    private Button change_password_confirm;
    private Button change_password_cancel;
    private EditText new_password;
    private EditText new_password_confirm;
    private EditText old_password;

    private RelativeLayout edit;
    private RelativeLayout info;

    private Return myReturn;


    private boolean isChange;

    public PersonalInfoActivity() {
        super(R.string.setting_change_password);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setActionBarUpButton(0, R.string.back);
        setContentView(R.layout.layout_personal);
        person_email = (TextView) findViewById(R.id.personal_email_value);
        person_image = (ImageView) findViewById(R.id.personal_image);
        person_username = (TextView) findViewById(R.id.personal_name);
        person_nickname = (TextView) findViewById(R.id.personal_nick);
        person_qq = (TextView) findViewById(R.id.personal_qq_value);
        person_vip = (TextView) findViewById(R.id.personal_vip);

        change_password = (Button) findViewById(R.id.change_password);
        change_password.setOnClickListener(this);
        change_password_confirm = (Button) findViewById(R.id.btn_change);
        change_password_confirm.setOnClickListener(this);
        change_password_cancel = (Button) findViewById(R.id.btn_change_cancel);
        change_password_cancel.setOnClickListener(this);

        new_password = (EditText) findViewById(R.id.et_change_password);
        new_password_confirm = (EditText) findViewById(R.id.et_change_password_confirm);

        old_password = (EditText) findViewById(R.id.et_old_password);

        info = (RelativeLayout) findViewById(R.id.info_view);
        edit = (RelativeLayout) findViewById(R.id.edit_view);


        isChange = false;
        getActionBarUpButton().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
        username = PreferencesUtils.getString(this, "username");
        password = PreferencesUtils.getString(this, "password");

        new Thread(getInfo).start();
    }

    Runnable getInfo = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                ApplicationParam.token = NetworkUtil.login(username, password);
                mui = NetworkUtil.testMemberInfo();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = mui;
            handler.sendMessage(msg);
        }

    };

    Runnable submit_change = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                ApplicationParam.token = NetworkUtil.login(username, password);
                myReturn = NetworkUtil.testModifyPwd(password, password_new);
                Log.e("change", myReturn.toString());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 2;
            msg.obj = myReturn;
            handler.sendMessage(msg);
        }
    };
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.arg1) {
                case 1:
                    MemberUserInfo thisMemberInfo;
                    thisMemberInfo = (MemberUserInfo) msg.obj;
                    person_email.setText(thisMemberInfo.getEmail());
                    person_qq.setText(thisMemberInfo.getQq());
                    person_username.setText(thisMemberInfo.getUserName());
                    person_nickname.setText(thisMemberInfo.getNick());
                    ImageLoader.getInstance().displayImage(
                            thisMemberInfo.getHeadPortrait(), person_image);
                    if (thisMemberInfo.getIsVip().toString().equals("VIP")) {
                        person_vip.setText("是");

                    } else {
                        person_vip.setText("否");
                    }
                    Log.e("person", msg.obj.toString());
                    break;
                case 2:
                    if (msg.obj.equals(null)) {
                        Log.e("change", "null");
                    } else {

                        Return thisReturn;
                        thisReturn = (Return) msg.obj;
                        if (thisReturn.getSuccess().toString().equals("1")) {
                            Toast.makeText(getApplicationContext(), "修改密码成功！", Toast.LENGTH_SHORT).show();
                            PreferencesUtils.putString(PersonalInfoActivity.this, "username", username);
                            PreferencesUtils.putString(PersonalInfoActivity.this, "password", password_new);
                        }
                    }
                    break;
            }

        }

    };

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.change_password:
                if (!isChange) {
                    edit.setVisibility(View.VISIBLE);
                    info.setVisibility(View.GONE);
                    change_password.setVisibility(View.GONE);
                    isChange = true;
                } else {

                }
                break;

            case R.id.btn_change:
                if (old_password.getText().toString().length() < 1 || new_password.getText().toString().length() < 1 || new_password_confirm.getText().toString().length() < 1) {
                    Toast.makeText(getApplicationContext(), "请填写内容！", Toast.LENGTH_SHORT).show();
                } else if (!old_password.getText().toString().equals(password)) {
                    Toast.makeText(getApplicationContext(), "密码错误！", Toast.LENGTH_SHORT).show();
                } else if (!new_password.getText().toString().equals(new_password_confirm.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "两次输入新密码不一致！", Toast.LENGTH_SHORT).show();
                } else {
                    password_new = new_password.getText().toString();
                    new Thread(submit_change).start();
                }
                break;
            case R.id.btn_change_cancel:
                edit.setVisibility(View.GONE);
                info.setVisibility(View.VISIBLE);
                change_password.setVisibility(View.VISIBLE);
                isChange = false;
                break;
        }

    }
}
