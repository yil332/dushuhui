package com.whalefin.dushuhui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.smartlion.bookread.BookReadActivity;
import com.value.ebook.protobuf.MyEbookProtos.MyEbook.Book;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.adaptor.MyShelfAdapter;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.domain.BookShelfItemInfo;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.FileUtil;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.util.PreferencesUtils;

import net.tsz.afinal.FinalDb;

import org.apache.http.Header;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BookShelfActivity extends MyActionBarActivity implements
        OnClickListener {
    private List<BookShelfItemInfo> myShelf;
    private MyShelfAdapter thisAdapter;
    private List<Book> ebook;
    private GridView gridview;

    private String bookBasePath = "";
    private String fileName = "";

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_bookshelf);
        gridview = (GridView) findViewById(R.id.bookshelf_gridview);
        getActionBarBackground().setBackgroundColor(
                getResources().getColor(R.color.white));
        getActionBarRightButton().setVisibility(View.VISIBLE);
        getActionBarRightButton().setText(R.string.book_shelf_edit);
        getActionBarRightButton().setTextColor(
                getResources().getColor(R.color.book_shelf_title));
        getActionBarUpButton().setText(R.string.book_shelf_back);
        getActionBarUpButton().setTextColor(
                getResources().getColor(R.color.book_shelf_title));
        getActionBarTitle().setText(R.string.book_shelf);
        getActionBarTitle().setTextColor(
                getResources().getColor(R.color.book_shelf_title));
        getActionBarUpButton().setOnClickListener(this);
        getActionBarRightButton().setOnClickListener(this);

        ebook = new ArrayList<Book>();
        myShelf = initData();
        thisAdapter = new MyShelfAdapter(myShelf, this);
        gridview.setAdapter(thisAdapter);
        gridview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Log.e("download", "clicked");
                if (myShelf.get(position).getId() == "") {

                } else {
                    if (checkDownloaded(myShelf.get(position).getId())) {
                        Intent intent = new Intent(BookShelfActivity.this,
                                BookReadActivity.class);
                        intent.putExtra("bookid",
                                Long.parseLong(myShelf.get(position).getId()));
                        intent.putExtra("booktitle", myShelf.get(position)
                                .getBookName().toString());
                        startActivity(intent);
                    } else {
                        downloadBook(position);
                    }

                }

            }
        });

    }

    protected void downloadBook(int position) {
        // TODO Auto-generated method stub
        bookBasePath = FileUtil.downloadDirectory + "/"
                + myShelf.get(position).getId() + "/";
        fileName = myShelf.get(position).getId() + ".mobile";
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            String[] allowedContentTypes = new String[]{"application/zip",
                    "text/html; charset=UTF-8"};
            client.addHeader("APPCODE", "mobile_reading");
            // String[] allowedContentTypes = new String[] { "text/plain" };

            client.get(myShelf.get(position).getDownload(),
                    new BinaryHttpResponseHandler(allowedContentTypes) {
                        @Override
                        public void onSuccess(byte[] fileData) {
                            Log.v("download", "success");
                            // Do something with the file
                            String path = bookBasePath + fileName;
                            File file = new File(bookBasePath);
                            if (!file.isDirectory()) {
                                file.mkdirs();
                            }
                            Log.v("download", path);
                            File book = new File(bookBasePath, fileName);

                            if (!book.exists()) {

                                try {
                                    book.createNewFile();
                                    FileOutputStream fos = new FileOutputStream(
                                            path);
                                    fos.write(fileData);
                                    fos.close();
                                    Log.v("download", "File written");
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                            }
                            Toast.makeText(getApplicationContext(), "下载完成，再次点击进入阅读", Toast.LENGTH_SHORT).show();

                        }

                        //
                        @Override
                        public void onFailure(int statusCode, Header[] headers,
                                              byte[] binaryData, Throwable error) {
                            // TODO Auto-generated method stub

                            Log.v("download", "failed");
                            Log.v("download",
                                    "status=" + Integer.toString(statusCode));
                            Log.v("download", "error=" + error.getMessage());
                            for (Header header : headers) {
                                Log.i("download", header.getName() + " / "
                                        + header.getValue());
                            }
                            super.onFailure(statusCode, headers, binaryData,
                                    error);
                        }

                        @Override
                        public void onProgress(int bytesWritten, int totalSize) {
                            Log.v("download",
                                    ((bytesWritten * 100 / totalSize)) + "%");
                        }

                        @Override
                        public void onStart() {
                            // TODO Auto-generated method stub
                            Log.v("download", "start");
                            Toast.makeText(getApplicationContext(), "下载中，请等待", Toast.LENGTH_SHORT).show();
                            super.onStart();
                        }

                    }
            );
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    private List<BookShelfItemInfo> initData() {
        // TODO Auto-generated method stub
        FinalDb db = FinalDb.create(this);
        List<BookShelfItemInfo> data = db.findAll(BookShelfItemInfo.class);
        int datasize = data.size();
        if (datasize < 12) {
            for (int i = datasize; i < 12; i++) {
                BookShelfItemInfo bk = new BookShelfItemInfo();
                bk.setBookName("");
                bk.setId("");
                bk.setImgurl("");
                bk.setUrl("");
                data.add(bk);
            }
        } else {
            int column_number = datasize / 3 + 1;
            int sum_number = 3 * column_number;
            for (int i = datasize; i < sum_number; i++) {
                BookShelfItemInfo bk = new BookShelfItemInfo();
                bk.setBookName("");
                bk.setId("");
                bk.setImgurl("");
                bk.setUrl("");
                data.add(bk);

            }
        }
        Log.e("!", String.valueOf(data.size()));
        return data;
    }

    public BookShelfActivity() {
        super(R.string.book_shelf);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.ActionBarRightButton:
                if (!ApplicationParam.isLogin) {
                	Toast.makeText(getApplicationContext(), "请先登录！", Toast.LENGTH_SHORT).show();
                    Intent newIntent = new Intent(this, LoginActivity.class);
                    startActivityForResult(newIntent, 1);
                    this.finish();
                } else {
                    new Thread(sync).start();
                }
                break;
            case R.id.ActionBarUpButton:
                this.finish();
                break;
        }
    }

    Runnable sync = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                String username = PreferencesUtils.getString(BookShelfActivity.this, "username");
                String password = PreferencesUtils.getString(BookShelfActivity.this, "password");
                ApplicationParam.token = NetworkUtil.login(username, password);
                ebook = NetworkUtil.testMYEBOOK();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = ebook;
            handler.sendMessage(msg);

        }

    };
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.arg1) {
                case 1:
                    List<Book> book = (List<Book>) msg.obj;
                    FinalDb db = FinalDb.create(getApplicationContext());
                    myShelf = db.findAll(BookShelfItemInfo.class);
                    for (int i = 0; i < book.size(); i++) {
                        BookShelfItemInfo bk = new BookShelfItemInfo();
                        bk.setBookName(book.get(i).getBookName());
                        bk.setId(book.get(i).getBookId());
                        bk.setUrl("");
                        bk.setDownload(book.get(i).getDownload());
                        bk.setImgurl(book.get(i).getImage());
                        if (db.findById(book.get(i).getBookId(),
                                BookShelfItemInfo.class) != null) {
                            db.update(bk);
                        } else {
                            db.save(bk);
                            myShelf.add(bk);
                        }

                    }
                    int datasize = myShelf.size();
                    if (datasize < 12) {
                        for (int i = datasize; i < 12; i++) {
                            BookShelfItemInfo bk = new BookShelfItemInfo();
                            bk.setBookName("");
                            bk.setId("");
                            bk.setImgurl("");
                            bk.setUrl("");
                            myShelf.add(bk);
                        }
                    } else {
                        int column_number = datasize / 3 + 1;
                        int sum_number = 3 * column_number;
                        for (int i = datasize; i < sum_number; i++) {
                            BookShelfItemInfo bk = new BookShelfItemInfo();
                            bk.setBookName("");
                            bk.setId("");
                            bk.setImgurl("");
                            bk.setUrl("");
                            myShelf.add(bk);

                        }
                    }
                    thisAdapter = new MyShelfAdapter(myShelf,
                            getApplicationContext());
                    gridview.setAdapter(thisAdapter);
                    break;
            }
        }

    };

    private boolean checkDownloaded(String bookId) {
        // TODO Auto-generated method stub
        bookBasePath = FileUtil.downloadDirectory + "/" + bookId + "/";
        String filename = bookId + ".mobile";
        Log.e("download", bookBasePath + "123" + filename);
        File checkFile = new File(bookBasePath, filename);
        if (checkFile.exists()) {
            Log.e("download", "true");
            return true;
        } else {
            Log.e("download", "false");
            return false;

        }

    }

}