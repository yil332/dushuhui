package com.whalefin.dushuhui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.smartlion.bookread.BookReadActivity;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.domain.BookShelfItemInfo;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.FileUtil;
import com.whalefin.dushuhui.util.HtmlUtil;

import net.tsz.afinal.FinalDb;

import org.apache.http.Header;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by tongwanglin on 14-4-3.
 */
public class BookDetail extends MyActionBarActivity implements
        View.OnClickListener {

    private static final String[] TabTitle = new String[]{"商品详情", "编辑推荐",
            "作者简介", "相关电子书"};

    public String url = "";
    private String categoryOfBook;
    private ImageView bookCover;
    private TextView bookName;
    private TextView publishHouse;
    private TextView publishDate;
    private TextView author;
    private TextView fileSize;
    private ScrollView scroll_context;
    private TextView[] basicInfo = new TextView[4];
    private String downloadUrl;
    private String bookId;
    private String fileName;
    private String bookBasePath = "";
    private Button buy_now;
    private Button share;
    private TextView brief_info;
    private Boolean downloaded = false;
    private String imageurl = "";
    private String nameOfBook;
    private FinalDb db;
    private Boolean wait = false;

    public BookDetail() {
        super(R.string.book_store_online);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_book_detail);
        scroll_context = (ScrollView) this.findViewById(R.id.scroll_context);
        bookCover = (ImageView) findViewById(R.id.image);
        bookName = (TextView) findViewById(R.id.name);
        basicInfo[1] = publishHouse = (TextView) findViewById(R.id.publishHouse);
        basicInfo[2] = publishDate = (TextView) findViewById(R.id.publishDate);
        basicInfo[0] = author = (TextView) findViewById(R.id.author);
        basicInfo[3] = fileSize = (TextView) findViewById(R.id.fileSize);
        brief_info = (TextView) findViewById(R.id.brief_info);
        buy_now = (Button) findViewById(R.id.buy_now);
        share = (Button) findViewById(R.id.share);

        url = this.getIntent().getStringExtra("url");
        bookId = this.getIntent().getStringExtra("book-id");
        fileName = bookId + ".mobile";
        bookBasePath = FileUtil.downloadDirectory + "/" + bookId + "/";
        File file = new File(bookBasePath);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
        categoryOfBook = this.getIntent().getStringExtra("bookCategory");
        checkDownloaded(fileName);

        setActionBarUpButton(0, R.string.back);

        getActionBarUpButton().setOnClickListener(this);
        buy_now.setOnClickListener(this);
        share.setOnClickListener(this);
        db = FinalDb.create(this);
        getBookDetailRequest();
    }

    private void checkDownloaded(String filename) {
        // TODO Auto-generated method stub
        File checkFile = new File(bookBasePath, filename);
        if (checkFile.exists()) {
            downloaded = true;
            wait = false;
            buy_now.setText("立即阅读");
            Log.v("file", "exsit");
        } else {
            downloaded = false;
            Log.v("file", "new");
        }

    }

    private void getBookDetailRequest() {
        // TODO Auto-generated method stub
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            client.addHeader("APPCODE", "mobile_reading");
            Log.e("lyq", "AsyncHttpClient-url:" + HtmlUtil.urlBase + url);
            client.get(HtmlUtil.urlBase + url, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, String content) {
                    // TODO Auto-generated method stub
                    super.onSuccess(statusCode, content);
                    Document doc = Jsoup.parse(content);
                    initBookDetailView(doc);
                }

            });

        } catch (Exception e) {
            Log.e("yxw", "getRequestCategoryinfo");
        }
    }

    private void initBookDetailView(Document doc) {
        // TODO Auto-generated method stub
        Element pic = doc.select("img").first();
        imageurl = pic.attr("src");
        ImageLoader.getInstance().displayImage(imageurl, bookCover);
        nameOfBook = doc.select("h1.title").first().text();
        bookName.setText(nameOfBook);
        Elements basicInformation = doc.select("dt");
        for (int i = 0; i < basicInformation.size(); i++) {
            basicInfo[i].setText(basicInformation.get(i).text());
        }
        Elements book_content = doc.select("p");
        String content = "";
        for (int i = 0; i < book_content.size(); i++) {
            content += "\t\t";
            content += book_content.get(i).text();
            content += "\n";
        }
        brief_info.setText("简介:\n" + content);
        scroll_context.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.whalefin_alpha_scale_in));
        try {
            String temp = doc.select(".download-btn").first().attr("href")
                    .replace("bluelion:/", "http://112.4.28.82:3002/api/v2");
            String[] temps = temp.split("\\?");
            if (temps.length > 1) {
                Log.e("yxw", temps[0] + "/download");
                downloadUrl = temps[0] + "/download";
            } else {
                downloadUrl = temp + "/download";
            }
        } catch (Exception e) {
            downloadUrl = null;
        }
        // downloadUrl="http://m.stage.lanshizi.com/api/v2/books/2493/download";

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ActionBarUpButton) {
            this.finish();
        } else if (v.getId() == R.id.buy_now) {
            Log.e("finish", "OK1");
            if (ApplicationParam.isLogin) {
                if (!wait) {
                    Log.e("yxw", downloaded + "");
                    if (!downloaded) {
                        downloadBook();
                    } else {
                        // 在这里写阅读activity
                        // 传入参数bookId也可以,fileName也可以
                        Intent intent = new Intent(this, BookReadActivity.class);
                        // intent.putExtra("fileName", fileName);
                        intent.putExtra("bookid", Long.parseLong(bookId));
                        intent.putExtra("booktitle", bookName.getText()
                                .toString());
                        startActivity(intent);
                    }
                }
            } else {
                Toast.makeText(BookDetail.this, "请先登录！", Toast.LENGTH_SHORT).show();
                Intent newIntent = new Intent(this, LoginActivity.class);
                startActivityForResult(newIntent, 1);
            }

        } else if (v.getId() == R.id.share) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("image/*");
            sendIntent.putExtra(
                    Intent.EXTRA_STREAM,
                    Uri.fromFile(ImageLoader.getInstance().getDiscCache()
                            .get(imageurl))
            );
            sendIntent.putExtra(Intent.EXTRA_TITLE, "读书汇");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "我在看《" + nameOfBook + "》.");
            sendIntent.putExtra("Kdescription", "1我在看《" + nameOfBook + "》.");
            startActivity(sendIntent);
        }
    }

    private void downloadBook() {
        // TODO Auto-generated method stub
        Log.e("yxw", downloadUrl + "");
        if (downloadUrl == null) {
            Toast.makeText(this, "非常抱歉，无法下载！", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            String[] allowedContentTypes = new String[]{"application/zip",
                    "text/html; charset=UTF-8"};
            client.addHeader("APPCODE", "mobile_reading");
            // String[] allowedContentTypes = new String[] { "text/plain" };

            client.get(downloadUrl, new BinaryHttpResponseHandler(
                    allowedContentTypes) {
                @Override
                public void onSuccess(byte[] fileData) {
                    Log.v("download", "success");
                    // Do something with the file
                    String path = bookBasePath + fileName;
                    Log.v("download", path);
                    File book = new File(bookBasePath, fileName);
                    if (!book.exists()) {

                        try {
                            book.createNewFile();
                            FileOutputStream fos = new FileOutputStream(path);
                            fos.write(fileData);
                            fos.close();
                            Log.v("download", "File written");
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    }

                    checkDownloaded(fileName);

                    //
                    BookShelfItemInfo bsi = new BookShelfItemInfo();
                    bsi.setId(bookId);
                    bsi.setBookName(nameOfBook);
                    bsi.setImgurl(imageurl);
                    bsi.setUrl(url);

                    db.save(bsi);

                    List<BookShelfItemInfo> data = db
                            .findAll(BookShelfItemInfo.class);
                    Log.e("database", data.get(0).getId()
                            + data.get(0).getBookName()
                            + data.get(0).getImgurl() + data.get(0).getUrl());
                    Intent intent = new Intent(getApplication(), BookReadActivity.class);
                    // intent.putExtra("fileName", fileName);
                    intent.putExtra("bookid", Long.parseLong(bookId));
                    intent.putExtra("booktitle", bookName.getText()
                            .toString());
                    startActivity(intent);
                }

                //
                @Override
                public void onFailure(int statusCode, Header[] headers,
                                      byte[] binaryData, Throwable error) {
                    // TODO Auto-generated method stub

                    Log.v("download", "failed");
                    Log.v("download", "status=" + Integer.toString(statusCode));
                    Log.v("download", "error=" + error.getMessage());
                    for (Header header : headers) {
                        Log.i("download",
                                header.getName() + " / " + header.getValue());
                    }
                    super.onFailure(statusCode, headers, binaryData, error);
                }

                @Override
                public void onProgress(int bytesWritten, int totalSize) {
                    // TODO Auto-generated method stub
                    // 可以写下载进度通知！
                    // Log.v("download",
                    // String.valueOf((float) bytesWritten / totalSize));
                    // super.onProgress(bytesWritten, totalSize);
                    buy_now.setText("正在下载");
                    buy_now.setText(String.valueOf(bytesWritten * 100 / totalSize) + "%");
                    Log.v("download", ((bytesWritten * 100 / totalSize)) + "%");
                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub
                    Log.v("download", "start");
//					buy_now.setText("请等待");
                    wait = true;
                    super.onStart();
                }

            });
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

}