package com.whalefin.dushuhui.activity;



import java.io.IOException;

import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.util.NetworkUtil;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends MyActionBarActivity implements
		View.OnClickListener {

	private Button register;
	private Button get_verify_code;
	private EditText username;
	private EditText password;
	private EditText password_confirm;
	private EditText verify_code;
	private EditText email_address;
	private int timeCount;
	private String verifyResult;
    private String Result;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_register);
		setActionBarUpButton(0, R.string.cancel);

		register = (Button) findViewById(R.id.btn_register_new);
		register.setOnClickListener(this);

		get_verify_code = (Button) findViewById(R.id.btn_register_verifycode);
		get_verify_code.setOnClickListener(this);
		
		username = (EditText)findViewById(R.id.et_register_username);
		password = (EditText)findViewById(R.id.et_register_password);
		password_confirm = (EditText)findViewById(R.id.et_register_password_confirm);
		verify_code = (EditText)findViewById(R.id.et_register_verifycode);
		email_address = (EditText)findViewById(R.id.et_register_email);

		getActionBarUpButton().setOnClickListener(this);

		timeCount = 10;

		
	}
	Handler handler = new Handler();
	Runnable runnable = new Runnable() {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (timeCount != 0) {
				get_verify_code.setText("请等待("
						+ Integer.toString(timeCount--) + ")秒");
			}
			else{
				get_verify_code.setText("再次发送");
				stopTask(); 
			}
			handler.postDelayed(this, 1000);
		}
	};

	protected void stopTask() {
		handler.removeCallbacks(runnable);   
		
	}

	public Register() {
		super(R.string.register);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.ActionBarUpButton:
			this.finish();
			break;
		case R.id.btn_register_new:
			if(username.getText().toString().trim().equals("")){
				Toast.makeText(this, "请填写用户名", Toast.LENGTH_SHORT).show();
			}else if(verify_code.getText().toString().trim().equals("")){
				Toast.makeText(this, "请填写验证码", Toast.LENGTH_SHORT).show();
			}else if (password.getText().toString().trim().equals("")|password_confirm.getText().toString().trim().equals("")){
				Toast.makeText(this, "请输入密码", Toast.LENGTH_SHORT).show();
			}else if (!password.getText().toString().trim().equals(password_confirm.getText().toString().trim())){
				Toast.makeText(this, "两次密码输入不正确", Toast.LENGTH_SHORT).show();
			}else{
				new Thread(Register).start();
			}
			break;
		case R.id.btn_register_verifycode:
			if(username.getText().toString().trim().equals("")){
				Toast.makeText(this, "请填写用户名", Toast.LENGTH_SHORT).show();
			}
			else{
			new Thread(getVerify).start();
			timeCount=10;
			handler.postDelayed(runnable, 1000);
			break;
			}
		}
	}
	Runnable getVerify = new Runnable() {
		public void run() {
			Message msg = handler.obtainMessage();
			String mobileuser = username.getText().toString();
			Log.e("verify",mobileuser);
			try {
				verifyResult = NetworkUtil.testGetMassage(mobileuser);
				Log.e("verify",verifyResult);
				
			} catch (Exception e) {
				for (int i = 0; i < e.getStackTrace().length; i++) {
				}
				e.printStackTrace();
			}
			msg.arg1 = 1;
			msg.obj = verifyResult;
			networkHandler.sendMessage(msg);
		}
	};
	
	Runnable Register = new Runnable(){

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Message msg = handler.obtainMessage();
			String mobileuser = username.getText().toString();
			String verifycode =verify_code.getText().toString();
			String pwd = password.getText().toString();
			try {
				Result = NetworkUtil.memberRegister(mobileuser, verifycode,pwd);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			msg.arg1 = 2;
			msg.obj = Result;
			networkHandler.sendMessage(msg);
		}
		
	};
	Handler networkHandler =new Handler(){
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.arg1) {
			case 1:
				verify_code.setText(msg.obj.toString());
				Toast.makeText(Register.this, msg.obj.toString(), Toast.LENGTH_SHORT).show();
				break;
			case 2:
				if(msg.obj.toString().equals("")){
					Toast.makeText(Register.this, "您已经是会员，请不要重复注册！", Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(Register.this, "注册成功！", Toast.LENGTH_SHORT).show();
				}
			}
		}
	};


}
