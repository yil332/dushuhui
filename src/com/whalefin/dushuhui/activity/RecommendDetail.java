package com.whalefin.dushuhui.activity;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.SpannedString;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.base.MyActionBarActivity;

public class RecommendDetail extends MyActionBarActivity implements OnClickListener{
    private TextView recdetail_content;
    private ImageView recdetial_image;
    private ImageLoader mImageLoader;
    private String content;
    private String title;
    private String image;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_recdetail);
		recdetail_content = (TextView) findViewById(R.id.recdetail_content);
		recdetial_image =(ImageView) findViewById(R.id.recdetail_topview);
		mImageLoader = ImageLoader.getInstance();
		setActionBarUpButton(0, R.string.back);
      
		getActionBarUpButton().setOnClickListener(this);
		
		title=this.getIntent().getStringExtra("title");
		image=this.getIntent().getStringExtra("image");
		content= this.getIntent().getStringExtra("content");
		mImageLoader.displayImage(image, recdetial_image);
		
		Spanned spanned = Html.fromHtml(content);
		recdetail_content.setText(spanned);
		this.setActionBarTitle(title);
	}

	public RecommendDetail() {
		super(R.string.daily_recommend);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.ActionBarUpButton:
				this.finish();
				break; 
		}
		
	}

}
