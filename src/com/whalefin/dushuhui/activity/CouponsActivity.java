package com.whalefin.dushuhui.activity;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.value.ebook.protobuf.MemberCouponProtos.MemberCoupon.MemberCouponInfo;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.adaptor.CouponListAdapter;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.domain.CouponInfo;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.util.PreferencesUtils;

import java.util.ArrayList;
import java.util.List;

public class CouponsActivity extends MyActionBarActivity {
    // UI组件
    private ListView listview;


    private List<MemberCouponInfo> MemberCouponList;
    private List<CouponInfo> myCouponInfoList;
    private List<CouponInfo> myCouponInfoList_used;
    private List<CouponInfo> myCouponInfoList_notused;
    private CouponListAdapter thisAdapter;
    private RadioGroup rg;

    public CouponsActivity() {
        super(R.string.coupons_my);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_coupons);
        listview = (ListView) findViewById(R.id.listview);
        rg = (RadioGroup) findViewById(R.id.radio_btns);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                switch (group.getCheckedRadioButtonId()) {
                    case R.id.text_all:
                        thisAdapter = new CouponListAdapter(getApplicationContext(),
                                myCouponInfoList);
                        listview.setAdapter(thisAdapter);

                        break;
                    case R.id.text_used:
                        thisAdapter = new CouponListAdapter(getApplicationContext(),
                                myCouponInfoList_used);
                        listview.setAdapter(thisAdapter);
                        break;
                    case R.id.text_not_used:
                        thisAdapter = new CouponListAdapter(getApplicationContext(),
                                myCouponInfoList_notused);
                        listview.setAdapter(thisAdapter);
                        break;
                }
            }
        });

        getActionBarUpButton().setText(R.string.back);
        getActionBarUpButton().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();

            }
        });
        getActionBarRightButton().setVisibility(View.INVISIBLE);
        myCouponInfoList = new ArrayList<CouponInfo>();
        myCouponInfoList_notused = new ArrayList<CouponInfo>();
        myCouponInfoList_used = new ArrayList<CouponInfo>();

        if (!ApplicationParam.isLogin) {
            Toast.makeText(this, "未登录",
                    Toast.LENGTH_SHORT).show();
        } else {
            new Thread(getCouponData).start();

        }
    }

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1) {
                case 1:
                    // todo;
                    Log.e("coupon", "message-get");
                    List<MemberCouponInfo> lmci = new ArrayList<MemberCouponInfo>();
                    if (!msg.obj.equals(null)) {
                        lmci = (List<MemberCouponInfo>) msg.obj;
                        for (int i = 0; i < lmci.size(); i++) {
                            CouponInfo ci = new CouponInfo();
                            ci.setBusinessName(lmci.get(i).getBuinessName());
                            ci.setEndDate(lmci.get(i).getEndDate());
                            ci.setKey(lmci.get(i).getKey());
                            ci.setLogoUrl(lmci.get(i).getBuinessLogo());
                            ci.setMemo(lmci.get(i).getMemo());
                            ci.setName(lmci.get(i).getName());
                            ci.setOnOff(lmci.get(i).getOnOff());
                            ci.setPassword(lmci.get(i).getPassword());
                            ci.setPrice(lmci.get(i).getPrice());

                            myCouponInfoList.add(ci);
                            if (lmci.get(i).getOnOff().equals("1")) {
                                myCouponInfoList_notused.add(ci);
                            } else {
                                myCouponInfoList_used.add(ci);
                            }
                        }
                    }
                    if (myCouponInfoList != null) {
                        thisAdapter = new CouponListAdapter(getApplicationContext(),
                                myCouponInfoList_notused);
                        listview.setAdapter(thisAdapter);
                    }
                    break;
            }
        }
    };

    Runnable getCouponData = new Runnable() {
        @Override
        public void run() {
            Log.e("coupon", "run");
            Log.e("coupon", ApplicationParam.token);
            Message msg = handler.obtainMessage();
            try {
                if (!NetworkUtil.getCoupon().equals(null)) {
                    MemberCouponList = NetworkUtil.getCoupon().getInfoList();
                }
                if (NetworkUtil.getCoupon().getTokenCode().equals("1")) {
                    String username = PreferencesUtils.getString(CouponsActivity.this, "username");
                    String password = PreferencesUtils.getString(CouponsActivity.this, "password");
                    ApplicationParam.token = NetworkUtil.login(username, password);
                    MemberCouponList = NetworkUtil.getCoupon().getInfoList();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = MemberCouponList;

            handler.sendMessage(msg);

        }
    };

}
