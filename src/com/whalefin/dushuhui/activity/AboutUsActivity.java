package com.whalefin.dushuhui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.value.ebook.protobuf.AppAboutUsProtos.AppAboutUs;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.util.NetworkUtil;

public class AboutUsActivity extends MyActionBarActivity {
	private AppAboutUs apu;
	private ImageView aboutUs_image;
	private TextView aboutUs_name;
	private TextView aboutUs_version;
	private TextView aboutUs_website;
	private TextView aboutUs_content;

	
	public AboutUsActivity() {
		super(R.string.about_us);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setActionBarUpButton(0, R.string.back);
		setContentView(R.layout.layout_aboutus);
		aboutUs_content =(TextView)findViewById(R.id.about_us_content);
		aboutUs_image = (ImageView)findViewById(R.id.about_us_image);
		aboutUs_name=(TextView)findViewById(R.id.about_us_name);
		aboutUs_version = (TextView)findViewById(R.id.about_us_version);
		aboutUs_website =(TextView)findViewById(R.id.about_us_web);
		getActionBarUpButton().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		new Thread(About_us).start();
	}
	Runnable About_us = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Log.e("aboutus","run");
			Message msg = handler.obtainMessage();
			try {
				if(!NetworkUtil.testappAboutUs().equals(null)){
				apu = NetworkUtil.testappAboutUs();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			msg.arg1 =1;
			msg.obj =apu;
			handler.sendMessage(msg);
		}
		
	};
	Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			Log.e("aboutus","handle");
			super.handleMessage(msg);
			switch(msg.arg1){
			case 1:
				try{
				AppAboutUs aboutus= (AppAboutUs)msg.obj;
				aboutUs_content.setText(aboutus.getRemark());
				aboutUs_name.setText(aboutus.getName());
				aboutUs_version.setText("V"+aboutus.getVersions());
				aboutUs_website.setText("官方网站："+aboutus.getWebUrl());
				ImageLoader.getInstance().displayImage(aboutus.getImage(), aboutUs_image);
				}catch(Exception e){
					Toast.makeText(getApplicationContext(), "网络连接错误", Toast.LENGTH_SHORT).show();;
				}
				break;
			}
		}
		
	};

}
