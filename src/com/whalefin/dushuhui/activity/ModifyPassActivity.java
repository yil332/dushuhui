package com.whalefin.dushuhui.activity;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.value.ebook.protobuf.ReturnProtos.Return;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.base.MyActionBarActivity;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.util.PreferencesUtils;

public class ModifyPassActivity extends MyActionBarActivity implements OnClickListener {
    private EditText et1;
    private EditText et2;

    private String userName;
    private String passWord;
    private String newPassWord;
    private Return myReturn;

    public ModifyPassActivity() {
        super(R.string.setting_change_password);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_modifypassword);
        et1 = (EditText) findViewById(R.id.modify_pass_value);
        et2 = (EditText) findViewById(R.id.modify_confirm_value);
        setActionBarUpButton(0, R.string.back);

        getActionBarUpButton().setOnClickListener(this);
        getActionBarRightButton().setVisibility(View.VISIBLE);
        setActionBarRightpButton(0, R.string.save);
        getActionBarRightButton().setOnClickListener(this);

        userName = PreferencesUtils.getString(this, "username");
        passWord = PreferencesUtils.getString(this, "password");

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.ActionBarUpButton:
                this.finish();
                break;

            case R.id.ActionBarRightButton:
                if (et1.getText().toString().equals(et2.getText().toString())) {
                    new Thread(modify).start();
                } else {
                    Toast.makeText(getApplicationContext(), "两次输入不一致!", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    Runnable modify = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                ApplicationParam.token = NetworkUtil.login(userName, passWord);
                newPassWord = et1.getText().toString();
                myReturn = NetworkUtil.testModifyPwd(passWord, newPassWord);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = myReturn;
            handler.sendMessage(msg);
        }

    };
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.arg1) {
                case 1:
                    Return thisReturn;
                    thisReturn = (Return) msg.obj;
                    if (thisReturn.getSuccess().toString().equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改密码成功！", Toast.LENGTH_SHORT).show();
                        PreferencesUtils.putString(ModifyPassActivity.this, "username", userName);
                        PreferencesUtils.putString(ModifyPassActivity.this, "password", newPassWord);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }

    };

}
