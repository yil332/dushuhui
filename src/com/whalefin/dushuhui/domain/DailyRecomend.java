package com.whalefin.dushuhui.domain;

import java.util.List;

/**
 * Created by tongwanglin on 14-4-2.
 */
public class DailyRecomend {
    public List<BookInfo> getBookInfoList() {
        return bookInfoList;
    }

    public void setBookInfoList(List<BookInfo> bookInfoList) {
        this.bookInfoList = bookInfoList;
    }

    private String recomandDate;
    private List<BookInfo> bookInfoList;

    public String getRecomandDate() {
        return recomandDate;
    }

    public void setRecomandDate(String recomandDate) {
        this.recomandDate = recomandDate;
    }
}
