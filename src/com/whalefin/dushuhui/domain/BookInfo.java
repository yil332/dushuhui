package com.whalefin.dushuhui.domain;

/**
 * Created by tongwanglin on 14-4-2.
 */
public class BookInfo {

	private String name;
	private String image;
	private String author;
	private float price;
	private String recomandDate;
	private String recomandReson;
	private String category;
	private int hot;
	private Boolean isRecommend;
	private int latest;
	private String content;
	private String url;
	private String bookId;

	public int getHot() {
		return hot;
	}

	public void setHot(int hot) {
		this.hot = hot;
	}

	public Boolean getIsRecommend() {
		return isRecommend;
	}

	public void setIsRecommend(Boolean isRecommend) {
		this.isRecommend = isRecommend;
	}

	public int getLatest() {
		return latest;
	}

	public void setLatest(int latest) {
		this.latest = latest;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getRecomandDate() {
		return recomandDate;
	}

	public void setRecomandDate(String recomandDate) {
		this.recomandDate = recomandDate;
	}

	public String getRecomandReson() {
		return recomandReson;
	}

	public void setRecomandReson(String recomandReson) {
		this.recomandReson = recomandReson;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	
	
}
