package com.whalefin.dushuhui.domain;

import java.util.List;

public class BookItems {
	private List<BookInfo> bookInfoList;

	public List<BookInfo> getBookInfoList() {
		return bookInfoList;
	}

	public void setBookInfoList(List<BookInfo> bookInfoList) {
		this.bookInfoList = bookInfoList;
	}
	

}
