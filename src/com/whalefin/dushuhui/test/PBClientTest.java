package com.whalefin.dushuhui.test;
//

import com.value.ebook.protobuf.AppSetProtos;
import com.value.ebook.protobuf.MemberCouponProtos;
import com.value.ebook.protobuf.MemberForgetPasswordProtos;
import com.value.ebook.protobuf.MemberGetCouponProtos;
import com.value.ebook.protobuf.MemberPasswordProtos;
import com.value.ebook.protobuf.MemberUserInfoProtos;
import com.value.ebook.protobuf.MemberUserProtos;
import com.value.ebook.protobuf.MemberUserProtos.MemberUser;
import com.value.ebook.protobuf.MemberUserRegistProtos;
import com.value.ebook.protobuf.ReturnProtos;
import com.value.ebook.protobuf.ShortMessagesProtos;
import com.value.ebook.protobuf.VipProtos;

import org.junit.Assert;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

//
///**
// *
// * @author jj
// */
public class PBClientTest {
    /**
     * 日志对象
     */
//    protected Logger logger = LoggerFactory.getLogger(getClass());
    public static void main(String[] args) throws Exception {
//        memberLogin();
//        testGetMassage();
//        memberRegister();
//        testModifyPwd();
//        testMemberInfo();
//        testMemberSet();
//        testGetMemberSet();
//        testGetCoupon();
//        testVIP();
    }


    public static InputStream connect(String url, byte[] content) throws IOException {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        URL targetUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) targetUrl.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("Content-Type", "application/x-protobuf");
        connection.setRequestProperty("Accept", "application/x-protobuf");
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connect-Length", Integer.toString(content.length));
        connection.setFixedLengthStreamingMode(content.length);
        outputStream = connection.getOutputStream();
        outputStream.write(content);
        outputStream.flush();
        // check response code
        int code = connection.getResponseCode();
//        logger.info("code:" + code);
        System.out.println(connection.getContent());
        boolean success = (code >= 200) && (code < 300);
        inputStream = success ? connection.getInputStream() : connection.getErrorStream();
        return inputStream;
    }


    /**
     * 测试登录
     *
     * @throws java.io.IOException
     */
    public static void memberLogin() throws Exception {
        String url = "http://192.168.0.137:8080/ebook/ws/login.pb";
        MemberUser memberUser = MemberUser.newBuilder()
                .setUserName("15967105436")
                .setPasswrod("123456789")
                .setType(MemberUser.MemberUserType.MEMBER).build();
        byte[] content = memberUser.toByteArray();
        InputStream in = connect(url, content);
        MemberUser user = MemberUser.parseFrom(in);
        in.close();
        System.out.println(user);
        Assert.assertNotNull(user.getToken(0).getToken());
    }

    ////
    public static String login() throws Exception {
        String url = "http://192.168.0.137:8080/ebook/ws/login.pb";
        MemberUser memberUser = MemberUser.newBuilder()
                .setUserName("15967105436")
                .setPasswrod("123456789")
                .setType(MemberUser.MemberUserType.MEMBER).build();
        byte[] content = memberUser.toByteArray();
        InputStream in = connect(url, content);
        MemberUser user = MemberUser.parseFrom(in);
        in.close();
        return user.getToken(0).getToken().toString();
    }
    /**
     * 发送短信测试
     */
//    @Test
//    public void testGetMassage() throws Exception {
//        String url = "http://192.168.0.137:8080/ebook/ws/getMassage.pb";
//        ShortMessagesProtos.ShortMessages shortMessages = ShortMessagesProtos.ShortMessages.newBuilder()
//                .setMobile("15011111111")
//                .build();
//        byte[] content = shortMessages.toByteArray();
//        OutputStream outputStream = null;
//        URL targetUrl = new URL(url);
//        HttpURLConnection connection = (HttpURLConnection) targetUrl.openConnection();
//        connection.setDoOutput(true);
//        connection.setDoInput(true);
//        connection.setRequestProperty("Content-Type", "application/x-protobuf");
//        connection.setRequestProperty("Accept", "application/x-protobuf");
//        connection.setRequestMethod("POST");
//        connection.setRequestProperty("Connect-Length", Integer.toString(content.length));
//        connection.setFixedLengthStreamingMode(content.length);
//        outputStream = connection.getOutputStream();
//        outputStream.write(content);
//        outputStream.flush();
//    }


    /**
     * 发送短信测试
     */
//    @Test
    public static void testGetMassage() throws Exception {


        String url = "http://192.168.0.137:8080/ebook/ws/getMassage.pb";
        ShortMessagesProtos.ShortMessages shortMessages = ShortMessagesProtos.ShortMessages.newBuilder()
                .setMobile("15505880801")
                .build();
        byte[] content = shortMessages.toByteArray();
        InputStream in = connect(url, content);
        ShortMessagesProtos.ShortMessages s = ShortMessagesProtos.ShortMessages.parseFrom(in);
        in.close();
        System.out.println(s);
        System.out.println(s.getCode());
    }

    /**
     * 修改密码测试
     */
//    @Test
    public static void testModifyPwd() throws Exception {
        String token = login();
        System.out.println(token);
        String url = "http://192.168.0.137:8080/ebook/ws/modifyPwd.pb";
        MemberPasswordProtos.MemberPassword memberPassword = MemberPasswordProtos.MemberPassword.newBuilder()
                .setOldPassword("123456")
                .setNewPassword("123456789")
                .setType(MemberPasswordProtos.MemberPassword.MemberUserType.MEMBER)
                .addToken(MemberPasswordProtos.MemberPassword.MemberToken.newBuilder()
                        .setToken(token)
                        .build())
                .build();
        byte[] content = memberPassword.toByteArray();
        InputStream in = connect(url, content);
        ReturnProtos.Return returnState = ReturnProtos.Return.parseFrom(in);
        in.close();
        //Assert.assertEquals("1", returnState.getSuccess());
        System.out.print(returnState.getMsg());
    }

    /**
     * 忘记密码测试
     */
//    @Test
    public void testForgetPwd() throws Exception {
        String code = "";
        String url = "http://192.168.0.137:8080/ebook/ws/forgetPwd.pb";

        MemberForgetPasswordProtos.MemberForgetPassword forgetPwd = MemberForgetPasswordProtos.MemberForgetPassword.newBuilder()
                .setMobile("15011111111")
                .setNewPassword("15011111111")
                .setCode(code)
                .build();
        byte[] content = forgetPwd.toByteArray();
        InputStream in = connect(url, content);
        ReturnProtos.Return returnState = ReturnProtos.Return.parseFrom(in);
        in.close();
        Assert.assertEquals("1", returnState.getSuccess());
    }

    /**
     * 手机注册
     */
//    @Test
    public static void memberRegister() throws IOException {
        MemberUserRegistProtos.MemberUserRegist memberUser = MemberUserRegistProtos.MemberUserRegist.newBuilder()
                .setUserName("15505880801")
                .setCode("6210")
                .setType(MemberUserRegistProtos.MemberUserRegist.MemberUserType.MEMBER)
                .addInfo(
                        MemberUserRegistProtos.MemberUserRegist.MemberInfo.newBuilder()
                                .setPassword("admin")
                                .setConfirmPassword("admin")
                                .build()
                ).build();
        byte[] content = memberUser.toByteArray();
        String url = "http://192.168.0.137:8080/ebook/ws/register.pb";
        URL targetUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) targetUrl.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("Content-Type", "application/x-protobuf");
        connection.setRequestProperty("Accept", "application/x-protobuf");
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connect-Length", Integer.toString(content.length));
        connection.setFixedLengthStreamingMode(content.length);
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(content);
        outputStream.flush();

        // check response code
        int code = connection.getResponseCode();
        System.out.println("code:" + code);
        System.out.println(connection.getContent());
        boolean success = (code >= 200) && (code < 300);

        InputStream in = success ? connection.getInputStream() : connection.getErrorStream();
        MemberUserProtos.MemberUser user = MemberUserProtos.MemberUser.parseFrom(in);
        in.close();
        System.out.println(user);

        outputStream.close();
    }

    /**
     * 个人信息测试
     */
//    @Test
    public static void testMemberInfo() throws Exception {
        String token = login();
        String url = "http://192.168.0.137:8080/ebook/ws/memberInfo.pb";
        MemberUserInfoProtos.MemberUserInfo memberInfo = MemberUserInfoProtos.MemberUserInfo.newBuilder()
                .setType(MemberUserInfoProtos.MemberUserInfo.MemberUserType.MEMBER).setIsVip(MemberUserInfoProtos.MemberUserInfo.MemberUserVIPType.COMMON)
                .setUserName("15967105436")
                .addToken(MemberUserInfoProtos.MemberUserInfo.MemberToken.newBuilder()
                        .setToken(token)
                        .build())
                .build();
        byte[] content = memberInfo.toByteArray();
        InputStream in = connect(url, content);
        MemberUserInfoProtos.MemberUserInfo memberUserInfo = MemberUserInfoProtos.MemberUserInfo.parseFrom(in);
        in.close();
        System.out.print(memberUserInfo.toString());
//        Assert.assertNotNull(memberUserInfo);
    }
//
//    /**
//     * 个人设置信息测试
//     */
//    @Test
    public static void testMemberSet() throws Exception {
        String token = login();
        String url = "http://192.168.0.137:8080/ebook/ws/memberSet.pb";
        AppSetProtos.AppSet memberSet = AppSetProtos.AppSet.newBuilder()
                .setPushSet(1)
                .setRingSet(1)
                .setWifiPic(1)
                .setType(AppSetProtos.AppSet.MemberUserType.MEMBER)
                .addToken(AppSetProtos.AppSet.MemberToken.newBuilder()
                        .setToken(token)
                        .build())
                .build();
        byte[] content = memberSet.toByteArray();
        InputStream in = connect(url, content);
        ReturnProtos.Return returnState = ReturnProtos.Return.parseFrom(in);
        in.close();
        System.out.println(returnState);
    }
//    /**
//     * 获取个人设置信息测试
//     */
//    @Test
    public static void testGetMemberSet() throws Exception {
        String url = "http://192.168.0.137:8080/ebook/ws/getMemberSet.pb";
        String token = login();
        AppSetProtos.AppSet memberSet = AppSetProtos.AppSet.newBuilder()
                .setPushSet(1)
                .setRingSet(1)
                .setWifiPic(1)
                .setType(AppSetProtos.AppSet.MemberUserType.MEMBER)
                .addToken(AppSetProtos.AppSet.MemberToken.newBuilder()
                        .setToken(token)
                        .build())
                .build();
        byte[] content = memberSet.toByteArray();
        InputStream in = connect(url, content);
        AppSetProtos.AppSet returnState = AppSetProtos.AppSet.parseFrom(in);
        in.close();
        System.out.println(returnState);
//        Assert.assertNotNull(appSet);
    }

//    /**
//     * 获取优惠券
//     */
//    @Test
    public static void testGetCoupon() throws Exception {
        String token = login();
        String url = "http://192.168.0.137:8080/ebook/ws/getCouponProtos.pb";
        MemberGetCouponProtos.MemberGetCoupon  memberGetCoupon = MemberGetCouponProtos.MemberGetCoupon.newBuilder()
                .setType(MemberGetCouponProtos.MemberGetCoupon.MemberUserType.MEMBER)
                .setIsVip(MemberGetCouponProtos.MemberGetCoupon.MemberUserVIPType.VIP)
                .addToken(MemberGetCouponProtos.MemberGetCoupon.MemberToken.newBuilder()
                        .setToken(token)
                        .build())
                .build();
        byte[] content = memberGetCoupon.toByteArray();
        InputStream in = connect(url, content);
        MemberCouponProtos.MemberCoupon    coupon = MemberCouponProtos.MemberCoupon .parseFrom(in);
        in.close();
        System.out.println(coupon.getInfoList().toString());
    }

//
//    @Test
    public static void testVIP() throws Exception {
        String token = login();
        String url = "http://192.168.0.137:8080/ebook/ws/vip.pb";
        VipProtos.Vip vip = VipProtos.Vip.newBuilder().setMobile("15967105436")
                .addToken( VipProtos.Vip.MemberToken.newBuilder()
                        .setToken(token))
                .build();
        byte[] content = vip.toByteArray();
        InputStream in = connect(url, content);
        VipProtos.Vip returnState = VipProtos.Vip.parseFrom(in);
        in.close();
        System.out.println(returnState);
    }
}
