package com.whalefin.dushuhui.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.domain.BookShelfItemInfo;

import java.util.List;

public class MyShelfAdapter extends BaseAdapter {
	private List<BookShelfItemInfo> myShelf;
	private LayoutInflater mInflater;
	private ImageLoader imageLoader;
	private Context context;

	public MyShelfAdapter(List<BookShelfItemInfo> myShelf, Context context) {
		super();
		this.myShelf = myShelf;
		this.context = context;
		mInflater = LayoutInflater.from(context);
		imageLoader = ImageLoader.getInstance();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return myShelf.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return myShelf.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.layout_bookshelf_item,
					null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.ItemText);
			holder.image = (ImageView) convertView.findViewById(R.id.ItemImage);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if (myShelf.get(position).getImgurl() == "") {
			holder.image.setVisibility(View.INVISIBLE);
		} else {
			imageLoader.displayImage(myShelf.get(position).getImgurl(),
					holder.image);
		}
		holder.name.setText(myShelf.get(position).getBookName());
		return convertView;
	}

	static class ViewHolder {
		private ImageView image;
		private TextView name;		
	}
}
