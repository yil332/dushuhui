package com.whalefin.dushuhui.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.domain.BookInfo;

import java.util.ArrayList;
import java.util.List;

public class BookItemListAdapter extends BaseAdapter {

    private List<BookInfo> bookitems;
    private ImageLoader imageLoader;
    private Context context;
    private Boolean isCategoty;

    public BookItemListAdapter(Context context, List<BookInfo> bookitems) {
        this.bookitems = bookitems;
        if (bookitems == null) {
            this.bookitems = new ArrayList<BookInfo>();
        }
        this.context = context;
        this.imageLoader = ImageLoader.getInstance();
        this.isCategoty = false;
    }

    public void setIsCategoty(Boolean isCategoty) {
        this.isCategoty = isCategoty;
    }

    @Override
    public int getCount() {
        return bookitems.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return bookitems.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.list_item_booksrecomend_child, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            holder.author = (TextView) convertView.findViewById(R.id.author);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.content = (TextView) convertView
                    .findViewById(R.id.RecReason);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        imageLoader.displayImage(bookitems.get(position).getImage(), holder.image);
        holder.name.setText(bookitems.get(position).getName());
        holder.author.setText(bookitems.get(position).getAuthor());
        if (!isCategoty) {
            holder.price.setVisibility(View.VISIBLE);
            holder.price.setText(bookitems.get(position).getPrice()
                    + "元");
        } else {
            holder.author.setVisibility(View.INVISIBLE);
            holder.price.setVisibility(View.INVISIBLE);
            holder.price.setText("");
        }
        holder.content.setText(bookitems.get(position).getContent());
        return convertView;
    }


    static class ViewHolder {
        private ImageView image;
        private TextView name;
        private TextView author;
        private TextView price;
        private TextView content;

    }

}
