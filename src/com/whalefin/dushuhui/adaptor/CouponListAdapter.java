package com.whalefin.dushuhui.adaptor;

import java.util.List;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.domain.CouponInfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CouponListAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private ImageLoader imageLoader;
	private Context context;
	private List<CouponInfo> couponlist;
	
	
	public CouponListAdapter(Context context, List<CouponInfo> couponlist) {
		super();
		this.context = context;
		this.couponlist = couponlist;
		mInflater = LayoutInflater.from(context);
		imageLoader = ImageLoader.getInstance();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return couponlist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return couponlist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_coupon,
					null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.text_coupon_name);
			holder.image = (ImageView) convertView.findViewById(R.id.image_business_logo);
			holder.businessName = (TextView) convertView.findViewById(R.id.text_store_name);
			holder.endDate =(TextView)convertView.findViewById(R.id.text_effective_date);
			holder.price =(TextView)convertView.findViewById(R.id.text_money);
			holder.used_or_not =(ImageView)convertView.findViewById(R.id.image_used_tag);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.name.setText(couponlist.get(position).getName());
		holder.businessName.setText(couponlist.get(position).getBusinessName());
		holder.endDate.setText("有效期至："+couponlist.get(position).getEndDate());
		holder.price.setText(couponlist.get(position).getPrice()+"元");
		imageLoader.displayImage(couponlist.get(position).getLogoUrl(),holder.image);
		if(couponlist.get(position).getOnOff().equals("1")){
			holder.used_or_not.setVisibility(View.INVISIBLE);
		}else{
			holder.used_or_not.setVisibility(View.VISIBLE);
		}
		return convertView;
	}
	static class ViewHolder {
		private ImageView image;
		private TextView name;
		private TextView businessName;
		private TextView endDate;
		private TextView price;
		private ImageView used_or_not;
	}
	public List<CouponInfo> getCouponlist() {
		return couponlist;
	}

	public void setCouponlist(List<CouponInfo> couponlist) {
		this.couponlist = couponlist;
	}

}
