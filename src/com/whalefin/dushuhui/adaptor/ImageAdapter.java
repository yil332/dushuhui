/**
 * @author yxw
 * date : 2013年10月9日 下午9:40:55 
 */
package com.whalefin.dushuhui.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;
import java.util.Map;


/**
 * @author yxw ImageAdapter用于异步加载图片
 */
public class ImageAdapter extends BaseAdapter {
    private List<Map<String, Object>> data;
    private Context context;
    private int layoutId;
    private String[] sNmaes;
    private int[] ids;
    private LayoutInflater inflater;
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    public ImageAdapter(Context context, List<Map<String, Object>> data,
                        int layoutId, String[] sNmaes, int[] ids) {
        this.context = context;
        this.data = data;
        this.layoutId = layoutId;
        this.sNmaes = sNmaes;
        this.ids = ids;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
        }

        for (int i = 0; i < sNmaes.length; i++) {

            View v = convertView.findViewById(ids[i]);
            Object dataTemp = data.get(position).get(sNmaes[i]);
            if (v instanceof Checkable) {
                if (dataTemp instanceof Boolean) {
                    ((Checkable) v).setChecked((Boolean) dataTemp);
                } else if (v instanceof TextView) {
                    ((TextView) v).setText((String) dataTemp);
                } else {
                    throw new IllegalStateException(v.getClass().getName()
                            + " should be bound to a Boolean, not a "
                            + (data == null ? "<unknown type>"
                            : data.getClass()));
                }

            } else if (v instanceof TextView) {
                ((TextView) v).setText(dataTemp + "");
            } else if (v instanceof ImageView) {
                if (dataTemp instanceof Integer) {
                    ((ImageView) v).setImageResource(Integer.parseInt(dataTemp
                            .toString()));
                } else {
                    try {
                        ((ImageView) v).setImageResource(Integer
                                .parseInt(dataTemp + ""));
                    } catch (NumberFormatException nfe) {
                        imageLoader.displayImage(
                                data.get(position).get(sNmaes[i]).toString(),
                                (ImageView) v);
                    }

                }
            } else {
                throw new IllegalStateException(v.getClass().getName()
                        + " is not a "
                        + " view that can be bounds by this SimpleAdapter");
            }
        }
        return convertView;
    }
}