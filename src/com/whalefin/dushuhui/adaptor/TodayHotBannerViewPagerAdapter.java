package com.whalefin.dushuhui.adaptor;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by tongwanglin on 14-4-2.
 */
public class TodayHotBannerViewPagerAdapter extends PagerAdapter {

    private Context context;
    private List<String> bannerImageUrls = null;

    public TodayHotBannerViewPagerAdapter(Context context, List<String> bannerImageUrls) {
        this.context = context;
        this.bannerImageUrls = bannerImageUrls;
    }

    @Override
    public int getCount() {
        return bannerImageUrls.size();
    }

    @Override
    public Object instantiateItem(View arg0, int arg1) {
        ImageView im = new ImageView(context);
        ImageLoader.getInstance().displayImage(bannerImageUrls.get(arg1), im);
        im.setScaleType(ImageView.ScaleType.FIT_XY);
        ((ViewPager) arg0).addView(im);
        return im;
    }

    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {

    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void startUpdate(View arg0) {

    }

    @Override
    public void finishUpdate(View arg0) {

    }
}