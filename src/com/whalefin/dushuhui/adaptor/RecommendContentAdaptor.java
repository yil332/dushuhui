package com.whalefin.dushuhui.adaptor;

import java.util.List;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.domain.RecommendContent;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RecommendContentAdaptor extends BaseExpandableListAdapter {
	private Context context;
	private List<RecommendContent> recomend;
	private LayoutInflater mInflater;
	private ImageLoader imageLoader;

	public RecommendContentAdaptor(Context context,
			List<RecommendContent> recomend) {
		super();
		this.context = context;
		this.recomend = recomend;
		mInflater = LayoutInflater.from(this.context);
		imageLoader = ImageLoader.getInstance();
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return recomend.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return recomend.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return recomend.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		TextView group = null;
		if (convertView == null) {
			convertView = mInflater.inflate(
					R.layout.list_item_booksrecomend_group, null);
			group = (TextView) convertView.findViewById(R.id.group);
			convertView.setTag(group);
		} else {
			group = (TextView) convertView.getTag();
		}
		group.setText(recomend.get(groupPosition).getTime());

		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_recommend_child,
					null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.rec_title);
			holder.image = (ImageView) convertView.findViewById(R.id.rec_image);
			holder.content = (TextView) convertView
					.findViewById(R.id.rec_content);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		imageLoader.displayImage(recomend.get(groupPosition).getImage(), holder.image);
		holder.name.setText(recomend.get(groupPosition).getTitle());
		Spanned spanned = Html.fromHtml(recomend.get(groupPosition).getContent());
		holder.content.setText(spanned);
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	static class ViewHolder {
		private ImageView image;
		private TextView name;
		private TextView content;

	}

}
