package com.whalefin.dushuhui.adaptor;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.domain.DailyRecomend;

import java.util.List;

/**
 * Created by tongwanglin on 14-4-2.
 */
public class RecomendBooksListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<DailyRecomend> recomends;
    private LayoutInflater mInflater;
    private ImageLoader imageLoader;
    private View mTopView;

    public RecomendBooksListAdapter(Context context,
                                    List<DailyRecomend> recomends) {
        this.context = context;
        this.recomends = recomends;
        // Cache the LayoutInflate to avoid asking for a new one each time.
        mInflater = LayoutInflater.from(context);
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getGroupCount() {
        return recomends.size() - 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return recomends.get(groupPosition).getBookInfoList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        return recomends.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return recomends.get(groupPosition).getBookInfoList()
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        TextView group = null;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.list_item_booksrecomend_group, null);
            group = (TextView) convertView.findViewById(R.id.group);
            convertView.setTag(group);
        } else {
            group = (TextView) convertView.getTag();
        }
        group.setText(recomends.get(groupPosition).getRecomandDate());

        return convertView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.list_item_booksrecomend_child, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            holder.author = (TextView) convertView.findViewById(R.id.author);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.recReson = (TextView) convertView
                    .findViewById(R.id.RecReason);
            holder.divider = (ImageView) convertView.findViewById(R.id.divider);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        imageLoader.displayImage(recomends.get(groupPosition).getBookInfoList()
                .get(childPosition).getImage(), holder.image);
        Spanned spannedName = Html.fromHtml(recomends.get(groupPosition).getBookInfoList()
                .get(childPosition).getName());
        holder.name.setText(spannedName);
        Spanned spannedAuthor = Html.fromHtml(recomends.get(groupPosition).getBookInfoList()
                .get(childPosition).getAuthor());
        holder.author.setText(spannedAuthor);
        holder.price.setText(recomends.get(groupPosition).getBookInfoList()
                .get(childPosition).getPrice()
                + "元");
        holder.recReson.setText(recomends.get(groupPosition).getBookInfoList()
                .get(childPosition).getRecomandReson());
        holder.recReson.setVisibility(View.INVISIBLE);
        if (childPosition == recomends.get(groupPosition).getBookInfoList().size() - 1) {
            holder.divider.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ViewHolder {
        private ImageView image;
        private TextView name;
        private TextView author;
        private TextView price;
        private TextView recReson;
        private ImageView divider;

    }
}
