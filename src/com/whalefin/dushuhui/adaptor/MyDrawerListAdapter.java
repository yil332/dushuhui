package com.whalefin.dushuhui.adaptor;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.whalefin.dushuhui.R;

public class MyDrawerListAdapter extends BaseAdapter {
    public int getSelect() {
		return select;
	}

	public void setSelect(int select) {
		this.select = select;
	}

	private Context mContext;
    private String[] mData;
    private int[] icon;
    private int select;

    public MyDrawerListAdapter(Context mContext, String[] mData, int[] icon) {
        this.mContext = mContext;
        this.mData = mData;
        this.icon = icon;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mData.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mData[position];
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            if (position==0) {
                convertView = LayoutInflater.from(mContext).inflate(
                        R.layout.drawer_list_item_login, null);
                holder.textview = (TextView) convertView.findViewById(R.id.title);
                holder.side = (ImageView)convertView.findViewById(R.id.side_jia);
                convertView.setTag(holder);
            } else {
                convertView = LayoutInflater.from(mContext).inflate(
                        R.layout.drawer_list_item, null);
                holder.textview = (TextView) convertView.findViewById(R.id.title);
                holder.side = (ImageView)convertView.findViewById(R.id.side);
                convertView.setTag(holder);

                
            }
            
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(position!=0){
        if(position==select){
        	holder.side.setVisibility(View.VISIBLE);
        	Log.e("red",String.valueOf(position)+"you");
        }else{
        	holder.side.setVisibility(View.GONE);
        	Log.e("red",String.valueOf(position)+"mei");
        }}
        holder.textview.setText(mData[position]);
        holder.textview.setCompoundDrawablesWithIntrinsicBounds(icon[position], 0, 0, 0);

        Log.i("tong", "------" + position + mData[position]);
        return convertView;
    }

    static class ViewHolder {
        private TextView textview;
        private ImageView side;
    }

    public String[] getmData() {
        return mData;
    }

    public void setmData(String[] mData) {
        this.mData = mData;
    }

}
