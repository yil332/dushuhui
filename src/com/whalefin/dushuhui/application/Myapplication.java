package com.whalefin.dushuhui.application;

import java.io.File;

import android.app.Application;

import com.nostra13.universalimageloader.cache.disc.impl.FileCountLimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.util.FileUtil;

/**
 * Created by tongwanglin on 14-4-1.
 */
public class Myapplication extends Application {
    private boolean isLogin = false;

    @Override
    public void onCreate() {
        super.onCreate();
        File cacheDir = new File(FileUtil.cacheDirectory);
        // ImageLoader配置项
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.book_default) // resource or drawable
                .showImageForEmptyUri(R.drawable.book_default) // resource or drawable
                .showImageOnFail(R.drawable.book_default) // resource or drawable
                .cacheInMemory(true).cacheOnDisc(true).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext()).denyCacheImageMultipleSizesInMemory()
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .discCacheSize(50 * 1024 * 1024).discCacheFileCount(100).discCache(new UnlimitedDiscCache(cacheDir))
                .build();
        ImageLoader.getInstance().init(config);
        
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean isLogin) {
        this.isLogin = isLogin;
    }

}
