/**
 * @author yxw
 * date : 2014年4月6日 下午7:13:56 
 */
package com.whalefin.dushuhui.util;

import android.os.Build;

import com.value.ebook.protobuf.AppAboutUsProtos;
import com.value.ebook.protobuf.AppAboutUsProtos.AppAboutUs;
import com.value.ebook.protobuf.AppSetProtos;
import com.value.ebook.protobuf.AppSetProtos.AppSet;
import com.value.ebook.protobuf.AppVersionsProtos;
import com.value.ebook.protobuf.AppVersionsProtos.AppVersions;
import com.value.ebook.protobuf.EveryDayRecommendPageProtos;
import com.value.ebook.protobuf.EveryDayRecommendPageProtos.EveryDayRecommendPage.EveryDay;
import com.value.ebook.protobuf.IndexPageProtos;
import com.value.ebook.protobuf.IndexPageProtos.IndexPage;
import com.value.ebook.protobuf.MemberCouponProtos;
import com.value.ebook.protobuf.MemberCouponProtos.MemberCoupon;
import com.value.ebook.protobuf.MemberGetCouponProtos;
import com.value.ebook.protobuf.MemberPasswordProtos;
import com.value.ebook.protobuf.MemberUserInfoModifyProtos;
import com.value.ebook.protobuf.MemberUserInfoProtos;
import com.value.ebook.protobuf.MemberUserInfoProtos.MemberUserInfo;
import com.value.ebook.protobuf.MemberUserProtos;
import com.value.ebook.protobuf.MemberUserProtos.MemberUser;
import com.value.ebook.protobuf.MemberUserRegistProtos.MemberUserRegist;
import com.value.ebook.protobuf.MyEbookProtos;
import com.value.ebook.protobuf.MyEbookProtos.MyEbook.Book;
import com.value.ebook.protobuf.ReturnProtos;
import com.value.ebook.protobuf.ReturnProtos.Return;
import com.value.ebook.protobuf.ShortMessagesProtos;
import com.value.ebook.protobuf.VipProtos;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class NetworkUtil {
    public static final String baseUrl = "http://112.4.28.82:8080/ebook";

    public static InputStream connect(String url, byte[] content)
            throws IOException {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        URL targetUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) targetUrl
                .openConnection();

        connection.setDoOutput(true);
        if (Build.VERSION.SDK != null && Build.VERSION.SDK_INT > 13) {
            connection.setRequestProperty("Connection", "close");
        }
        connection.setDoInput(true);
        connection.setRequestProperty("Content-Type",
                "application/x-protobuf");
        connection.setRequestProperty("Accept", "application/x-protobuf");
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connect-Length",
                Integer.toString(content.length));
        connection.setFixedLengthStreamingMode(content.length);
        outputStream = connection.getOutputStream();
        outputStream.write(content);
        outputStream.flush();
        // check response code
        int code = connection.getResponseCode();
        boolean success = (code >= 200) && (code < 300);
        inputStream = success ? connection.getInputStream() : connection
                .getErrorStream();
        return inputStream;
    }

    public static String login(String userName, String passWord)
            throws Exception {
//		String url = "http://202.91.235.102:8221/ebook/ws/login.pb";
        String url = baseUrl + "/ws/login.pb";
        // String url = "http://192.168.0.137:8221/ebook/ws/login.pb";
        MemberUser memberUser = MemberUser.newBuilder().setUserName(userName)
                .setPasswrod(passWord)
                .setType(MemberUser.MemberUserType.MEMBER).build();
        byte[] content = memberUser.toByteArray();
        InputStream in;
        MemberUser user = null;
        try {
            in = connect(url, content);
            user = MemberUser.parseFrom(in);
            in.close();
            // System.out.println(user.getToken(0).getToken());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return user.getToken(0).getToken().toString();
    }

    public static MemberCoupon getCoupon() throws Exception {
        String token = ApplicationParam.token;
//		String url = "http://202.91.235.102:8221/ebook/ws/getCouponProtos.pb";
        String url = baseUrl + "/ws/getCouponProtos.pb";
        // String url = "http://192.168.0.137:8221/ebook/ws/getCouponProtos.pb";
        MemberGetCouponProtos.MemberGetCoupon memberGetCoupon = MemberGetCouponProtos.MemberGetCoupon
                .newBuilder()
                .setType(
                        MemberGetCouponProtos.MemberGetCoupon.MemberUserType.MEMBER)
                .setIsVip(
                        MemberGetCouponProtos.MemberGetCoupon.MemberUserVIPType.VIP)
                .addToken(
                        MemberGetCouponProtos.MemberGetCoupon.MemberToken
                                .newBuilder().setToken(token).build()
                ).build();
        byte[] content = memberGetCoupon.toByteArray();
        InputStream in = connect(url, content);
        MemberCouponProtos.MemberCoupon coupon = MemberCouponProtos.MemberCoupon
                .parseFrom(in);
        in.close();
        return coupon;
    }

    public static String testGetMassage(String userName) throws Exception {
//		String url = "http://202.91.235.102:8221/ebook/ws/getMassage.pb";
        String url = baseUrl + "/ws/getMassage.pb";
        // String url = "http://192.168.0.137:8221/ebook/ws/getMassage.pb";
        ShortMessagesProtos.ShortMessages shortMessages = ShortMessagesProtos.ShortMessages
                .newBuilder().setMobile(userName).build();
        byte[] content = shortMessages.toByteArray();
        InputStream in = connect(url, content);
        ShortMessagesProtos.ShortMessages s = ShortMessagesProtos.ShortMessages
                .parseFrom(in);
        in.close();
        return s.getCode();
    }

    public static String memberRegister(String username, String verify_code,
                                        String password) throws IOException {
        MemberUserRegist memberUser = MemberUserRegist
                .newBuilder()
                .setUserName(username)
                .setCode(verify_code)
                .setType(MemberUserRegist.MemberUserType.MEMBER)
                .addInfo(
                        MemberUserRegist.MemberInfo.newBuilder()
                                .setPassword(password)
                                .setConfirmPassword(password).build()
                ).build();
        byte[] content = memberUser.toByteArray();
//		String url = "http://202.91.235.102:8221/ebook/ws/register.pb";
        String url = baseUrl + "/ws/register.pb";
        // String url = "http://192.168.0.137:8221/ebook/ws/register.pb";
        InputStream in = connect(url, content);
        MemberUserProtos.MemberUser user = MemberUserProtos.MemberUser
                .parseFrom(in);
        in.close();
        return user.getToken(0).getToken().toString();

    }

    public static List<EveryDay> testEveryDay() throws Exception {
//		String url = "http://202.91.235.102:8221/ebook/ws/everyDay.pb";
        String url = baseUrl + "/ws/everyDay.pb";
        // String url = "http://localhost:8080/ebook/ws/everyDay.pb";
        EveryDayRecommendPageProtos.EveryDayRecommendPage every = EveryDayRecommendPageProtos.EveryDayRecommendPage
                .newBuilder().build();
        byte[] content = every.toByteArray();

        InputStream in = connect(url, content);
        EveryDayRecommendPageProtos.EveryDayRecommendPage page = EveryDayRecommendPageProtos.EveryDayRecommendPage
                .parseFrom(in);
        in.close();
        // System.out.println("实体书籍");
        // System.out.println(page.getEveryDayRecommendList());
        return page.getEveryDayRecommendList();
    }

    public static List<EveryDay> testEveryDayNews() throws Exception {
//		String url = "http://202.91.235.102:8221/ebook/ws/everyDay.pb";
        String url = baseUrl + "/ws/everyDay.pb";
        // String url = "http://localhost:8080/ebook/ws/everyDay.pb";
        EveryDayRecommendPageProtos.EveryDayRecommendPage every = EveryDayRecommendPageProtos.EveryDayRecommendPage
                .newBuilder().build();
        byte[] content = every.toByteArray();

        InputStream in = connect(url, content);
        EveryDayRecommendPageProtos.EveryDayRecommendPage page = EveryDayRecommendPageProtos.EveryDayRecommendPage
                .parseFrom(in);
        in.close();
        // System.out.println("实体书籍");
        System.out.println(page.getEveryDayRecommendList());
//		Log.e("5.27",page.getOneMessageRecommendList().toString());
        return page.getOneMessageRecommendList();
    }

    public static String testVIP(String username) throws Exception {
        String token = ApplicationParam.token;
//		String url = "http://202.91.235.102:8221/ebook/ws/vip.pb";
        String url = baseUrl + "/ws/vip.pb";
        VipProtos.Vip vip = VipProtos.Vip
                .newBuilder()
                .setMobile(username)
                .addToken(
                        VipProtos.Vip.MemberToken.newBuilder().setToken(token))
                .build();
        byte[] content = vip.toByteArray();
        InputStream in = connect(url, content);
        VipProtos.Vip returnState = VipProtos.Vip.parseFrom(in);
        in.close();
        // System.out.println(returnState);
        return returnState.getCode();
    }

    public static AppAboutUs testappAboutUs() throws Exception {
//		String url = "http://202.91.235.102:8221/ebook/ws/appAboutUs.pb";
        String url = baseUrl + "/ws/appAboutUs.pb";
        AppAboutUsProtos.AppAboutUs appAboutUs = AppAboutUsProtos.AppAboutUs
                .newBuilder().setName("读书汇").setVersions("1.0")
                .setDevice(AppAboutUsProtos.AppAboutUs.AppDevice.ANDROID)
                .build();
        byte[] content = appAboutUs.toByteArray();
        InputStream in = connect(url, content);
        AppAboutUsProtos.AppAboutUs page = AppAboutUsProtos.AppAboutUs
                .parseFrom(in);
        in.close();
        // System.out.println(page);
        // System.out.println(page.getName());
        // System.out.println(page.getRemark());
        // System.out.println(page.getReturnMessage());
        return page;
        // System.out.println("实体书籍");

    }

    public static AppVersions testappVersion() throws Exception {
//		String url = "http://202.91.235.102:8221/ebook/ws/appVersion.pb";
        String url = baseUrl + "/ws/appVersion.pb";
        AppVersionsProtos.AppVersions appVersions = AppVersionsProtos.AppVersions
                .newBuilder().setAppPakage("com.dushuhui.com").setCount(1)
                .setName("读书汇").setVersions("1.0")
                .setDevice(AppVersionsProtos.AppVersions.AppDevice.ANDROID)
                .build();
        byte[] content = appVersions.toByteArray();
        InputStream in = connect(url, content);
        AppVersionsProtos.AppVersions page = AppVersionsProtos.AppVersions
                .parseFrom(in);
        in.close();

        // System.out.println(page);
        return page;
    }

    public static IndexPage testIndex() throws Exception {
//		String url = "http://202.91.235.102:8221/ebook/ws/index.pb";
        String url = baseUrl + "/ws/index.pb";
        IndexPageProtos.IndexPage indexPage = IndexPageProtos.IndexPage
                .newBuilder().build();
        byte[] content = indexPage.toByteArray();
        InputStream in = connect(url, content);
        IndexPageProtos.IndexPage page = IndexPageProtos.IndexPage
                .parseFrom(in);
        in.close();
        // System.out.println("实体书籍");
        // System.out.println(page.getBookList());
        // System.out.println("电子书籍");
        // System.out.println(page.getEbooksList());
        // System.out.println("礼包书籍");
        // System.out.println(page.getGifsList());
        // System.out.println("广告");
        // System.out.println(page.getAdvertisementsList());
        return page;
    }

    public static MemberUserInfo testMemberInfo() throws Exception {
        String token = ApplicationParam.token;
//		String url = "http://202.91.235.102:8221/ebook/ws/memberInfo.pb";
        String url = baseUrl + "/ws/memberInfo.pb";
        MemberUserInfoProtos.MemberUserInfo memberInfo = MemberUserInfoProtos.MemberUserInfo
                .newBuilder()
                .setType(
                        MemberUserInfoProtos.MemberUserInfo.MemberUserType.MEMBER)
                .addToken(
                        MemberUserInfoProtos.MemberUserInfo.MemberToken
                                .newBuilder().setToken(token).build()
                )
                .setIsVip(
                        MemberUserInfoProtos.MemberUserInfo.MemberUserVIPType.COMMON)
                .setUserName("18768116036").build();
        byte[] content = memberInfo.toByteArray();
        InputStream in = connect(url, content);
        MemberUserInfoProtos.MemberUserInfo memberUserInfo = MemberUserInfoProtos.MemberUserInfo
                .parseFrom(in);
        in.close();
        // System.out.println(memberUserInfo);
        return memberUserInfo;
        // Assert.assertNotNull(memberUserInfo);
    }

    public static AppSet testGetMemberSet() throws Exception {
        String token = ApplicationParam.token;
//		String url = "http://202.91.235.102:8221/ebook/ws/getMemberSet.pb";
        String url = baseUrl + "/ws/getMemberSet.pb";
        AppSetProtos.AppSet memberSet = AppSetProtos.AppSet
                .newBuilder()
                .setType(AppSetProtos.AppSet.MemberUserType.MEMBER)
                .setPushSet(0)
                .setRingSet(0)
                .setWifiPic(0)
                .addToken(
                        AppSetProtos.AppSet.MemberToken.newBuilder()
                                .setToken(token).build()
                ).build();
        byte[] content = memberSet.toByteArray();
        InputStream in = connect(url, content);
        AppSetProtos.AppSet appSet = AppSetProtos.AppSet.parseFrom(in);
        in.close();
        // System.out.println(appSet);
        return appSet;
    }

    public static Return testMemberSet(int PushSet, int RingSet, int WifiPic)
            throws Exception {
        String token = ApplicationParam.token;
//		String url = "http://202.91.235.102:8221/ebook/ws/memberSet.pb";
        String url = baseUrl + "/ws/memberSet.pb";
        AppSetProtos.AppSet memberSet = AppSetProtos.AppSet
                .newBuilder()
                .setPushSet(PushSet)
                .setRingSet(RingSet)
                .setWifiPic(WifiPic)
                .setType(AppSetProtos.AppSet.MemberUserType.MEMBER)
                .addToken(
                        AppSetProtos.AppSet.MemberToken.newBuilder()
                                .setToken(token).build()
                ).build();
        byte[] content = memberSet.toByteArray();
        InputStream in = connect(url, content);
        ReturnProtos.Return returnState = ReturnProtos.Return.parseFrom(in);
        in.close();
        // System.out.println(returnState);
        return returnState;
    }

    public static Return testModifyPwd(String _old, String _new)
            throws Exception {
        String token = ApplicationParam.token;
//		String url = "http://202.91.235.102:8221/ebook/ws/modifyPwd.pb";
        String url = baseUrl + "/ws/modifyPwd.pb";
        MemberPasswordProtos.MemberPassword memberPassword = MemberPasswordProtos.MemberPassword
                .newBuilder()
                .setOldPassword(_old)
                .setNewPassword(_new)
                .setType(
                        MemberPasswordProtos.MemberPassword.MemberUserType.MEMBER)
                .addToken(
                        MemberPasswordProtos.MemberPassword.MemberToken
                                .newBuilder().setToken(token).build()
                ).build();
        byte[] content = memberPassword.toByteArray();
        InputStream in = connect(url, content);
        ReturnProtos.Return returnState = ReturnProtos.Return.parseFrom(in);
        in.close();
        System.out.println(returnState);
        return returnState;
    }

    public static String modifyNickName(String nickName) throws Exception {
        String url = "http://112.4.28.82:8080/ebook/ws/memberInfoModify.pb";
        MemberUserInfoModifyProtos.MemberUserInfoModify memberInfo = MemberUserInfoModifyProtos.MemberUserInfoModify.newBuilder()
                .setType(MemberUserInfoModifyProtos.MemberUserInfoModify.ModifyType.NICK)
                .setData(nickName)
                .addToken(MemberUserInfoModifyProtos.MemberUserInfoModify.MemberToken.newBuilder()
                        .setToken(ApplicationParam.token)
                        .build())
                .build();
        byte[] content = memberInfo.toByteArray();
        InputStream in = connect(url, content);
        MemberUserInfoModifyProtos.MemberUserInfoModify r = MemberUserInfoModifyProtos.MemberUserInfoModify.parseFrom(in);
        in.close();
        return r.getTokenCode();

    }

    public static String modifyQQ(String qq) throws Exception {
        String url = "http://112.4.28.82:8080/ebook/ws/memberInfoModify.pb";
        MemberUserInfoModifyProtos.MemberUserInfoModify memberInfo = MemberUserInfoModifyProtos.MemberUserInfoModify.newBuilder()
                .setType(MemberUserInfoModifyProtos.MemberUserInfoModify.ModifyType.QQ)
                .setData(qq)
                .addToken(MemberUserInfoModifyProtos.MemberUserInfoModify.MemberToken.newBuilder()
                        .setToken(ApplicationParam.token)
                        .build())
                .build();
        byte[] content = memberInfo.toByteArray();
        InputStream in = connect(url, content);
        MemberUserInfoModifyProtos.MemberUserInfoModify r = MemberUserInfoModifyProtos.MemberUserInfoModify.parseFrom(in);
        in.close();
        return r.getTokenCode();

    }

    public static String modifyEmail(String email) throws Exception {
        String url = "http://112.4.28.82:8080/ebook/ws/memberInfoModify.pb";
        MemberUserInfoModifyProtos.MemberUserInfoModify memberInfo = MemberUserInfoModifyProtos.MemberUserInfoModify.newBuilder()
                .setType(MemberUserInfoModifyProtos.MemberUserInfoModify.ModifyType.EMAIL)
                .setData(email)
                .addToken(MemberUserInfoModifyProtos.MemberUserInfoModify.MemberToken.newBuilder()
                        .setToken(ApplicationParam.token)
                        .build())
                .build();
        byte[] content = memberInfo.toByteArray();
        InputStream in = connect(url, content);
        MemberUserInfoModifyProtos.MemberUserInfoModify r = MemberUserInfoModifyProtos.MemberUserInfoModify.parseFrom(in);
        in.close();
        return r.getTokenCode();

    }

    public static List<Book> testMYEBOOK() throws Exception {
        String token = ApplicationParam.token;
        String url = "http://112.4.28.82:8080/ebook/ws/myEbook.pb";
        MyEbookProtos.MyEbook ebook = MyEbookProtos.MyEbook.newBuilder().addToken(MyEbookProtos.MyEbook.MemberToken.newBuilder().setToken(token)).build();
        byte[] content = ebook.toByteArray();
        InputStream in = connect(url, content);
        MyEbookProtos.MyEbook page =
                MyEbookProtos.MyEbook.parseFrom(in);
        in.close();

//        System.out.println(page.getEbooksList());
        return page.getEbooksList();
    }

}
