package com.whalefin.dushuhui.util;

import java.io.File;
import java.text.DecimalFormat;

import com.whalefin.dushuhui.domain.InstallInfo;

import net.tsz.afinal.FinalDb;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class FileUtil {
	public static final String sdCardRoot = Environment
			.getExternalStorageDirectory().getAbsolutePath();
	public static final String applicationDirectory = sdCardRoot
			+ "/onlineBookStore";
	public static final String downloadDirectory = applicationDirectory
			+ "/download";
	public static final String cacheDirectory = applicationDirectory + "/cache";
	public static final String updateDirectory = applicationDirectory+"/update";

	public static void initDirectory(Context context) {
		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {
			String path = downloadDirectory;
			File dir = new File(path);
			FinalDb db = FinalDb.create(context);
			if (!dir.exists()) {
				dir.mkdirs();
				InstallInfo ii = new InstallInfo();
				ii.setId("install");
				db.save(ii);
				Log.v("file", dir.getPath());

			} else {
				if(db.findAll(InstallInfo.class).size()<1){
					deleSDFolder(dir);
					dir.mkdirs();
					InstallInfo ii = new InstallInfo();
					ii.setId("install");
					db.save(ii);
				}
				Log.v("file", "File already there!");
			}
		} else {
			Log.v("file", "error!");
		}
	}

	public static long getFileSize(File f) throws Exception {
		long size = 0;
		File flist[] = f.listFiles();
		for (int i = 0; i < flist.length; i++) {
			if (flist[i].isDirectory()) {
				size = size + getFileSize(flist[i]);
			} else {
				size = size + flist[i].length();
			}
		}
		return size;
	}

	public static String FormetFileSize(long fileS) {

		DecimalFormat df = new DecimalFormat("#.00");

		String fileSizeString = "";

		if (fileS < 1024) {

			fileSizeString = df.format((double) fileS) + "B";

		} else if (fileS < 1048576) {

			fileSizeString = df.format((double) fileS / 1024) + "K";

		} else if (fileS < 1073741824) {

			fileSizeString = df.format((double) fileS / 1048576) + "M";

		} else {

			fileSizeString = df.format((double) fileS / 1073741824) + "G";

		}

		return fileSizeString;

	}

	public static void deleSDFolder(File file) {
		if (file.exists()) {
			if (file.listFiles().length > 0) {
				for (File temp : file.listFiles()) {
					if (temp.isDirectory()) {
						deleSDFolder(temp);
					} else {
						temp.delete();
					}
				}
			}
			file.delete();
		}
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
