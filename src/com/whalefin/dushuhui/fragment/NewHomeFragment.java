package com.whalefin.dushuhui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.value.ebook.protobuf.EveryDayRecommendPageProtos.EveryDayRecommendPage.EveryDay;
import com.value.ebook.protobuf.IndexPageProtos.IndexPage;
import com.viewpagerindicator.UnderlinePageIndicator;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.adaptor.RecomendBooksListAdapter;
import com.whalefin.dushuhui.adaptor.TodayHotBannerViewPagerAdapter;
import com.whalefin.dushuhui.base.DrawerLayoutActivity;
import com.whalefin.dushuhui.domain.BookInfo;
import com.whalefin.dushuhui.domain.DailyRecomend;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.view.myViewpager;

import java.util.ArrayList;
import java.util.List;

public class NewHomeFragment extends Fragment implements ViewPager.OnPageChangeListener {
    private View topView;
    private View thisView;
    private List<BookInfo> realBook;
    private List<BookInfo> digitalBook;
    private IndexPage ip;
    private RadioGroup navis;
    private RecomendBooksListAdapter thisAdaptor;

    private ExpandableListView thisList;
    private List<DailyRecomend> thisData;

    private ViewPager banner;
    private TextView content;
    private TextView time;
    private TextView title;
    private List<String> bannerUrl;
    private List<String> bannerTitle;
    private List<String> bannerContent;
    private List<String> bannerDate;
    private List<EveryDay> everyDay;
    private UnderlinePageIndicator indicator;
    private LinearLayout ly;
    private Context context;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        realBook = new ArrayList<BookInfo>();
        digitalBook = new ArrayList<BookInfo>();
        thisData = new ArrayList<DailyRecomend>();
        new Thread(getMessage).start();
        new Thread(getData).start();
        thisList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        thisList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri url1 = Uri.parse(thisData.get(groupPosition).getBookInfoList().get(childPosition).getUrl());
                intent.setData(url1);
                startActivity(intent);
                return true;
            }
        });
        navis.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (group.getCheckedRadioButtonId()) {
                    case R.id.real:
                        if (thisList.getAdapter() != null) {
                            thisList.setSelectedGroup(0);
                        }
                        break;
                    case R.id.digital:
                        if (thisList.getAdapter() != null) {
                            thisList.setSelectedGroup(1);
                        }
                        break;
                }
            }
        });
        indicator.setOnPageChangeListener(this);
        banner.setOnPageChangeListener(this);
        bannerDate = new ArrayList<String>();
        bannerContent = new ArrayList<String>();
        bannerTitle = new ArrayList<String>();
        bannerUrl = new ArrayList<String>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        thisView = inflater.inflate(R.layout.layout_newhome, null);
        navis = (RadioGroup) thisView.findViewById(R.id.home_btns);
        topView = inflater.inflate(R.layout.layout_hometopview, null);
        ly = (LinearLayout) thisView.findViewById(R.id.bottom_btn_view);

        banner = (myViewpager) topView.findViewById(R.id.topview_banner);
        content = (TextView) topView.findViewById(R.id.bannerContent);
        time = (TextView) topView.findViewById(R.id.bannerDate);
        indicator = (UnderlinePageIndicator) topView.findViewById(R.id.bannerIndictor);
        title = (TextView) topView.findViewById(R.id.bannerTitle);
        thisList = (ExpandableListView) thisView.findViewById(R.id.itemlist);
        thisList.addHeaderView(topView);
        DrawerLayoutActivity fca = (DrawerLayoutActivity) context;
        fca.getActionBarRightButton().setVisibility(View.GONE);
        return thisView;
    }

    Runnable getData = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                if (!NetworkUtil.testIndex().equals(null)) {
                    ip = NetworkUtil.testIndex();
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = ip;
            handler.sendMessage(msg);
        }

    };
    Runnable getMessage = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                if (NetworkUtil.testEveryDayNews() != null) {
                    everyDay = NetworkUtil.testEveryDayNews();
                    Log.e("5.27","number"+everyDay.size());
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 2;
            msg.obj = everyDay;
            handler.sendMessage(msg);

        }

    };
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            try {
                int height = ly.getMeasuredHeight();
                Log.e("height", "button" + String.valueOf(height));
                int contentTop = getActivity().getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
                Log.e("height", "content" + String.valueOf(contentTop));
                WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
                AbsListView.LayoutParams lp = new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, wm.getDefaultDisplay().getHeight() - height - contentTop);
                topView.setLayoutParams(lp);
            } catch (Exception e) {

            }
            switch (msg.arg1) {
                case 1:
                    try {
                        ip = (IndexPage) msg.obj;
                        DailyRecomend dr1 = new DailyRecomend();
                        DailyRecomend dr2 = new DailyRecomend();
                        for (int i = 0; i < ip.getBookList().size(); i++) {
                            BookInfo bi = new BookInfo();
                            bi.setAuthor(ip.getBookList().get(i).getAuthor());
                            bi.setContent(ip.getBookList().get(i).getRecommend());
                            bi.setPrice(Float.valueOf(ip.getBookList().get(i)
                                    .getPrice()));
                            bi.setImage(ip.getBookList().get(i).getImage());
                            bi.setUrl(ip.getBookList().get(i).getBookDetailUrl());
                            bi.setName(ip.getBookList().get(i).getBookName());
                            realBook.add(bi);
                        }
                        dr1.setBookInfoList(realBook);
                        dr1.setRecomandDate("实体书推荐");
                        for (int i = 0; i < ip.getEbooksList().size(); i++) {
                            BookInfo bi = new BookInfo();
                            bi.setAuthor(ip.getEbooksList().get(i).getAuthor());
                            bi.setContent(ip.getEbooksList().get(i).getRecommend());
                            bi.setPrice(Float.valueOf(ip.getEbooksList().get(i)
                                    .getPrice()));
                            bi.setImage(ip.getEbooksList().get(i).getImage());
                            bi.setUrl(ip.getEbooksList().get(i).getBookDetailUrl());
                            bi.setName(ip.getEbooksList().get(i).getBookName());
                            digitalBook.add(bi);
                        }
                        dr2.setBookInfoList(digitalBook);
                        dr2.setRecomandDate("电子书推荐");
                        thisData.add(dr1);
                        thisData.add(dr2);
                        thisAdaptor = new RecomendBooksListAdapter(context, thisData);
                        thisList.setAdapter(thisAdaptor);
                        int groupCount = thisList.getCount();
                        for (int i = 0; i < groupCount; i++) {
                            thisList.expandGroup(i);
                        }
                    } catch (NullPointerException e) {
                        Toast.makeText(context, "网络连接错误", Toast.LENGTH_SHORT).show();
                        // TODO: handle exception
                    }
                    break;
                case 2:
                    try {
                        everyDay = (List<EveryDay>) msg.obj;
                        Log.e("5.27","number"+everyDay.size());
                        for (int i = 0; i < everyDay.size(); i++) {
                            bannerUrl.add(everyDay.get(i).getImage());
                            Log.e("5.27","everyDay.get(i).getImage()");
                            bannerTitle.add(everyDay.get(i).getTitle());
                            bannerContent.add(everyDay.get(i).getContent());
                            bannerDate.add(everyDay.get(i).getTime());
                        }
                        banner.setAdapter(new TodayHotBannerViewPagerAdapter(context, bannerUrl));
                        banner.setCurrentItem(0);
                        if(bannerContent.size()>0){
                        Spanned spanned = Html.fromHtml(bannerContent.get(0));
                        content.setText(spanned);
                        }else{
                        	content.setText("");
                        }
                        if(bannerTitle.size()>0){
                        title.setText(bannerTitle.get(0));
                        }else{
                        	title.setText("");
                        }
                        if(bannerDate.size()>0){
                        time.setText(bannerDate.get(0));
                        }else{
                        	time.setText("");
                        }
                        indicator.setViewPager(banner);
                        indicator.setSelectedColor(context.getResources().getColor(R.color.pink));
                        indicator.setBackgroundColor(context.getResources().getColor(R.color.today_hot_banner_indictor));
                        indicator.setFades(false);
                    } catch (NullPointerException e) {
                        Toast.makeText(context, "网络连接错误", Toast.LENGTH_SHORT).show();
                        // TODO: handle exception
                    }
                    break;
            }
        }

    };

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int arg0) {
        // TODO Auto-generated method stub
        Spanned spanned = Html.fromHtml(bannerContent.get(arg0));
        content.setText(spanned);
        title.setText(bannerTitle.get(arg0));
        time.setText(bannerDate.get(arg0));
    }
}
