package com.whalefin.dushuhui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.value.ebook.protobuf.MemberUserInfoProtos.MemberUserInfo;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.activity.CouponsActivity;
import com.whalefin.dushuhui.activity.LoginActivity;
import com.whalefin.dushuhui.activity.ModifyEmailActivity;
import com.whalefin.dushuhui.activity.ModifyNickNameActivity;
import com.whalefin.dushuhui.activity.ModifyPassActivity;
import com.whalefin.dushuhui.activity.ModifyQQActivity;
import com.whalefin.dushuhui.adaptor.MyDrawerListAdapter;
import com.whalefin.dushuhui.base.DrawerLayoutActivity;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.util.PreferencesUtils;

public class MyCenterFragment extends Fragment {
    private Context context;
    private View thisView;
    private TextView nickname;
    private TextView username;
    private TextView qq;
    private TextView email;
    private TextView coupons;
    private ImageView touxiang;
    private TextView vip;

    private String nick;
    private String q;
    private String mail;

    private String userName;
    private String passWord;

    private RelativeLayout rl1;
    private RelativeLayout rl2;
    private RelativeLayout rl3;
    private RelativeLayout rl4;
    private RelativeLayout rl5;
    private RelativeLayout center_coupons;

    private MemberUserInfo mui;

    private Boolean processing = false;

    private ImageView arrow;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        thisView = inflater.inflate(R.layout.layout_mycenter, null);
        nickname = (TextView) thisView.findViewById(R.id.center_nickname_value);
        username = (TextView) thisView.findViewById(R.id.center_username_value);
        qq = (TextView) thisView.findViewById(R.id.center_qq_value);
        email = (TextView) thisView.findViewById(R.id.center_email_value);
        touxiang = (ImageView) thisView.findViewById(R.id.center_touxiang_pic);
        vip = (TextView) thisView.findViewById(R.id.center_vip_value);
        arrow = (ImageView) thisView.findViewById(R.id.arrow5);
        coupons = (TextView) thisView.findViewById(R.id.my_coupons);

        rl1 = (RelativeLayout) thisView.findViewById(R.id.center_nickname);
        rl2 = (RelativeLayout) thisView.findViewById(R.id.center_email);
        rl3 = (RelativeLayout) thisView.findViewById(R.id.center_qq);
        rl4 = (RelativeLayout) thisView.findViewById(R.id.center_password);
        rl5 = (RelativeLayout) thisView.findViewById(R.id.center_vip);
        center_coupons = (RelativeLayout) thisView.findViewById(R.id.center_coupons);

        ((DrawerLayoutActivity) context).getActionBarRightButton().setText(R.string.log_out);
        ((DrawerLayoutActivity) context).getActionBarRightButton().setVisibility(View.VISIBLE);
        ((DrawerLayoutActivity) context).getActionBarRightButton().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ApplicationParam.isLogin = false;
                PreferencesUtils.putString(context, "password", "");
                ((DrawerLayoutActivity) context).selectItem(1);
                ((DrawerLayoutActivity) context).mTitles[0] = "未登录";
                ((DrawerLayoutActivity) context).drawerListAdapter = new MyDrawerListAdapter(
                        context, ((DrawerLayoutActivity) context).mTitles, ((DrawerLayoutActivity) context).icon
                );
                ((DrawerLayoutActivity) context).mDrawerList.setAdapter(((DrawerLayoutActivity) context).drawerListAdapter);
            }
        });
        return thisView;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        context = activity;
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        userName = PreferencesUtils.getString(context, "username");
        passWord = PreferencesUtils.getString(context, "password");
        new Thread(getMyInfo).start();
        rl1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!processing) {
                    Intent intent = new Intent(context, ModifyNickNameActivity.class);
                    intent.putExtra("nickname", nick);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "正在加载用户数据,请稍候!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        rl2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!processing) {
                    Intent intent = new Intent(context, ModifyEmailActivity.class);
                    intent.putExtra("email", mail);
                    startActivity(intent);
                }
            }
        });
        rl3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!processing) {
                    Intent intent = new Intent(context, ModifyQQActivity.class);
                    intent.putExtra("qq", q);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "正在加载用户数据,请稍候!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        rl4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!processing) {
                    Intent intent = new Intent(context, ModifyPassActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "正在加载用户数据,请稍候!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        rl5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!processing) {
                    if (ApplicationParam.VIP) {
                        Toast.makeText(context, "您已经是VIP了", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "请期待开通付费VIP", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "正在加载用户数据,请稍候!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        center_coupons.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!processing) {
                    Intent intent = new Intent(context, CouponsActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "正在加载用户数据,请稍候!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        super.onActivityCreated(savedInstanceState);
    }

    Runnable getMyInfo = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                ApplicationParam.token = NetworkUtil.login(userName, passWord);
                mui = NetworkUtil.testMemberInfo();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = mui;
            handler.sendMessage(msg);
        }

    };
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.arg1) {
                case 1:
                    MemberUserInfo thisMemberInfo;
                    thisMemberInfo = (MemberUserInfo) msg.obj;
                    mail = thisMemberInfo.getEmail();
                    email.setText(mail);
                    q = thisMemberInfo.getQq();
                    qq.setText(q);
                    username.setText(thisMemberInfo.getUserName());
                    nick = thisMemberInfo.getNick();
                    nickname.setText(nick);
                    ImageLoader.getInstance().displayImage(
                            thisMemberInfo.getHeadPortrait(), touxiang);
                    if (thisMemberInfo.getIsVip().toString().equals("VIP")) {
                        ApplicationParam.VIP = true;
                        vip.setText("您已经是vip!");
                        arrow.setVisibility(View.INVISIBLE);
                    } else {
                        ApplicationParam.VIP = false;
                        vip.setText("成为vip");
                        arrow.setVisibility(View.VISIBLE);
                    }
                    processing = false;
                    break;
            }
        }


    };

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (!ApplicationParam.isLogin) {
            Toast.makeText(context, "未登录", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(context, LoginActivity.class);
            ((Activity) context).startActivityForResult(intent, 1);
        } else {
            userName = PreferencesUtils.getString(context, "username");
            passWord = PreferencesUtils.getString(context, "password");
            new Thread(getMyInfo).start();
        }
    }


}
