package com.whalefin.dushuhui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.activity.BookDetail;
import com.whalefin.dushuhui.activity.BookListActivity;
import com.whalefin.dushuhui.activity.BookShelfActivity;
import com.whalefin.dushuhui.adaptor.BookItemListAdapter;
import com.whalefin.dushuhui.base.DrawerLayoutActivity;
import com.whalefin.dushuhui.domain.BookInfo;
import com.whalefin.dushuhui.util.HtmlUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class HandLibraryFragment extends Fragment {

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        context = activity;
    }

    private ImageView iv;
    private View thisView;
    private ListView bookItemList;
    private RadioGroup navis;
    private List<BookInfo> categoryinfo;
    private List<BookInfo> recinfo;
    private List<BookInfo> hotinfo;
    private List<BookInfo> latestinfo;
    private BookItemListAdapter thisAdapter;
    private boolean isDisplayingCategory;
    private int flag = 1;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        thisView = inflater.inflate(R.layout.layout_library, null);
        initview();
        DrawerLayoutActivity fca = (DrawerLayoutActivity) context;
        fca.getActionBarRightButton().setVisibility(View.VISIBLE);
        fca.getActionBarRightButton().setText(R.string.book_shelf);
        fca.getActionBarRightButton().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(context, BookShelfActivity.class);
                startActivity(intent);
            }
        });
        return thisView;
    }

    private void initview() {
        // TODO Auto-generated method stub
        iv = (ImageView) thisView.findViewById(R.id.imageView1);

        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        bookItemList = (ListView) thisView.findViewById(R.id.bookitemlist);
        bookItemList.setDividerHeight(0);
        navis = (RadioGroup) thisView.findViewById(R.id.navi_btns);
        navis.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (group.getCheckedRadioButtonId()) {
                    case R.id.Button1:
                        isDisplayingCategory = false;

                        flag = 1;
                        thisAdapter = new BookItemListAdapter(context,
                                recinfo);
                        thisAdapter.setIsCategoty(true);
                        bookItemList.setAdapter(thisAdapter);
                        break;
                    case R.id.Button2:
                        isDisplayingCategory = false;

                        flag = 2;
                        thisAdapter = new BookItemListAdapter(context,
                                hotinfo);
                        thisAdapter.setIsCategoty(true);
                        bookItemList.setAdapter(thisAdapter);
                        break;
                    case R.id.Button3:
                        isDisplayingCategory = false;

                        flag = 3;
                        thisAdapter = new BookItemListAdapter(context,
                                latestinfo);
                        thisAdapter.setIsCategoty(true);
                        bookItemList.setAdapter(thisAdapter);
                        break;
                    case R.id.Button4:
                        isDisplayingCategory = true;

                        thisAdapter = new BookItemListAdapter(context,
                                categoryinfo);
                        thisAdapter.setIsCategoty(true);
                        bookItemList.setAdapter(thisAdapter);
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // rawdata = initdata();
        getRequestCategoryinfo();

        isDisplayingCategory = false;
        bookItemList
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        if (!isDisplayingCategory) {
                            Intent intent = new Intent(context,
                                    BookDetail.class);
                            switch (flag) {
                                case 1:
                                    intent.putExtra("url", recinfo.get(position)
                                            .getUrl());
                                    intent.putExtra("bookCategory",
                                            recinfo.get(position).getCategory());
                                    intent.putExtra("book-id", recinfo
                                            .get(position).getBookId());
                                    intent.putExtra("imgurl", recinfo.get(position)
                                            .getImage());
                                    Log.v("sendurl", recinfo.get(position).getUrl());
                                    startActivity(intent);
                                    break;
                                case 2:
                                    intent.putExtra("url", hotinfo.get(position)
                                            .getUrl());
                                    intent.putExtra("bookCategory",
                                            hotinfo.get(position).getCategory());
                                    intent.putExtra("book-id", hotinfo
                                            .get(position).getBookId());
                                    intent.putExtra("imgurl", hotinfo.get(position)
                                            .getImage());
                                    startActivity(intent);
                                    break;
                                case 3:
                                    intent.putExtra("url", latestinfo.get(position)
                                            .getUrl());
                                    intent.putExtra("bookCategory",
                                            latestinfo.get(position).getCategory());
                                    intent.putExtra("book-id",
                                            latestinfo.get(position).getBookId());
                                    intent.putExtra("imgurl",
                                            latestinfo.get(position).getImage());
                                    startActivity(intent);
                                    break;

                            }
                        } else {
                            String url = categoryinfo.get(position).getUrl();
                            String thisCategory = categoryinfo.get(position)
                                    .getCategory();
                            Intent intent = new Intent(context,
                                    BookListActivity.class);
                            intent.putExtra("url", url);
                            intent.putExtra("thisCategory", thisCategory);
                            startActivity(intent);

                        }
                    }
                });
    }

    private List<BookInfo> initcategoryinfo(Document doc) {
        // TODO Auto-generated method stub
        List<BookInfo> category = new ArrayList<BookInfo>();

        Elements ele1 = doc.select(".category-title");
        Elements ele2 = doc.select(".count");
        Elements ele3 = doc.select("article#content");
        Elements ele4 = ele3.select("a.clickable-box");
        Elements ele5 = ele3.select("img");
        for (int i = 0; i < ele1.size() && i < ele2.size() && i < ele4.size()
                && i < ele5.size(); i++) {
            BookInfo bookInfo = new BookInfo();
            bookInfo.setUrl(ele4.get(i).attr("href"));
            bookInfo.setName(ele1.get(i).text());
            bookInfo.setContent(ele2.get(i).text());
            bookInfo.setImage(ele5.get(i).attr("data-src"));
            bookInfo.setCategory(ele1.get(i).text());
            category.add(bookInfo);
        }

        return category;
    }

    private List<BookInfo> initRecommendData(Document doc) {
        List<BookInfo> BookInfoList = new ArrayList<BookInfo>();
        Elements ele1 = doc.select("#recommend");
        Elements ele2 = ele1.select("div.book-row");
        Elements ele3 = ele1.select("a.clickable-box");
        Elements ele4 = ele1.select("h3.title");
        Elements ele5 = ele1.select("div.author");
        Elements ele6 = ele1.select("div.desc");
        Elements ele7 = ele1.select("img");
        for (int i = 0; i < ele2.size() && i < ele3.size() && i < ele4.size()
                && i < ele5.size() && i < ele6.size() && i < ele7.size(); i++) {
            BookInfo bookInfo = new BookInfo();
            bookInfo.setBookId(ele2.get(i).attr("book-id"));
            bookInfo.setUrl(ele3.get(2 * i).attr("href"));
            Log.v("getUrl", ele3.get(i).toString());
            Log.e("seturl", ele3.get(i).attr("href"));
            bookInfo.setImage(ele7.get(i).attr("data-src"));
            bookInfo.setContent(ele6.get(i).text());
            bookInfo.setAuthor(ele5.get(i).text());
            bookInfo.setName(ele4.get(i).text());
            BookInfoList.add(bookInfo);
        }
        return BookInfoList;

    }

    private List<BookInfo> initHotData(Document doc) {
        List<BookInfo> BookInfoList = new ArrayList<BookInfo>();
        Elements ele1 = doc.select("#hot");
        Elements ele2 = ele1.select("div.book-row");
        Elements ele3 = ele1.select("a.clickable-box");
        Elements ele4 = ele1.select("h3.title");
        Elements ele5 = ele1.select("div.author");
        Elements ele6 = ele1.select("div.desc");
        Elements ele7 = ele1.select("img");
        for (int i = 0; i < ele2.size() && i < ele3.size() && i < ele4.size()
                && i < ele5.size() && i < ele6.size() && i < ele7.size(); i++) {
            BookInfo bookInfo = new BookInfo();
            bookInfo.setBookId(ele2.get(i).attr("book-id"));
            bookInfo.setUrl(ele3.get(2 * i).attr("href"));
            bookInfo.setImage(ele7.get(i).attr("data-src"));
            bookInfo.setContent(ele6.get(i).text());
            bookInfo.setAuthor(ele5.get(i).text());
            bookInfo.setName(ele4.get(i).text());
            BookInfoList.add(bookInfo);
        }
        return BookInfoList;

    }

    private List<BookInfo> initLatestdData(Document doc) {
        List<BookInfo> BookInfoList = new ArrayList<BookInfo>();
        Elements ele1 = doc.select("#last");
        Elements ele2 = ele1.select("div.book-row");
        Elements ele3 = ele1.select("a.clickable-box");
        Elements ele4 = ele1.select("h3.title");
        Elements ele5 = ele1.select("div.author");
        Elements ele6 = ele1.select("div.desc");
        Elements ele7 = ele1.select("img");
        for (int i = 0; i < ele2.size() && i < ele3.size() && i < ele4.size()
                && i < ele5.size() && i < ele6.size() && i < ele7.size(); i++) {
            BookInfo bookInfo = new BookInfo();
            bookInfo.setBookId(ele2.get(i).attr("book-id"));
            bookInfo.setUrl(ele3.get(2 * i).attr("href"));
            bookInfo.setImage(ele7.get(i).attr("data-src"));
            bookInfo.setContent(ele6.get(i).text());
            bookInfo.setAuthor(ele5.get(i).text());
            bookInfo.setName(ele4.get(i).text());
            BookInfoList.add(bookInfo);
        }
        return BookInfoList;

    }

    private void getRequestCategoryinfo() {
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            client.addHeader("APPCODE", "mobile_reading");
            client.get(HtmlUtil.urlWithRec, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, String content) {
                    // TODO Auto-generated method stub
                    super.onSuccess(statusCode, content);
                    Document doc = Jsoup.parse(content);
                    categoryinfo = initcategoryinfo(doc);
                    recinfo = initRecommendData(doc);
                    hotinfo = initHotData(doc);
                    latestinfo = initLatestdData(doc);
                    thisAdapter = new BookItemListAdapter(context,
                            recinfo);
                    thisAdapter.setIsCategoty(true);
                    bookItemList.setAdapter(thisAdapter);
                    Elements pic = doc.select("div.banner").select("a.clickable-box");
                    Elements img = pic.select("img");
                    ImageLoader.getInstance().displayImage(
                            img.first().attr("src"),
                            iv);

                }

            });
        } catch (Exception e) {
            Log.e("yxw", "getRequestCategoryinfo");
        }

    }

}
