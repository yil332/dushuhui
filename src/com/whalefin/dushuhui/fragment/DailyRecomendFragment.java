package com.whalefin.dushuhui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.value.ebook.protobuf.EveryDayRecommendPageProtos.EveryDayRecommendPage.EveryDay;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.activity.RecommendDetail;
import com.whalefin.dushuhui.adaptor.RecommendContentAdaptor;
import com.whalefin.dushuhui.base.DrawerLayoutActivity;
import com.whalefin.dushuhui.domain.RecommendContent;
import com.whalefin.dushuhui.util.NetworkUtil;

import java.util.ArrayList;
import java.util.List;

public class DailyRecomendFragment extends Fragment {

    private List<EveryDay> everyDayList;
    private ExpandableListView thisList;
    private List<RecommendContent> thisArray;
    private RecommendContentAdaptor thisAdaptor;
    private Context context;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_recomond, null);
        thisList = (ExpandableListView) view.findViewById(R.id.reclist);
        DrawerLayoutActivity fca = (DrawerLayoutActivity) context;
        fca.getActionBarRightButton().setVisibility(View.INVISIBLE);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        thisList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        thisList.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(context, RecommendDetail.class);
                intent.putExtra("title", thisArray.get(groupPosition).getTitle());
                intent.putExtra("image", thisArray.get(groupPosition).getImage());
                intent.putExtra("content", thisArray.get(groupPosition).getContent());
                startActivity(intent);
                return true;
            }
        });
        new Thread(getData).start();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
    }

    Runnable getData = new Runnable() {

        @Override

        public void run() {
            // TODO Auto-generated method stub
            Log.e("test", "run");
            Message msg = handler.obtainMessage();
            try {

                if (NetworkUtil.testEveryDay() != null && NetworkUtil.testEveryDay().size() > 0) {

                    everyDayList = NetworkUtil.testEveryDay();

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = everyDayList;
            handler.sendMessage(msg);
        }

    };
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            Log.e("test", "handle");
            switch (msg.arg1) {
                case 1:
                    thisArray = new ArrayList<RecommendContent>();
                    List<EveryDay> ed = new ArrayList<EveryDay>();
                    ed = (List<EveryDay>) msg.obj;
                    if (ed != null) {
                        for (int i = 0; i < ed.size(); i++) {
                            RecommendContent rc = new RecommendContent();
                            rc.setTitle(ed.get(i).getTitle());
                            rc.setContent(ed.get(i).getContent());
                            rc.setTime(ed.get(i).getTime());
                            rc.setImage(ed.get(i).getImage());
                            rc.setFlag(i % ed.size());
                            thisArray.add(rc);

                        }
                        thisAdaptor = new RecommendContentAdaptor(context, thisArray);
                        thisList.setAdapter(thisAdaptor);
                        int groupCount = thisList.getCount();
                        for (int i = 0; i < groupCount; i++) {
                            thisList.expandGroup(i);
                        }
                    }
//				
//				
                    break;
            }
        }


    };


}
