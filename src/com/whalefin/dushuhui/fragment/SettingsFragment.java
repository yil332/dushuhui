package com.whalefin.dushuhui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import com.umeng.update.UpdateStatus;
import com.value.ebook.protobuf.AppSetProtos.AppSet;
import com.value.ebook.protobuf.AppVersionsProtos.AppVersions;
import com.value.ebook.protobuf.ReturnProtos.Return;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.activity.AboutUsActivity;
import com.whalefin.dushuhui.base.DrawerLayoutActivity;
import com.whalefin.dushuhui.service.updateService;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.FileUtil;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.util.PreferencesUtils;

import java.io.File;

public class SettingsFragment extends Fragment implements OnClickListener {
    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        context = activity;
    }

    private int pushSet;
    private int wifiPic;
    private int ringSet;
    private View view;
    private TextView tv_check_new;
    private TextView tv_about_us;
    private TextView tv_cache;
    private TextView tv_clean_cache;

    private CheckBox settings_push;
    private CheckBox settings_ring;
    private CheckBox settings_wifi;

    private AppVersions avp;

    private String username;
    private String password;

    private AppSet mySet;
    private Return myReturn;

    private Context context;

    private String localVersion;
    private String remoteVersion;
    private String appName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DrawerLayoutActivity fca = (DrawerLayoutActivity) context;
        fca.getActionBarRightButton().setVisibility(View.INVISIBLE);
        view = inflater.inflate(R.layout.layout_settings, null);
        settings_push = (CheckBox) view
                .findViewById(R.id.checkbox_setting_push);
        settings_ring = (CheckBox) view
                .findViewById(R.id.checkbox_remind_and_ring);
        settings_wifi = (CheckBox) view.findViewById(R.id.checkbox_wifi);
        tv_check_new = (TextView) view.findViewById(R.id.check_for_new);
        tv_about_us = (TextView) view.findViewById(R.id.about_us);
        tv_about_us.setOnClickListener(this);
        tv_check_new.setOnClickListener(this);
        tv_cache = (TextView) view.findViewById(R.id.cache_size);
        tv_clean_cache = (TextView) view.findViewById(R.id.title_image_cache);
        tv_clean_cache.setOnClickListener(this);

        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            localVersion = packageInfo.versionName;
//			localVersion = "0.5";
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        settings_push
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (isChecked) {
                            pushSet = 1;
                        } else {
                            pushSet = 0;
                        }
                        if (ApplicationParam.isLogin) {
                            new Thread(submitSettings).start();
                        }
                    }
                });
        settings_ring
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (isChecked) {
                            ringSet = 1;
                        } else {
                            ringSet = 0;
                        }
                        if (ApplicationParam.isLogin) {
                            new Thread(submitSettings).start();
                        }
                    }
                });
        settings_wifi
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        if (isChecked) {
                            wifiPic = 1;
                        } else {
                            wifiPic = 0;
                        }
                        if (ApplicationParam.isLogin) {
                            new Thread(submitSettings).start();
                        }
                    }
                });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            tv_cache.setText("当前图片缓存大小："
                    + FileUtil.FormetFileSize(FileUtil.getFileSize(new File(
                    FileUtil.cacheDirectory))));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!ApplicationParam.isLogin) {
            Toast.makeText(context, "未登录，无法同步设置", Toast.LENGTH_SHORT).show();
            wifiPic = 0;
            ringSet = 0;
            pushSet = 0;
            settings_push.setChecked(false);
            settings_wifi.setChecked(false);
            settings_ring.setChecked(false);
        } else {
            username = PreferencesUtils.getString(context, "username");
            password = PreferencesUtils.getString(context, "password");
            new Thread(Settings).start();
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.about_us:
                Intent intent = new Intent(context, AboutUsActivity.class);
                startActivity(intent);
                break;
            case R.id.check_for_new:
                Toast.makeText(context, "检查更新中...", Toast.LENGTH_SHORT).show();
                UmengUpdateAgent.setUpdateAutoPopup(false);
                UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
                    @Override
                    public void onUpdateReturned(int updateStatus,UpdateResponse updateInfo) {
                        switch (updateStatus) {
                        case UpdateStatus.Yes: // has update
                            UmengUpdateAgent.showUpdateDialog(context, updateInfo);
                            break;
                        case UpdateStatus.No: // has no update
                            Toast.makeText(context, "没有更新", Toast.LENGTH_SHORT).show();
                            break;
                        case UpdateStatus.NoneWifi: // none wifi
                            Toast.makeText(context, "没有wifi连接， 只在wifi下更新", Toast.LENGTH_SHORT).show();
                            break;
                        case UpdateStatus.Timeout: // time out
                            Toast.makeText(context, "超时", Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                });
                UmengUpdateAgent.update(context);
//			new Thread(Check_new).start();
                break;
            case R.id.title_image_cache:
                Log.e("clean", "success");
                FileUtil.deleSDFolder(new File(FileUtil.cacheDirectory));
                tv_cache.setText("当前图片缓存大小：0B");
                break;
        }

    }

    Runnable Check_new = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                avp = NetworkUtil.testappVersion();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 2;
            msg.obj = avp;
            handler.sendMessage(msg);
        }
    };
    Runnable Settings = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                ApplicationParam.token = NetworkUtil.login(username, password);
                mySet = NetworkUtil.testGetMemberSet();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = mySet;
            handler.sendMessage(msg);
        }

    };
    Runnable submitSettings = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            Message msg = handler.obtainMessage();
            try {
                ApplicationParam.token = NetworkUtil.login(username, password);
                Log.e("settings", ApplicationParam.token);
                myReturn = NetworkUtil.testMemberSet(pushSet, ringSet, wifiPic);
                Log.e("settings", msg.obj.toString());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            msg.arg1 = 3;
            msg.obj = myReturn;
            handler.sendMessage(msg);
        }
    };

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            Log.e("aboutus", "handle");
            super.handleMessage(msg);
            switch (msg.arg1) {
                case 1:
                    AppSet thisSet = (AppSet) msg.obj;
                    pushSet = thisSet.getPushSet();
                    wifiPic = thisSet.getWifiPic();
                    ringSet = thisSet.getRingSet();
                    updateUi(pushSet, wifiPic, ringSet);
                    break;
                case 2:
                    Log.e("aboutus", msg.obj.toString());
                    avp = (AppVersions) msg.obj;
                    appName = avp.getName();
                    remoteVersion = avp.getVersions();
                    if (remoteVersion.equals(localVersion)) {
                        Toast.makeText(context, "您当前使用的是最新版", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(context, "正在检查更新，请等待", Toast.LENGTH_LONG)
                                .show();
                        AlertDialog.Builder alert = new AlertDialog.Builder(context);
                        alert.setTitle("发现新版本")
                                .setIcon(0)
                                .setMessage(appName + "(V" + remoteVersion + ")")
                                .setPositiveButton("更新",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                Intent updateIntent = new Intent(
                                                        context,
                                                        updateService.class);
                                                updateIntent.putExtra(
                                                        "downloadurl", avp.getUrl());
                                                updateIntent.putExtra(
                                                        "appname", appName);
                                                updateIntent.putExtra(
                                                        "version", remoteVersion);
                                                context.startService(updateIntent);
                                            }
                                        }
                                )
                                .setNegativeButton("取消",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                dialog.dismiss();
                                            }
                                        }
                                );
                        alert.create().show();
                    }

                    break;
                case 3:
                    Return thisReturn = (Return) msg.obj;
                    if (thisReturn.getSuccess().equals("1")) {
                        Log.e("settings", "设置成功");
                    } else {
                        Log.e("settings", "设置失败");
                    }
                    break;
            }
        }

    };

    protected void updateUi(int pushSet, int wifiPic, int ringSet) {
        // TODO Auto-generated method stub
        if (pushSet == 1) {
            settings_push.setChecked(true);
        } else {
            settings_push.setChecked(false);
        }
        if (wifiPic == 1) {
            settings_wifi.setChecked(true);
        } else {
            settings_wifi.setChecked(false);
        }
        if (ringSet == 1) {
            settings_ring.setChecked(true);

        } else {
            settings_ring.setChecked(false);
        }
        Log.e("settings", "get");
    }

}
