package com.whalefin.dushuhui.base;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.activity.LoginActivity;
import com.whalefin.dushuhui.adaptor.MyDrawerListAdapter;
import com.whalefin.dushuhui.fragment.DailyRecomendFragment;
import com.whalefin.dushuhui.fragment.HandLibraryFragment;
import com.whalefin.dushuhui.fragment.MyCenterFragment;
import com.whalefin.dushuhui.fragment.NewHomeFragment;
import com.whalefin.dushuhui.fragment.SettingsFragment;
import com.whalefin.dushuhui.util.ApplicationParam;
import com.whalefin.dushuhui.util.FileUtil;
import com.whalefin.dushuhui.util.NetworkUtil;
import com.whalefin.dushuhui.util.PreferencesUtils;

public class DrawerLayoutActivity extends MyActionBarActivity implements
        OnClickListener {
    public String[] mTitles = {"未登录", "首页", "每日推荐", "电子图书", "我的读书汇", "系统设置"};
    public int[] icon = {R.drawable.ic_sm_login, R.drawable.ic_sm_home,
            R.drawable.ic_sm_zone, R.drawable.ic_sm_library,
            R.drawable.ic_sm_coupons, R.drawable.ic_sm_collections, R.drawable.ic_sm_settings};
    private DrawerLayout mDrawerLayout;
    public ListView mDrawerList;
    private Fragment mContent;
    private String userName;
    private String passWord;
    public MyDrawerListAdapter drawerListAdapter;

    public DrawerLayoutActivity() {
        super(R.string.home);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_main);
        if (savedInstanceState != null) {
            mContent = getSupportFragmentManager().getFragment(
                    savedInstanceState, "mContent");
        }
        if (mContent == null) {
            setActionBarUpButton(R.drawable.ic_actionbar_menu, 0);
            mContent = new DailyRecomendFragment();
        }
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        drawerListAdapter = new MyDrawerListAdapter(this, mTitles, icon);
        mDrawerList.setAdapter(drawerListAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        this.getActionBarUpButton().setOnClickListener(this);
        FileUtil.initDirectory(this);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, mContent).commit();
        if (this.getIntent().getStringExtra("info") != null) {
            String a = this.getIntent
                    ().getStringExtra("info");
            if (a.equals("back")) {
                selectItem(0);
            }
        }
        selectItem(1);
        if (!ApplicationParam.isLogin)
            init_account();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "mContent", mContent);
    }

    public void switchContent(Fragment fragment, int _title) {
        // TODO Auto-generated method stub
        mContent = fragment;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment).commit();
        setActionBarTitle(_title);
    }

    private void init_account() {
        // TODO Auto-generated method stub
        userName = PreferencesUtils.getString(this, "username");
        passWord = PreferencesUtils.getString(this, "password");
        if (userName != null && passWord != null && userName.length() > 1 && passWord.length() > 1) {
            new Thread(login).start();
        }
    }

    Runnable login = new Runnable() {
        public void run() {
            Message msg = handler.obtainMessage();
            String loginResult = null;
            try {
                loginResult = NetworkUtil.login(userName, passWord);
                ApplicationParam.token = loginResult;
                if (NetworkUtil.testVIP(userName).equals("1")) {
                    ApplicationParam.VIP = true;
                } else {
                    ApplicationParam.VIP = false;
                }
                Log.i("token", ApplicationParam.token);
            } catch (Exception e) {
                e.printStackTrace();
            }
            msg.arg1 = 1;
            msg.obj = loginResult;
            handler.sendMessage(msg);
        }
    };
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1) {
                case 1:
                    if (msg.obj != null && msg.obj.toString().length() < 1) {
                        Toast.makeText(DrawerLayoutActivity.this, "登录失败！",
                                Toast.LENGTH_SHORT).show();
                        ApplicationParam.isLogin = false;
                        Log.e("login", "false1");
                    } else {
                        ApplicationParam.isLogin = true;
                        if (ApplicationParam.VIP) {
                            Toast.makeText(DrawerLayoutActivity.this, "欢迎您,VIP用户！",
                                    Toast.LENGTH_SHORT).show();
                            mTitles[0] = "VIP用户";
                        } else {
                            Toast.makeText(DrawerLayoutActivity.this, "登录成功！",
                                    Toast.LENGTH_SHORT).show();
                            mTitles[0] = userName.substring(userName.length() - 4, userName.length()) + " 已登录";
                        }
                        drawerListAdapter = new MyDrawerListAdapter(
                                DrawerLayoutActivity.this, mTitles, icon
                        );
                        mDrawerList.setAdapter(drawerListAdapter);
                        selectItem(1);
                    }
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.ActionBarUpButton:
                if (mDrawerLayout.isDrawerOpen(Gravity.START)) {
                    mDrawerLayout.closeDrawers();
                } else {
                    mDrawerLayout.openDrawer(Gravity.START);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                if (data.getStringExtra("type").equals("member")) {
                    if (ApplicationParam.VIP) {
                        Toast.makeText(DrawerLayoutActivity.this, "欢迎您,VIP用户！",
                                Toast.LENGTH_SHORT).show();
                        mTitles[0] = "VIP用户";
                    } else {
                        Toast.makeText(DrawerLayoutActivity.this, "登录成功！",
                                Toast.LENGTH_SHORT).show();
                        mTitles[0] = userName.substring(userName.length() - 4, userName.length()) + " 已登录";
                    }
                    selectItem(4);
                } else {
                    mTitles[0] = "游客登录";
                    selectItem(1);
                }
                drawerListAdapter = new MyDrawerListAdapter(
                        DrawerLayoutActivity.this, mTitles, icon);
                mDrawerList.setAdapter(drawerListAdapter);
            }
        }
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position,
                                long id) {
            selectItem(position);
        }
    }

    public void selectItem(int position) {
        Fragment newContent = null;
        int _title = 0;
        switch (position) {
            case 0:
                if (ApplicationParam.isLogin) {
                    newContent = new MyCenterFragment();
                    _title = R.string.my_zone;
                    drawerListAdapter.setSelect(0);
                } else {
                    Intent intent = new Intent(this, LoginActivity.class);
                    startActivityForResult(intent, 1);
                    return;
                }
                break;
            case 1:
                newContent = new NewHomeFragment();
                _title = R.string.home;
                drawerListAdapter.setSelect(1);
                break;
            case 2:
                newContent = new DailyRecomendFragment();
                _title = R.string.daily_recommend;
                drawerListAdapter.setSelect(2);
                break;
            case 3:
                newContent = new HandLibraryFragment();
                _title = R.string.hand_library;
                drawerListAdapter.setSelect(3);
                break;
            case 4:
                if (ApplicationParam.isLogin) {
                    newContent = new MyCenterFragment();
                    _title = R.string.my_zone;
                    drawerListAdapter.setSelect(4);
                } else {
                    Intent intent = new Intent(this, LoginActivity.class);
                    startActivityForResult(intent, 1);
                    return;
                }
                break;
            case 5:
                newContent = new SettingsFragment();
                _title = R.string.settings;
                drawerListAdapter.setSelect(5);
                break;

        }
//        drawerListAdapter.notifyDataSetChanged();
        mDrawerList.setAdapter(drawerListAdapter);
        switchContent(newContent, _title);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

}
