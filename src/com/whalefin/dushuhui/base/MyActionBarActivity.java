package com.whalefin.dushuhui.base;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.whalefin.dushuhui.R;

public class MyActionBarActivity extends ActionBarActivity {
    private int mTitleRes;
    private TextView title;
    private TextView upIcon;
    private TextView rightIcon;
    private RelativeLayout background;

    public MyActionBarActivity(int titleRes) {
        this.mTitleRes = titleRes;
        Log.v("LogRes", String.valueOf(titleRes));
    }

    public MyActionBarActivity() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        View actionBar = getLayoutInflater().inflate(R.layout.layout_actionbar,
                new RelativeLayout(this), false);
        title = (TextView) actionBar.findViewById(R.id.title);
        title.setText(mTitleRes);
        upIcon = (TextView) actionBar.findViewById(R.id.ActionBarUpButton);
        rightIcon=(TextView) actionBar.findViewById(R.id.ActionBarRightButton);
        background=(RelativeLayout)actionBar.findViewById(R.id.actionbar_background);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(actionBar);
    }

    public void setActionBarTitle(int _title) {
        title.setText(_title);
    }
    public void setActionBarTitle(String _title) {
        title.setText(_title);
    }

    public void setActionBarUpButton(int drawableId, int Content) {
        if (Content != 0) {
            upIcon.setText(Content);
        }
        upIcon.setCompoundDrawablesWithIntrinsicBounds(drawableId, 0, 0, 0);
    }

    public TextView getActionBarUpButton() {
        return upIcon;
    }
    public void setActionBarRightpButton(int drawableId, int Content) {
        if (Content != 0) {
            rightIcon.setText(Content);
        }
        rightIcon.setCompoundDrawablesWithIntrinsicBounds(drawableId, 0, 0, 0);
    }

    public TextView getActionBarRightButton() {
        return rightIcon;
    }
    public RelativeLayout getActionBarBackground(){
		return background;
    }
    public TextView getActionBarTitle() {
        return title;
    }

}
