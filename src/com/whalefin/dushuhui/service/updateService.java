package com.whalefin.dushuhui.service;

import java.io.File;
import java.io.IOException;

import org.apache.http.Header;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.whalefin.dushuhui.R;
import com.whalefin.dushuhui.base.DrawerLayoutActivity;
import com.whalefin.dushuhui.util.FileUtil;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

public class updateService extends Service {

	private String url;
	private String name;
	private String version;
	private String fileName;
	private NotificationManager notificationManager;
	private Notification notification;
	private RemoteViews contentView;
	private Intent updateIntent;
	private PendingIntent pendingIntent;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.e("service","start");
		url = intent.getStringExtra("downloadurl");
		name = intent.getStringExtra("appname");
		version = intent.getStringExtra("version");
		fileName = name + version;
		createNotofication();
		try {
			AsyncHttpClient client = new AsyncHttpClient();
			String[] allowedContentTypes = new String[] { "application/vnd.android.package-archive",
					"text/html; charset=UTF-8" };
			client.get("http://www.google.com", new BinaryHttpResponseHandler(
					allowedContentTypes){

						@Override
						public void onFailure(int statusCode, Header[] headers,
								byte[] binaryData, Throwable error) {
							// TODO Auto-generated method stub
							notification.setLatestEventInfo(updateService.this,  
		                            name, "下载失败", pendingIntent);
							Log.e("service", "status=" + Integer.toString(statusCode));
							Log.e("service", "error=" + error.getMessage());
							Log.e("service","fail");
							stopSelf();
							super.onFailure(statusCode, headers, binaryData, error);
						}

						@Override
						public void onSuccess(byte[] binaryData) {
							// TODO Auto-generated method stub
							Log.e("service","success");
							super.onSuccess(binaryData);
						}

						@Override
						public void onProgress(int bytesWritten, int totalSize) {
							// TODO Auto-generated method stub
							Log.e("service","progress");
							contentView.setTextViewText(R.id.download_number,  
			                        (int)(bytesWritten/totalSize*100) + "%");  
			                contentView.setProgressBar(R.id.download_progress, 100,  
			                		(int)(bytesWritten/totalSize*100), false);  
			                // show_view  
			                notificationManager.notify(0, notification); 
							super.onProgress(bytesWritten, totalSize);
						}

						@Override
						public void onStart() {
							// TODO Auto-generated method stub
							Log.e("service","down-start");
							super.onStart();
						}
				
			});
		} catch (Exception e) {
			// TODO: handle exception
		}

		return super.onStartCommand(intent, flags, startId);
	}

	private void createNotofication() {
		// TODO Auto-generated method stub
		notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE); 
        notification = new Notification(); 
        notification.icon = R.drawable.logo;
        notification.tickerText = "开始下载";
        notification.defaults=Notification.DEFAULT_ALL;
        contentView =new RemoteViews(getPackageName(), R.layout.layout_notification);
        
        contentView.setTextViewText(R.id.notificationTitle, "正在下载"); 
        contentView.setTextViewText(R.id.download_number, "0%"); 
        contentView.setProgressBar(R.id.download_progress, 100, 0, false);
        notification.contentView=contentView;
        
        updateIntent = new Intent(this,DrawerLayoutActivity.class);  
        updateIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);  
        pendingIntent = PendingIntent.getActivity(this, 0, updateIntent, 0);  
  
        notification.contentIntent = pendingIntent; 
        
        notificationManager.notify(0, notification);
	}

	

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
