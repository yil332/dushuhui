package com.smartlion.bookread.parserutil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.model.parserzip.ContentInfo;
import com.smartlion.bookread.model.parserzip.ManifestInfo;
import com.smartlion.bookread.model.parserzip.TargetInfo;

/**
 * 将书籍按照章节读取的类
 * 
 * @author 薛文杰
 * 
 */
public class BookParserUtil {

	private static final String MANIFEST = "/manifest";

	private Context ctx;

	private long bookid;

	private ManifestInfo mManifestInfo = null;

	public BookParserUtil(Context ctx, long bookid) {
		this.ctx = ctx;
		this.bookid = bookid;
	}

	/**
	 * 解析出文章的目录
	 * 
	 * @return
	 * @throws IOException
	 */
	public ManifestInfo getMaifestInfo() throws IOException {

		if (mManifestInfo == null) {
			ManifestInfo contentInfo = new ManifestInfo();
			mManifestInfo = new ManifestInfo();
			Gson gson = new Gson();
			BufferedReader reader = null;
			File file = new File(Config.getInstance().getBookLocation(bookid)
					+ MANIFEST);
			Log.e("yxw", file.getAbsolutePath());
			if (!file.exists()) {
				Log.e("yxw", Config.getInstance().getBookLocation(bookid) + "/"
						+ bookid + ".mobile");
				Config.getInstance().Unzip(
						Config.getInstance().getBookLocation(bookid) + "/"
								+ bookid + ".mobile",
						Config.getInstance().getBookLocation(bookid)+"/");
				Log.e("yxw", "upzip2");
			}
			reader = new BufferedReader(new FileReader(Config.getInstance()
					.getBookLocation(bookid) + MANIFEST));
			String json = getStringByReader(reader);

			contentInfo = gson.fromJson(json, ManifestInfo.class);

			for (int i = 0; i < contentInfo.mContentInfo.size(); i++) {
				if (contentInfo.mContentInfo.get(i).mChildrenInfo != null
						&& contentInfo.mContentInfo.get(i).mChildrenInfo.size() > 0) {

					mManifestInfo.mContentInfo.add(contentInfo.mContentInfo
							.get(i));

					for (int j = 0; j < contentInfo.mContentInfo.get(i).mChildrenInfo
							.size(); j++) {
						ContentInfo content1 = new ContentInfo();
						content1.id = contentInfo.mContentInfo.get(i).mChildrenInfo
								.get(j).id;
						content1.title = contentInfo.mContentInfo.get(i).mChildrenInfo
								.get(j).title;
						content1.target = contentInfo.mContentInfo.get(i).mChildrenInfo
								.get(j).target;
						mManifestInfo.mContentInfo.add(content1);
					}
				} else {
					mManifestInfo.mContentInfo.add(contentInfo.mContentInfo
							.get(i));
				}

			}

			mManifestInfo.authors = contentInfo.authors;
			mManifestInfo.category = contentInfo.category;
			mManifestInfo.description = contentInfo.description;
			mManifestInfo.isbn = contentInfo.isbn;
			mManifestInfo.presses = contentInfo.presses;
			mManifestInfo.published = contentInfo.published;
			mManifestInfo.tags = contentInfo.tags;
			mManifestInfo.title = contentInfo.title;

			return mManifestInfo;
		}
		return mManifestInfo;
	}

	/**
	 * 获取指定章节的Target值
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	public String getTargetByID(String id) throws IOException {
		String target = "";
		ManifestInfo contentInfo = getMaifestInfo();
		for (int i = 0; i < contentInfo.mContentInfo.size(); i++) {
			if (contentInfo.mContentInfo.get(i).id.equals(id)) {
				target = contentInfo.mContentInfo.get(i).target;
				break;
			}
		}
		return target;
	}

	/**
	 * 获取指定章节的标题
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	public String getTitleByID(String id) throws IOException {
		String title = "";
		ManifestInfo contentInfo = getMaifestInfo();
		for (int i = 0; i < contentInfo.mContentInfo.size(); i++) {
			if (contentInfo.mContentInfo.get(i).id.equals(id)) {
				title = contentInfo.mContentInfo.get(i).title;
				break;
			}
		}
		return title;
	}

	/**
	 * 判断指定章节是否有前一章节，如果有，返回前一章节id，没有就返回－1
	 * 
	 * @param contentid
	 * @return
	 * @throws IOException
	 */
	public long checkContentHasPreContent(long contentid) throws IOException {
		long preContentid = -1;
		ManifestInfo mManifestInfo = getMaifestInfo();
		for (int i = 0; i < mManifestInfo.mContentInfo.size(); i++) {

			long id = Long.valueOf(mManifestInfo.mContentInfo.get(i).id);
			if (id == contentid) {
				return preContentid;
			}
			preContentid = id;
		}
		return preContentid;
	}

	/**
	 * 解析出文章的章节内容
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	public TargetInfo getTargetInfo(long id) throws IOException {
		TargetInfo mTargetInfo = new TargetInfo();
		String path = Config.getInstance().getBookLocation(bookid) + "/"
				+ String.valueOf(id);
		if (isFileExist(path)) {
			try {
				Gson gson = new Gson();
				BufferedReader reader = null;
				reader = new BufferedReader(new FileReader(path));
				String json = getStringByReader(reader);
				mTargetInfo = gson.fromJson(json, TargetInfo.class);
			} catch (Exception e) {
				return null;
			}
		}
		return mTargetInfo;
	}

	/**
	 * 获取经过unicodeToUtf8之后的string
	 * 
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	private String getStringByReader(BufferedReader reader) throws IOException {
		StringBuilder builder = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			byte[] utf8 = line.getBytes("UTF-8");
			line = new String(utf8, "UTF-8");
			builder.append(line);
		}
		return builder.toString();
	}

	public boolean isFileExist(String path) {
		File file = new File(path);
		return file.exists();
	}

}