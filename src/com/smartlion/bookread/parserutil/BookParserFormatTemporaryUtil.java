package com.smartlion.bookread.parserutil;

import java.io.IOException;

import com.smartlion.bookread.model.PageShowInfo;
import com.smartlion.bookread.model.parserzip.TargetInfo;


import android.content.Context;

public class BookParserFormatTemporaryUtil extends BookParserFormatUtil {

	private long id;

	private long appointIndex;

	private boolean noticeUI1;

	private boolean parserInIndex = false;

	private long appointPageIndex = -1;

	protected boolean isToEndPage = false;

	public BookParserFormatTemporaryUtil(Context ctx, long bookid,
			int fontSizeIndex) {
		super(ctx, bookid, fontSizeIndex);
		parserInIndex = false;
	}

	public BookParserFormatTemporaryUtil(Context ctx, long bookid,
			int fontSizeIndex, long appointIndex) {
		super(ctx, bookid, fontSizeIndex);
		this.noticeUI = false;
		noticeUI1 = true;
		this.appointIndex = appointIndex;
		parserInIndex = true;
		appointPageIndex = -1;
		isToEndPage = false;
	}

	@Override
	public void paraserAllBook() throws IOException {

	}

	@Override
	public void noticeUIifPageGreaterThree() {
		// if (noticeUI) {
		// if (totalPage>3) {
		// proxy.doAfterParserBookTemporaryUnit(totalPage);
		// noticeUI=false;
		// }
		// }
	}

	@Override
	public void noticeUIifPageInindex() {
		if (noticeUI1) {
			if (contentCharSize > appointIndex) {
				appointPageIndex = totalPage - 1;
				noticeUI1 = false;
			}
		}
	}

	@Override
	public void paraserText(long id) throws IOException {
		this.id = id;
		mBookCacheUtil.setCacheForTemporary(id);
		mBookInfoCacheUtil.setCacheForTemporary();

		String target = mBookParserUtil.getTargetByID(String.valueOf(id));
		long targetId = id;
		if (target != null && !target.equals("")) {
			targetId = Long.valueOf(target);
		}

		TargetInfo mTargetInfo = mBookParserUtil.getTargetInfo(targetId);
		if (mTargetInfo == null) {
			mTargetInfo = new TargetInfo();
			
		}
		mTargetInfo.title = mBookParserUtil
				.getTitleByID(String.valueOf(id));

		if (!parserInIndex) {
			int contentTotalPage=mBookInfoCacheUtil.getChapterTotalPage(id);
			if (contentTotalPage!=-1) {
				totalPage=contentTotalPage;
			}else{
				paraser(mTargetInfo.mTargetContentInfo, id, mTargetInfo.title);
			}
		} else{
			paraser(mTargetInfo.mTargetContentInfo, id, mTargetInfo.title);
		}
		noticeUIifPage();
	}

	/**
	 * 当整个章节解析好了 通知ui
	 */
	public void noticeUIifPage() {
		if (isToEndPage) {
			proxy.doAfterParserBookTemporaryToIndex(getTotalPage() - 1);
		} else if (parserInIndex) {
			proxy.doAfterParserBookTemporaryToIndex(appointPageIndex);
		} else
			proxy.doAfterParserTemporaryEnd(totalPage);
	}

	public int findOffsetSizeInContent(long appointIndex) throws IOException {
		int contentTotalPage = mBookInfoCacheUtil.getChapterTotalPage(id);
		for (int i = 0; i < contentTotalPage; i++) {
			PageShowInfo pg = mBookCacheUtil.readCacheObjectByPageIndex(i);
			if (pg.totalCharCount > appointIndex) {
				return i;
			}
		}
		return 0;
	}

	public long getPageAppointIndex(long pageIndex) throws IOException {
		PageShowInfo pg = mBookCacheUtil
				.readCacheObjectByPageIndex((int) pageIndex);
		long appointIndex = pg.totalCharCount - pg.pageCharCount;
		return appointIndex;
	}
	

}
