package com.smartlion.bookread.parserutil;

import java.io.IOException;
import java.util.HashMap;

import android.content.Context;

import com.google.gson.Gson;
import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.model.BookMarkerInfos;

/**
 * 管理书籍标签的类
 * 
 * @author wenjiexue
 * 
 */
public class BookMarkerCacheUtil extends CacheUtil{

	//缓存集合，使用hash防止重复读取以及重复纪录
	public HashMap<Long, BookMarkerInfos> mBookLabelInfoMap = new HashMap<Long, BookMarkerInfos>();

	/**
	 * 初始化保存路径地址
	 * @param ctx
	 * @param bookid
	 */
	public BookMarkerCacheUtil(Context ctx, long bookid) {
		localPath = Config.getInstance().getBookLocation(bookid) + "/cache_";
	}

	/**
	 * 保存书籍标签，并更新当前的hashmap
	 * @param contentid
	 * @param mBookMarkerInfos
	 * @throws IOException
	 */
	public void saveMarkerList(long contentid, BookMarkerInfos mBookMarkerInfos)
			throws IOException {
		String path = localPath + "bookmarker" + "contentid_" + contentid;
		mBookLabelInfoMap.put(contentid, mBookMarkerInfos);
		Gson gson = new Gson();
		String json = gson.toJson(mBookMarkerInfos);
		saveCache(path, json);
	}

	/**
	 * 读取书籍标签，如果当前已经存在，就直接读取hashmap中的
	 * @param contentid
	 * @return
	 * @throws IOException
	 */
	public BookMarkerInfos loadMarkerList(long contentid) throws IOException {
		if (mBookLabelInfoMap.get(contentid) == null) {
			BookMarkerInfos mBookMarkerInfos = new BookMarkerInfos();
			String path = localPath + "bookmarker" + "contentid_" + contentid;
			Gson gson = new Gson();
			String json = readCache(path);
			if (!json.equals("")) {
				mBookMarkerInfos = gson.fromJson(json, BookMarkerInfos.class);
			}
			mBookLabelInfoMap.put(contentid, mBookMarkerInfos);
		}
		return mBookLabelInfoMap.get(contentid);
	}

	/**
	 * 根据书签的第一个位置，删除该书籍标签
	 * @param contentid
	 * @param startIndex
	 * @throws IOException
	 */
	public void deleteMarker(long contentid, int startIndex) throws IOException {
		BookMarkerInfos mBookMarkerInfos = loadMarkerList(contentid);
		for (int i = 0; i < mBookMarkerInfos.mBookMarkerInfosList.size(); i++) {
			if (mBookMarkerInfos.mBookMarkerInfosList.get(i).markerIndex == startIndex) {
				mBookMarkerInfos.mBookMarkerInfosList.remove(i);
				break;
			}
		}
		saveMarkerList(contentid, mBookMarkerInfos);
	}
	
}
