package com.smartlion.bookread.parserutil;

import java.io.IOException;


import android.content.Context;

public class BookParserThread extends Thread{

	public BookParserFormatUtil mBookParserFormatUtil;
	
	boolean isFirst;
	
	public BookParserThread(Context ctx,long bookid,int fontSizeIndex,boolean isStart) {
		mBookParserFormatUtil=new BookParserFormatUtil(ctx, bookid, fontSizeIndex,isStart);
	}

	public int getTotalPage(){
		return mBookParserFormatUtil.getTotalPage();
	}
	
	public boolean isAllLoad(){
		return mBookParserFormatUtil.isAllLoad;
	}

    
	@Override
	public void run() {
		
		try {
			if (!mBookParserFormatUtil.isAllLoad) {
				mBookParserFormatUtil.isCallStop=false;
				mBookParserFormatUtil.paraserAllBook();
			}
		} catch (IOException e) {
			
		}
	}
	
	
	public void callStop() {
		mBookParserFormatUtil.isCallStop=true;
	}
}
