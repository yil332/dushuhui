package com.smartlion.bookread.parserutil;

import java.io.IOException;

import android.content.Context;

import com.google.gson.Gson;
import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.model.BookLoadedInfo;
import com.smartlion.bookread.model.ContentCacheLoadedInfo;

/**
 * 书籍基础内容的缓存管理
 * 
 * @author wenjiexue
 * 
 */
public class BookInfoCacheUtil extends CacheUtil {

	private Context ctx;
	private long bookid;
	// 当前读取的字体大小
	private int fontSizeIndex;

	/**
	 * 设置缓存地址
	 * 
	 * @param ctx
	 * @param bookid
	 * @param fontSizeIndex
	 */
	public BookInfoCacheUtil(Context ctx, long bookid, int fontSizeIndex) {
		this.ctx=ctx;
		this.bookid=bookid;
		this.fontSizeIndex = fontSizeIndex;
		localPath = Config.getInstance().getBookLocation(bookid)+ "/cache_";
	}
	
	/**
	 * 将该缓存设置成临时缓存
	 * @param temporaryContentID
	 */
	public void setCacheForTemporary(){
		localPath=Config.getInstance().getBookLocation(bookid)+ "/cache_"+"temporary";
	}

	/**
	 * 保存该书的总页数－同时也表示该书已经加载完毕
	 * 
	 * @param totalPage
	 * @throws IOException
	 */
	public void saveBookInfo(long totalPage) throws IOException {
		String path = localPath + "fontsize_" + fontSizeIndex
				+ "bookisloadover";
		BookLoadedInfo mBookInfo = new BookLoadedInfo();
		mBookInfo.totalPage = totalPage;
		Gson gson = new Gson();
		saveCache(path, gson.toJson(mBookInfo));
	}

	/**
	 * 获取该书的总页数，－1表示该书还没又加载完毕
	 * 
	 * @return
	 * @throws IOException
	 */
	public long readBookTotalPage() throws IOException {
		String path = localPath + "fontsize_" + fontSizeIndex
				+ "bookisloadover";
		String json = readCache(path);
		if (json.equals("")) {
			return -1;
		}
		BookLoadedInfo mBookInfo = new BookLoadedInfo();
		Gson gson = new Gson();
		mBookInfo = gson.fromJson(json, BookLoadedInfo.class);
		return mBookInfo.totalPage;
	}
	
	
	/**
     * 读取缓存获取全文解析到此处总的页数
     * @param id
     * @return
     * @throws IOException 
     */
    public int getChapterTotalPage(long id) throws IOException{
    	ContentCacheLoadedInfo alreadyCreateCacheInfo=new ContentCacheLoadedInfo();
		Gson gson=new Gson();
		String json=readCacheByTargetContentID(id);
    	if (json.equals("")) {
			return -1;
		}
    	alreadyCreateCacheInfo=gson.fromJson(json, ContentCacheLoadedInfo.class);
    	return (int) alreadyCreateCacheInfo.totalPage;
    }
    
    /**
     * 获取指定章节在在全文中的字数－－－文章解析到此处的总字数
     * @param id
     * @return
     * @throws IOException
     */
    public int getContentTotalSize(long id) throws IOException{
    	ContentCacheLoadedInfo alreadyCreateCacheInfo=new ContentCacheLoadedInfo();
		Gson gson=new Gson();
		String json=readCacheByTargetContentID(id);
		if (json.equals("")) {
			return -1;
		}
		alreadyCreateCacheInfo=gson.fromJson(json, ContentCacheLoadedInfo.class);
		return (int) alreadyCreateCacheInfo.contenttotalsize;
    }
    
    /**
     * 获取该章节的总字数
     * @param id
     * @return
     * @throws IOException
     */
    public int getContentCharSize(long id) throws IOException{
    	ContentCacheLoadedInfo alreadyCreateCacheInfo=new ContentCacheLoadedInfo();
		Gson gson=new Gson();
		String json=readCacheByTargetContentID(id);
		if (json.equals("")) {
			return -1;
		}
		alreadyCreateCacheInfo=gson.fromJson(json, ContentCacheLoadedInfo.class);
		return (int) alreadyCreateCacheInfo.contentcharsize;
    }
    
    /**
     * 读取缓存获取指定章节开始的页面位置
     * @param id
     * @return
     * @throws IOException
     */
    public int getTargetStartPageID(long id) throws IOException{
		ContentCacheLoadedInfo alreadyCreateCacheInfo=new ContentCacheLoadedInfo();
		Gson gson=new Gson();
		String json=readCacheByTargetContentID(id);
		if (json.equals("")) {
			return -1;
		}
		alreadyCreateCacheInfo=gson.fromJson(json, ContentCacheLoadedInfo.class);
		int beginPageID=(int) (alreadyCreateCacheInfo.totalPage-alreadyCreateCacheInfo.page);
		return beginPageID;
	}
    
    /**
	 * 读取缓存文件-段落保存信息
	 * @param path
	 * @param json
	 * @throws IOException
	 */
    public String readCacheByTargetContentID(long id) throws IOException{
    	String path=localPath+"fontsize_"+fontSizeIndex+"targetContentID_"+id;
    	return readCache(path);
    }
}
