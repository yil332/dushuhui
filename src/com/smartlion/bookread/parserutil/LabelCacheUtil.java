package com.smartlion.bookread.parserutil;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import android.content.Context;

import com.google.gson.Gson;
import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.model.BookLabelInfo;
import com.smartlion.bookread.model.BookLabelInfos;

/**
 * 书籍标记缓存管理的类
 * @author wenjiexue
 *
 */
public class LabelCacheUtil extends CacheUtil{

	//缓存集合，使用hash防止重复读取以及重复纪录
	public HashMap<Long, BookLabelInfos> mBookLabelInfoMap = new HashMap<Long, BookLabelInfos>();

	/**
	 * 初始化缓存地址
	 * @param ctx
	 * @param bookid
	 */
	public LabelCacheUtil(Context ctx, long bookid) {
		localPath = Config.getInstance().getBookLocation(bookid) + "/cache_";
	}

	/**
	 * 保存书籍标记
	 * @param contentid
	 * @param mBookLabelInfos
	 * @throws IOException
	 */
	public void saveLabelList(long contentid, BookLabelInfos mBookLabelInfos)
			throws IOException {
		String path = localPath + "label" + "contentid_" + contentid;
		Collections.sort(mBookLabelInfos.mBookLabelInfoList,new Comparator<BookLabelInfo>() {

			@Override
			public int compare(BookLabelInfo lhs, BookLabelInfo rhs) {
				return lhs.labelStartIndex.compareTo(rhs.labelStartIndex); 
			}
		});
		mBookLabelInfoMap.put(contentid, mBookLabelInfos);
		Gson gson = new Gson();
		String json = gson.toJson(mBookLabelInfos);
		saveCache(path, json);
	}

	
	/**
	 * 读取书籍标记
	 * @param contentid
	 * @return
	 * @throws IOException
	 */
	public BookLabelInfos loadLabelList(long contentid) throws IOException {
		if (mBookLabelInfoMap.get(contentid) == null) {
			BookLabelInfos mBookLabelInfos = new BookLabelInfos();
			String path = localPath + "label" + "contentid_" + contentid;
			Gson gson = new Gson();
			String json = readCache(path);
			if (!json.equals("")) {
				mBookLabelInfos = gson.fromJson(json, BookLabelInfos.class);
			}
			mBookLabelInfoMap.put(contentid, mBookLabelInfos);
		}
		return mBookLabelInfoMap.get(contentid);
	}
	
	public void deleteLabelListByStartIndex(long contentid,int startIndex,int count) throws IOException{
		BookLabelInfos mBookLabelInfos=mBookLabelInfoMap.get(contentid);
		int deleteIndex=-1;
		if (mBookLabelInfos!=null) {
			for (int i = 0; i < mBookLabelInfos.mBookLabelInfoList.size(); i++) {
				if (mBookLabelInfos.mBookLabelInfoList.get(i).labelStartIndex.equals(startIndex)) {
					deleteIndex=i;
					break;
				}
			}
			if (deleteIndex!=-1) {
				for (int i = 0; i < count; i++) {
					mBookLabelInfos.mBookLabelInfoList.remove(deleteIndex);
				}
			}
			saveLabelList(contentid, mBookLabelInfos);
		}
	}
}
