package com.smartlion.bookread.parserutil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.google.gson.Gson;
import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.common.PaintUtil;
import com.smartlion.bookread.inf.BookReadInterface;
import com.smartlion.bookread.inf.BookReadProxy;
import com.smartlion.bookread.model.BookPageCountInfo;
import com.smartlion.bookread.model.ContentCacheLoadedInfo;
import com.smartlion.bookread.model.PageCharInfo;
import com.smartlion.bookread.model.PageCharInfos;
import com.smartlion.bookread.model.PageShowInfo;
import com.smartlion.bookread.model.parserzip.ManifestInfo;
import com.smartlion.bookread.model.parserzip.TargetContentInfo;
import com.smartlion.bookread.model.parserzip.TargetInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;


/**
 * 将书籍规范化显示的类－－－－－－乱七八糟，这个类很凶残，不要乱改乱动，我无力了。日月神剑
 * 写的什么垃圾，各种公公变量混用，各种公共方法搭配公共变量，你妹，你做什么程序员
 * 
 * @author 薛文杰
 * 
 */
public class BookParserFormatUtil {
	// ui层标记将当前进程终止！
	public boolean isCallStop = false;

	protected Context ctx;
	// 保存传入的书籍id
	protected long bookid;
	// 当前选择的书籍字号index
	protected int fontSizeIndex;

	// 正文开始的x,y坐标
	private float startX, startY;
	// 正文占用的宽度和高度
	private float textWidth, textHeight;

	private int footnoteWidth;
	private int footnoteHeight;

	// 正文中每行间隔距离
	private float contentLine;

	// 标记当前解析章节的页面
	private int page;
	// 标记当前解析全文的页面
	protected int totalPage;
	// 标记当前解析的字在全文中占的位移量
	private int totaloffset;
	// 用于纪录当前页面总字数
	private int pageCharSize;

	// 代理，返回ui层一些信息
	protected BookReadProxy proxy;

	// 标记是否已经通知过ui一次了
	protected boolean noticeUI = true;

	// 解析书籍
	protected BookParserUtil mBookParserUtil;
	// 书籍缓存管理
	protected BookContentCacheUtil mBookCacheUtil;
	protected BookInfoCacheUtil mBookInfoCacheUtil;

	// 纪录当前章节的字数
	protected long contentCharSize;

	public boolean isAllLoad = false;

	private int totalParserChapter = 0;

	public BookParserFormatUtil(Context ctx, long bookid, int fontSizeIndex) {
		this.ctx = ctx;
		this.bookid = bookid;
		this.fontSizeIndex = fontSizeIndex;
		initPageInfo();

		proxy = new BookReadProxy((BookReadInterface) ctx);

		mBookParserUtil = new BookParserUtil(ctx, bookid);
		mBookCacheUtil = new BookContentCacheUtil(ctx, bookid, fontSizeIndex);
		mBookInfoCacheUtil = new BookInfoCacheUtil(ctx, bookid, fontSizeIndex);
		isCallStop = false;
	}

	public BookParserFormatUtil(Context ctx, long bookid, int fontSizeIndex,
			boolean isFirst) {
		this.ctx = ctx;
		this.bookid = bookid;
		this.fontSizeIndex = fontSizeIndex;
		initPageInfo();

		proxy = new BookReadProxy((BookReadInterface) ctx);

		mBookParserUtil = new BookParserUtil(ctx, bookid);
		mBookCacheUtil = new BookContentCacheUtil(ctx, bookid, fontSizeIndex);
		mBookInfoCacheUtil = new BookInfoCacheUtil(ctx, bookid, fontSizeIndex);
		noticeUI = isFirst;
		isCallStop = false;
	}

	/**
	 * 初始化一些页面显示信息
	 */
	private void initPageInfo() {
		startX = PaintUtil.getBOOK_PAGE_PADDING_X(ctx);
		startY = PaintUtil.getBOOK_PAGE_PADDING_TOP(ctx);
		textWidth = Config.getInstance().getWindowWidth() - startX;
		textHeight = Config.getInstance().getWindowHeight()-PaintUtil.dp2pixel(ctx, 36);
		footnoteWidth = PaintUtil.TEXT_SIZE_NORMAL[fontSizeIndex] * 2;
		footnoteHeight = PaintUtil.TEXT_SIZE_NORMAL[fontSizeIndex] * 2;

		page = 0;
		totalPage = 0;
	}

	/**
	 * 解析全书-整本书解析的时候
	 * 
	 * @throws IOException
	 */
	public void paraserAllBook() throws IOException {
		isCallStop = false;
		totaloffset = 0;
		totalParserChapter = 0;
		// 先判断已经加载完成
		long bookTotalPage = mBookInfoCacheUtil.readBookTotalPage();
		if (bookTotalPage == -1) {
			ManifestInfo mManifestInfo = mBookParserUtil.getMaifestInfo();
			proxy.doGetTotalChapter(mManifestInfo.mContentInfo.size());
			for (int i = 0; i < mManifestInfo.mContentInfo.size(); i++) {
				if (isCallStop) {
					break;
				}

				// 先判断缓存是否存在
				long id = Long.valueOf(mManifestInfo.mContentInfo.get(i).id);

				paraserText(id);
			}

			if (!isCallStop) {
				isAllLoad = true;
				mBookInfoCacheUtil.saveBookInfo(totalPage);
				// 整本书解析完成后通知ui
				proxy.doAfterParserEnd(totalPage);
			}
		} else {
			totalPage = (int) bookTotalPage;
			proxy.doAfterParserEndBefore(totalPage);
		}

	}

	/**
	 * 解析对应的某个章节
	 * 
	 * @param mTargetInfo
	 * @throws IOException
	 */
	public void paraserText(long id) throws IOException {
		totalParserChapter++;
		String target = mBookParserUtil.getTargetByID(String.valueOf(id));
		long targetId = id;
		if (target != null && !target.equals("")) {
			targetId = Long.valueOf(target);
		}
		int contentTotalPage = mBookInfoCacheUtil.getChapterTotalPage(id);
		// 先判断是否已经解析过了
		if (contentTotalPage != -1) {
			totalPage = contentTotalPage;
			noticeUIifPageGreaterThree();
			noticeUIifPageInindex();
		} else {
			TargetInfo mTargetInfo = mBookParserUtil.getTargetInfo(targetId);
			if (mTargetInfo == null) {
				mTargetInfo = new TargetInfo();
			}
			mTargetInfo.title = mBookParserUtil
					.getTitleByID(String.valueOf(id));
			paraser(mTargetInfo.mTargetContentInfo, id, mTargetInfo.title);
		}
		proxy.doAfterParserChapter(totalParserChapter);

	}

	/**
	 * 当书籍解析页数超过3张之后同志页面
	 */
	public void noticeUIifPageGreaterThree() {
		if (noticeUI) {
			if (totalPage > 3) {
				proxy.doAfterParserBookUnit(totalPage);
				noticeUI = false;
			}
		}
	}

	public void noticeUIifPageInindex() {

	}

	public void saveTitlePage(String title, long id) throws IOException {
		if (!title.equals("")) {
			contentCharSize += title.length();
			pageCharSize += title.length();
			PageCharInfos mPageCharInfos = getPageCharInfoInfosForTitle(title);
			savePageShowInfo("title", mPageCharInfos, id, title);
			beforeSavePageShowInfo();
		}
	}

	public ContentCacheLoadedInfo alreadyCreateCacheInfo = new ContentCacheLoadedInfo();
	public ArrayList<PageCharInfo> footPageCharList = new ArrayList<PageCharInfo>();
	public ArrayList<Integer> footIndexList = new ArrayList<Integer>();

	/**
	 * 解析h1类型的内容
	 * 
	 * @param mTargetContentInfo
	 * @return
	 * @throws IOException
	 */
	protected void paraser(ArrayList<TargetContentInfo> mTargetContentInfoList,
			long id, String title) throws IOException {

		pageCharSize = 0;
		contentCharSize = 0;
		page = 0;
		float currentY = startY;
		alreadyCreateCacheInfo = new ContentCacheLoadedInfo();
		if (!title.equals("封面")) {
			saveTitlePage(title, id);
		}
		
		

		PageCharInfos mPageCharInfos = new PageCharInfos();

		for (int i = 0; i < mTargetContentInfoList.size(); i++) {
			if (isCallStop) {
				break;
			}
			TargetContentInfo mTargetContentInfo = mTargetContentInfoList
					.get(i);
			if (mTargetContentInfo.type == null) {
				if (mTargetContentInfo.tag != null) {
					mTargetContentInfo.type = mTargetContentInfo.tag;
				}
			}
			// 保存图片
			if (mTargetContentInfo.type.equals("img")) {
				String imgSrc = getTrueImageSrc(mTargetContentInfo.src);
				Bitmap bitmap = adaptationImage(imgSrc);
                if(bitmap!=null){


                    if (imgSrc.contains("cover.jpg")) {
                        mPageCharInfos.mPageCharInfoList.clear();
                        currentY = startY;
                    }
                    if ((currentY + bitmap.getHeight()) > textHeight
                            && mPageCharInfos.mPageCharInfoList.size() != 0) {
                        contentCharSize += imgSrc.length();
                        pageCharSize += imgSrc.length();
                        savePageShowInfo(mTargetContentInfo.type, mPageCharInfos,
                                id, title);
                        mPageCharInfos = new PageCharInfos();
                        beforeSavePageShowInfo();
                        currentY = startY;
                        if (isCallStop) {
                            break;
                        }
                    }
                    mPageCharInfos.mPageCharInfoList.add(getPageCharInfoForImg(
                            imgSrc, bitmap, currentY, mTargetContentInfo.type));
                    currentY = currentY + contentLine + bitmap.getHeight();

                    if (imgSrc.contains("cover.jpg") || currentY > textHeight) {
                        contentCharSize += imgSrc.length();
                        pageCharSize += imgSrc.length();
                        savePageShowInfo(mTargetContentInfo.type, mPageCharInfos,
                                id, title);
                        mPageCharInfos = new PageCharInfos();
                        beforeSavePageShowInfo();
                        currentY = startY;
                        if (isCallStop) {
                            break;
                        }
                    }
                }
			}
			// 保存文字
			float widthes = startX;
			float strWidth = 0;
			float startPositionX = startX;
			String str = "";
			boolean isShowLi = false;
			// 获取画笔
			Paint mPaint = PaintUtil.getPaintByType(ctx,
					mTargetContentInfo.type);
			float fontSize = mPaint.getTextSize();
			if (mTargetContentInfo.type.equals("p")) {
				widthes = widthes + (2 * fontSize);
				startPositionX = startPositionX + (2 * fontSize);
			}
			if (mTargetContentInfo.type.equals("li")) {
				isShowLi = true;
				startPositionX = startPositionX + fontSize;
				widthes = startPositionX + fontSize;
			}

			for (int j = 0; j < mTargetContentInfo.mTextInfo.size(); j++) {
				mTargetContentInfo.mTextInfo.get(j).content = unicodeToUtf8(mTargetContentInfo.mTextInfo
						.get(j).content);
				contentLine = PaintUtil.getLineHeightByType(ctx,
						mTargetContentInfo.type);

				if (mTargetContentInfo.mTextInfo.get(j).content.contains("\n")) {
                    if (footPageCharList.size() > 0) {
                        for (int k = 0; k < footPageCharList.size(); k++) {

                            mPageCharInfos.mPageCharInfoList
                                    .add(footPageCharList.get(k));
                        }
                        footPageCharList.clear();
                        footIndexList.clear();
                    }
					PageCharInfo mPageCharInfo = getPageCharInfoForChar(str,
							startPositionX, strWidth, currentY, fontSize,
							j + 1, mTargetContentInfo.type, isShowLi, false,
							contentCharSize);
					addPageToPageList(mPageCharInfo, mPageCharInfos);

					currentY = currentY + contentLine;
					currentY = currentY + contentLine / 2;
					if (currentY > textHeight
							&& mPageCharInfos.mPageCharInfoList.size() != 0) {
						savePageShowInfo(mTargetContentInfo.type,
								mPageCharInfos, id, title);
						mPageCharInfos = new PageCharInfos();
						beforeSavePageShowInfo();
						currentY = startY;
						if (isCallStop) {
							break;
						}
					}
					widthes = startX;
					strWidth = 0;
					startPositionX = startX;
					str = "";
					isShowLi = false;

					continue;
				}

				if (mTargetContentInfo.mTextInfo.get(j).kind.equals("footnote")) {
					float fontWidth = mPaint.measureText("　");
                    str = str + "　";
					footPageCharList.add(getPageCharInfoForFoot(
							mTargetContentInfo.mTextInfo.get(j).content,
							widthes, currentY,
							mTargetContentInfo.mTextInfo.get(j).kind));
					footIndexList.add(str.length()-1);
                    //先判断加了这个空格之后，会不会超出


					widthes = widthes + fontWidth;
					strWidth = strWidth + fontWidth;
					if (j == mTargetContentInfo.mTextInfo.size() - 1) {
						PageCharInfo mPageCharInfo = getPageCharInfoForChar(
								str, startPositionX, strWidth, currentY,
								fontSize, j + 1, mTargetContentInfo.type,
								isShowLi, false, contentCharSize);
						addPageToPageList(mPageCharInfo, mPageCharInfos);

						if (footPageCharList.size() > 0) {
							for (int k = 0; k < footPageCharList.size(); k++) {
								footPageCharList.get(k).left += footIndexList
										.get(k) * mPageCharInfo.fixWidth;
								mPageCharInfos.mPageCharInfoList
										.add(footPageCharList.get(k));
							}
							footPageCharList.clear();
							footIndexList.clear();
						}

						currentY = currentY + contentLine;
						currentY = currentY
								+ PaintUtil.getBOOK_PAGE_PADDING_CONTENT(ctx);
						if (currentY > textHeight
								&& mPageCharInfos.mPageCharInfoList.size() != 0) {
							savePageShowInfo(mTargetContentInfo.type,
									mPageCharInfos, id, title);
							mPageCharInfos = new PageCharInfos();
							beforeSavePageShowInfo();
							currentY = startY;
							if (isCallStop) {
								break;
							}
						}
						widthes = startX;
						strWidth = 0;
						startPositionX = startX;
						str = "";
						isShowLi = false;
					}
					continue;
				}

				LinkedList<String> lefts = stringToList(mTargetContentInfo.mTextInfo
						.get(j).content);
                if(mTargetContentInfo.type.equals("h1")||mTargetContentInfo.type.equals("h2")||mTargetContentInfo.type.equals("h3")){
                    if(currentY!=startY)
                        currentY=currentY+PaintUtil.getLineHeightByType(ctx,mTargetContentInfo.type);
                    if (currentY > textHeight
                            && mPageCharInfos.mPageCharInfoList.size() != 0) {
                        savePageShowInfo(mTargetContentInfo.type,
                                mPageCharInfos, id, title);
                        mPageCharInfos = new PageCharInfos();
                        beforeSavePageShowInfo();
                        currentY = startY;
                        if (isCallStop) {
                            break;
                        }
                    }
                }
				int index = 0;
				int len = lefts.size();
				while (true) {

					for (; index < len; index++) {
						if (index == 0
								&& mTargetContentInfo.type.equals("blockquote")) {
							widthes = widthes + (2 * fontSize);
							startPositionX = startPositionX + (2 * fontSize);
						}
						float fontWidth = mPaint.measureText(lefts.get(index));

						if (fontWidth + startPositionX > textWidth) {
							LinkedList<String> tempLefts = new LinkedList<String>();
							for (int k = 0; k < index; k++) {
								tempLefts.add(lefts.get(k));
							}
							String temp = lefts.get(index);
							for (int k = 0; k < temp.length(); k++) {
								tempLefts.add(temp.substring(k, k + 1));
							}
							for (int k = index + 1; k < len; k++) {
								tempLefts.add(lefts.get(k));
							}
							lefts = tempLefts;
							len = lefts.size();
						}

						if (fontWidth + widthes > textWidth) {
							widthes = fontWidth + widthes;
							break;
						}
						str = str + lefts.get(index);
						strWidth = fontWidth + strWidth;
						widthes = fontWidth + widthes;
					}
					if (widthes > textWidth
							|| ((j == mTargetContentInfo.mTextInfo.size() - 1) && index == len)) {
						boolean isShowFixWidth = false;
						if (!(index == len && (mTargetContentInfo.type
								.equals("blockquote")||mTargetContentInfo.type
                                .equals("footnote")))
								&& !((j == mTargetContentInfo.mTextInfo.size() - 1) && index == len)
								&& (str.length() - 2) > 0) {
							isShowFixWidth = true;
						}

						PageCharInfo mPageCharInfo = getPageCharInfoForChar(
								str, startPositionX, strWidth, currentY,
								fontSize, j + 1, mTargetContentInfo.type,
								isShowLi, isShowFixWidth, contentCharSize);
						addPageToPageList(mPageCharInfo, mPageCharInfos);

						if (footPageCharList.size() > 0) {
							for (int k = 0; k < footPageCharList.size(); k++) {
								footPageCharList.get(k).left += footIndexList
										.get(k) * mPageCharInfo.fixWidth;
								mPageCharInfos.mPageCharInfoList
										.add(footPageCharList.get(k));
							}
							footPageCharList.clear();
							footIndexList.clear();
						}

						currentY = currentY + contentLine;
						if (index == len) {
							currentY = currentY
									+ PaintUtil
											.getBOOK_PAGE_PADDING_CONTENT(ctx);
						}
						if (currentY > textHeight
								&& mPageCharInfos.mPageCharInfoList.size() != 0) {
							savePageShowInfo(mTargetContentInfo.type,
									mPageCharInfos, id, title);
							mPageCharInfos = new PageCharInfos();
							beforeSavePageShowInfo();
							currentY = startY;
							if (isCallStop) {
								break;
							}
						}
						widthes = startX;
						strWidth = 0;
						startPositionX = startX;
						str = "";
						isShowLi = false;
					}

					if (isCallStop) {
						break;
					}
					if (index >= len) {
						break;
					}

				}

				if (isCallStop) {
					break;
				}
			}
			if (isCallStop) {
				break;
			}
			if (i == (mTargetContentInfoList.size() - 1)
					&& mPageCharInfos.mPageCharInfoList.size() != 0) {
				savePageShowInfo(mTargetContentInfo.type, mPageCharInfos, id,
						title);
				mPageCharInfos = new PageCharInfos();
				beforeSavePageShowInfo();
				currentY = startY;
				if (isCallStop) {
					break;
				}
			}
		}
		if (!isCallStop) {
			// 每一段落生成之后，保存该段落生成结束的缓存信息
			saveContentInfo(id);
		}

	}

	/**
	 * 每一段落生成之后，保存该段落生成结束的缓存信息
	 * 
	 * @param id
	 * @throws IOException
	 */
	public void saveContentInfo(long id) throws IOException {
		Gson gson = new Gson();
		// ContentCacheLoadedInfo alreadyCreateCacheInfo = new
		// ContentCacheLoadedInfo();
		alreadyCreateCacheInfo.page = page;
		alreadyCreateCacheInfo.totalPage = totalPage;
		alreadyCreateCacheInfo.contentcharsize = contentCharSize;
		alreadyCreateCacheInfo.contenttotalsize = totaloffset;
		mBookCacheUtil.saveCacheByTargetContentID(id,
				gson.toJson(alreadyCreateCacheInfo));
	}

	/**
	 * 返回断字之后的内容
	 * 
	 * @param s
	 * @return
	 */
	private LinkedList<String> stringToList(String s) {

		LinkedList<String> pp = new LinkedList<String>();
		BreakIterator boundary = BreakIterator.getLineInstance(Locale
				.getDefault());
		boundary.setText(s);
		int start = boundary.first();
		for (int end = boundary.next(); end != BreakIterator.DONE; start = end, end = boundary
				.next()) {
			pp.add(s.substring(start, end));
		}

		return pp;
	}

	/**
	 * 通过页面的index读取缓存文件-返回对象
	 * 
	 * @param pageIndex
	 * @return
	 * @throws IOException
	 */
	public PageShowInfo readCacheObjectByPageIndex(int pageIndex)
			throws IOException {
		return mBookCacheUtil.readCacheObjectByPageIndex(pageIndex);
	}

	public int getTotalPage() {
		return totalPage;
	}

	public String getTrueImageSrc(String imgSrc) {
		String src = imgSrc;
		if (imgSrc.startsWith("../")) {
			src = Config.getInstance().getBooksDir() + imgSrc.replace("../", "");
		}
		else if (imgSrc.startsWith("..")) {
			src = Config.getInstance().getBooksDir() + "/" + imgSrc.replace("..", "");
		} else {
			src = Config.getInstance().getBookLocation(bookid) + "/" + imgSrc;
		}
		return src;
	}

	public Bitmap adaptationImage(String imgSrc) {
		Bitmap bitmap = BitmapFactory.decodeFile(imgSrc);

		if (bitmap != null) {
			// 如果图片宽度大于屏幕宽度
			if (textWidth < bitmap.getWidth() && !imgSrc.contains("cover.jpg")) {
				float scaleWidth = ((float) textWidth) / bitmap.getWidth();
				Matrix matrix = new Matrix();
				matrix.postScale(scaleWidth, scaleWidth);
				Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
						bitmap.getWidth(), bitmap.getHeight(), matrix, true);

				File file = new File(imgSrc);
				try {
					FileOutputStream out = new FileOutputStream(file);
					if (resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100,
							out)) {
						out.flush();
						out.close();
					}
				} catch (FileNotFoundException e) {
				} catch (IOException e) {
				}

				bitmap = BitmapFactory.decodeFile(imgSrc);

			}
			// 如果是封面，就适配全屏幕
			if (imgSrc.contains("cover.")) {

				float scaleWidth = Config.getInstance().getWindowWidth()
						/ (float) bitmap.getWidth();
				float scaleHeight =Config.getInstance().getWindowHeight()
						/ (float) bitmap.getHeight();

				Matrix matrix = new Matrix();
				matrix.postScale(scaleWidth, scaleHeight);

				Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
						bitmap.getWidth(), bitmap.getHeight(), matrix, true);

				File file = new File(imgSrc);
				try {
					FileOutputStream out = new FileOutputStream(file);
					if (resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100,
							out)) {
						out.flush();
						out.close();
					}
				} catch (FileNotFoundException e) {
				} catch (IOException e) {
				}

				bitmap = BitmapFactory.decodeFile(imgSrc);
			}
		}

		return bitmap;
	}

	public void savePageShowInfo(String type, PageCharInfos mPageCharInfos,
			long contentid, String title) throws IOException {
		Gson gson = new Gson();
		PageShowInfo mPageShowInfo = new PageShowInfo();
		mPageShowInfo.mPageCharInfos = mPageCharInfos;
		mPageShowInfo.chapterPageIndex = page;
		mPageShowInfo.pageCharCount = pageCharSize;
		mPageShowInfo.totalCharCount = totaloffset;
		mPageShowInfo.chapterID = contentid;
		mPageShowInfo.chapterCharCount = contentCharSize;
		mPageShowInfo.chapterTitle = title;
		mBookCacheUtil.saveCacheByPageIndex(totalPage,
				gson.toJson(mPageShowInfo));

		BookPageCountInfo mBookPageCountInfo = new BookPageCountInfo();
		mBookPageCountInfo.pageIndex = totalPage;
		mBookPageCountInfo.startCharCounInChapter = (int) (contentCharSize - pageCharSize);
		mBookPageCountInfo.endCharCountInChapter = (int) contentCharSize;
		alreadyCreateCacheInfo.mBookPageCountInfoList.add(mBookPageCountInfo);

	}

	public void beforeSavePageShowInfo() {
		pageCharSize = 0;
		page++;
		totalPage++;
		noticeUIifPageGreaterThree();
		noticeUIifPageInindex();
	}

	public PageCharInfo getPageCharInfoForImg(String imgSrc, Bitmap bitmap,
			float currentY, String type) {
		PageCharInfo mPageCharInfo = new PageCharInfo();
		mPageCharInfo.content = imgSrc;
		if (imgSrc.contains("cover.jpg")) {
			mPageCharInfo.left = 0;
			mPageCharInfo.right = bitmap.getWidth();
			mPageCharInfo.top = 0;
			mPageCharInfo.bottom = bitmap.getHeight();
		} else {
			mPageCharInfo.left = startX
					+ (textWidth / 2 - bitmap.getWidth() / 2);
			mPageCharInfo.right = startX
					+ (textWidth / 2 - bitmap.getWidth() / 2)
					+ bitmap.getWidth();
			mPageCharInfo.top = currentY;
			mPageCharInfo.bottom = currentY + bitmap.getHeight();
		}
		mPageCharInfo.textInfoIndex = -1;
		mPageCharInfo.kind = type;
		return mPageCharInfo;
	}

	public PageCharInfo getPageCharInfoForFoot(String content, float currentX,
			float currentY, String kind) {
		PageCharInfo mPageCharInfo = new PageCharInfo();
		mPageCharInfo.content = content;

		mPageCharInfo.left = currentX;
		mPageCharInfo.right = currentX + footnoteWidth;
		mPageCharInfo.top = currentY;
		mPageCharInfo.bottom = currentY + footnoteHeight;

		mPageCharInfo.kind = kind;
		return mPageCharInfo;
	}

	public void addPageToPageList(PageCharInfo mPageCharInfo,
			PageCharInfos mPageCharInfos) {
		mPageCharInfos.mPageCharInfoList.add(mPageCharInfo);
		pageCharSize = pageCharSize + mPageCharInfo.content.length();
		contentCharSize += mPageCharInfo.content.length();
		totaloffset = totaloffset + mPageCharInfo.content.length();
	}

	/**
	 * 获得普通的页面内容
	 * 
	 * @param content
	 * @param left
	 * @param fontWidth
	 * @param top
	 * @param fontSize
	 * @param textInfoid
	 * @param type
	 * @param isShowLi
	 * @param isShowFixWidth
	 * @param contentCharSize
	 * @return
	 */
	public PageCharInfo getPageCharInfoForChar(String content, float left,
			float fontWidth, float top, float fontSize, int textInfoid,
			String type, boolean isShowLi, boolean isShowFixWidth,
			long contentCharSize) {
		PageCharInfo mPageCharInfo = new PageCharInfo();
		mPageCharInfo.content = content;
		mPageCharInfo.left = left;
		mPageCharInfo.right = left + fontWidth;
		mPageCharInfo.top = top;
		mPageCharInfo.bottom = top + fontSize;
		mPageCharInfo.textInfoIndex = textInfoid;
		mPageCharInfo.kind = type;
		mPageCharInfo.isShowLI = isShowLi;
		mPageCharInfo.lineInCPIndex = contentCharSize;
		if (isShowFixWidth) {
			mPageCharInfo.fixWidth = (textWidth - mPageCharInfo.right)
					/ ((mPageCharInfo.content.length() - 1));
		} else {
			mPageCharInfo.fixWidth = 0.0f;
		}
		return mPageCharInfo;
	}

	/**
	 * 得到标题页页面内容
	 * 
	 * @param content
	 * @return
	 */
	public PageCharInfos getPageCharInfoInfosForTitle(String content) {
		PageCharInfos mPageCharInfos = new PageCharInfos();
		Paint mPaint = PaintUtil.getPaintByType(ctx, "title");
		float fontSize=mPaint.getTextSize();
		LinkedList<String> lefts = stringToList(content);

		int len = lefts.size();
		float top = textHeight / 2;

		float widthes = startX;
		float end = top;
		int index = 0;
		String str="";
		while(true){
			for (; index < len; index++) {
				
				float fontWidth = mPaint.measureText(lefts.get(index));

				if (fontWidth + startX > textWidth) {
					LinkedList<String> tempLefts = new LinkedList<String>();
					for (int k = 0; k < index; k++) {
						tempLefts.add(lefts.get(k));
					}
					String temp = lefts.get(index);
					for (int k = 0; k < temp.length(); k++) {
						tempLefts.add(temp.substring(k, k + 1));
					}
					for (int k = index + 1; k < len; k++) {
						tempLefts.add(lefts.get(k));
					}
					lefts = tempLefts;
					len = lefts.size();
				}

				if (fontWidth + widthes > textWidth) {
					break;
				}
				str = str + lefts.get(index);
				widthes = fontWidth + widthes;
			}
			PageCharInfo mPageCharInfo=getPageCharInfoForChar(str, startX, widthes, end, fontSize, -1, "title", false, index!=len, -1);
			mPageCharInfos.mPageCharInfoList.add(mPageCharInfo);
			end=end+ PaintUtil.getLineHeightByType(ctx, "title");
			str="";
			widthes = startX;
			if (index>=len) {
				break;
			}
		}
		

		if (mPageCharInfos.mPageCharInfoList.size()!=1) {
			for (int i = 0; i < mPageCharInfos.mPageCharInfoList.size(); i++) {
				mPageCharInfos.mPageCharInfoList.get(i).top -= (end - top) / 2;
			}
		}
		
		return mPageCharInfos;
	}

	/**
	 * unicode 转换成 utf-8
	 * 
	 * @param theString
	 * @return
	 */
	public String unicodeToUtf8(String theString) {
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);
		for (int x = 0; x < len;) {
			aChar = theString.charAt(x++);
			if (aChar == '\\') {
				if (x < len) {
					aChar = theString.charAt(x++);
				}

				if (aChar == 'u') {
					// Read the xxxx
					int value = 0;
					for (int i = 0; i < 4; i++) {
						aChar = theString.charAt(x++);
						switch (aChar) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							value = (value << 4) + aChar - '0';
							break;
						case 'a':
						case 'b':
						case 'c':
						case 'd':
						case 'e':
						case 'f':
							value = (value << 4) + 10 + aChar - 'a';
							break;
						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						case 'F':
							value = (value << 4) + 10 + aChar - 'A';
							break;
						default:
							throw new IllegalArgumentException(
									"Malformed   \\uxxxx   encoding.");
						}
					}
					outBuffer.append((char) value);
				} else {
					if (aChar == 't')
						aChar = '\t';
					else if (aChar == 'r')
						aChar = '\r';
					else if (aChar == 'n')
						aChar = '\n';
					else if (aChar == 'f')
						aChar = '\f';
					outBuffer.append(aChar);
				}
			} else
				outBuffer.append(aChar);
		}
		return outBuffer.toString();
	}

}
