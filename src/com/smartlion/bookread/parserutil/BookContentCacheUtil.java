package com.smartlion.bookread.parserutil;

import java.io.IOException;

import android.content.Context;

import com.google.gson.Gson;
import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.model.PageShowInfo;

/**
 * 书籍的缓存管理类
 * @author 薛文杰
 *
 */
public class BookContentCacheUtil extends CacheUtil{

	Context ctx;
	long bookid;
	//当前读取的字体大小
	private int fontSizeIndex;
	
	//标示是否是读取临时缓存
	private boolean readTemporary=false;
	//临时读取的缓存的章节id
	public long temporaryContentID=-1;
	
	/**
	 * 设置缓存地址
	 * @param ctx
	 * @param bookid
	 * @param fontSizeIndex
	 */
	public BookContentCacheUtil(Context ctx,long bookid,int fontSizeIndex) {
		this.ctx=ctx;
		this.bookid=bookid;
		this.fontSizeIndex=fontSizeIndex;
		localPath=Config.getInstance().getBookLocation(bookid) + "/cache_";
	}
	
	/**
	 * 将该缓存设置成临时缓存
	 * @param temporaryContentID
	 */
	public void setCacheForTemporary(long temporaryContentID){
		localPath=Config.getInstance().getBookLocation(bookid) + "/cache_"+"temporary";
		readTemporary=true;
		this.temporaryContentID=temporaryContentID;
	}
    
    /**
     * 通过页面的index读取缓存文件-返回String
     * @param pageIndex
     * @return
     * @throws IOException
     */
    public String readCacheByPageIndex(int pageIndex) throws IOException{
    	String path=getPageChachePath(pageIndex);
    	return readCache(path);
    }
    
    /**
     * 获取书籍内容缓存文件的地址
     * 临时文件缓存临时文件的地址
     * @param pageIndex
     * @return
     */
    private String getPageChachePath(int pageIndex){
    	if (readTemporary) {
    		return localPath+"fontsize_"+fontSizeIndex+"content_"+temporaryContentID+"page_"+pageIndex;
		}
    	return localPath+"fontsize_"+fontSizeIndex+"page_"+pageIndex;
    }
    
    /**
     * 通过页面的index读取缓存文件-返回对象
     * @param pageIndex
     * @return
     * @throws IOException
     */
    public PageShowInfo readCacheObjectByPageIndex(int pageIndex) throws IOException{
    	PageShowInfo pg=null;
    	Gson gson=new Gson();
    	String json=readCacheByPageIndex(pageIndex);
    	if (!json.equals("")) {
    		pg=gson.fromJson(json, PageShowInfo.class);
    		return pg;
		}
    	return null;
    }
    
    /**
	 * 读取缓存文件-段落保存信息
	 * @param path
	 * @param json
	 * @throws IOException
	 */
    public String readCacheByTargetContentID(long id) throws IOException{
    	String path=localPath+"fontsize_"+fontSizeIndex+"targetContentID_"+id;
    	return readCache(path);
    }
    
    /**
	 * 写入缓存文件
	 * @param path
	 * @param json
	 * @throws IOException
	 */
    public void saveCacheByPageIndex(long pageIndex,String json) throws IOException{
    	String path=getPageChachePath((int) pageIndex);
    	saveCache(path,json);
    }
    
    /**
	 * 写入缓存文件-段落保存信息
	 * @param path
	 * @param json
	 * @throws IOException
	 */
    public void saveCacheByTargetContentID(long id,String json) throws IOException{
    	String path=localPath+"fontsize_"+fontSizeIndex+"targetContentID_"+id;
    	saveCache(path,json);
    }
    
    
    
}
