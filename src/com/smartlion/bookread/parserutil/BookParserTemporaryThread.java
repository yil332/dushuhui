package com.smartlion.bookread.parserutil;

import java.io.IOException;

import android.content.Context;


public class BookParserTemporaryThread extends Thread{

	public BookParserFormatTemporaryUtil mBookParserFormatTemporaryUtil;
	
	public long contentid;
	
	
	public BookParserTemporaryThread(Context ctx,long bookid,int fontSizeIndex,long contentid) {
		mBookParserFormatTemporaryUtil=new BookParserFormatTemporaryUtil(ctx, bookid, fontSizeIndex);
		this.contentid=contentid;
	}
	
	public BookParserTemporaryThread(Context ctx,long bookid,int fontSizeIndex,long contentid,long appointIndex) {
		mBookParserFormatTemporaryUtil=new BookParserFormatTemporaryUtil(ctx, bookid, fontSizeIndex,appointIndex);
		this.contentid=contentid;
	}
	
	public void setToEndPage(boolean isToEndPage){
		mBookParserFormatTemporaryUtil.isToEndPage=isToEndPage;
	}

	public int getTotalPage(){
		return mBookParserFormatTemporaryUtil.getTotalPage();
	}
    
	@Override
	public void run() {
		
		try {
			mBookParserFormatTemporaryUtil.isCallStop=false;
			mBookParserFormatTemporaryUtil.paraserText(contentid);
			
		} catch (IOException e) {
			
		}
	}
	
	
	public void callStop() {
		mBookParserFormatTemporaryUtil.isCallStop=true;
	}
}
