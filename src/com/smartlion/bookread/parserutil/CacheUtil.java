package com.smartlion.bookread.parserutil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * 缓存类
 * @author wenjiexue
 *
 */
public class CacheUtil {
	/**
	 * 保存的路径
	 */
	protected String localPath = "";
	
	/**
	 * 通过具体的路径读取缓存文件－文件不存在就返回空
	 * @param path
	 * @return
	 * @throws IOException
	 */
	protected String readCache(String path) throws IOException{
    	File file=new File(path);
    	if (file.exists()) {
    		FileInputStream readCacheStream=new FileInputStream(path);
    		byte buffer[]=new byte[(int) file.length()];
    		readCacheStream.read(buffer);
    		String json=new String(buffer);
    		readCacheStream.close();
    		return json;
		}
    	return "";
    }
    
    /**
	 * 写入缓存文件
	 * @param path
	 * @param json
	 * @throws IOException
	 */
	protected void saveCache(String path,String json) throws IOException{
    	FileOutputStream fos=new FileOutputStream(path,false);
		fos.write(json.getBytes());
		fos.close();
    }
	
	
}
