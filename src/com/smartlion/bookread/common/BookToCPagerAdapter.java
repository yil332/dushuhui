package com.smartlion.bookread.common;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class BookToCPagerAdapter extends PagerAdapter{

	ListView mTocListView;
	ListView mBookmarkListview;
	
	public BookToCPagerAdapter(ListView mTocListView,ListView mBookmarkListview) {
		this.mTocListView=mTocListView;
		this.mBookmarkListview=mBookmarkListview;
	}
	
	@Override
	public int getCount() {
		return 2;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		if (position == 0) {
			container.addView(mTocListView);
			return mTocListView;
		} else {
			container.addView(mBookmarkListview);
			return mBookmarkListview;
		}
	}

	@Override
	public void destroyItem(ViewGroup container, int position,
			Object object) {
		((ViewPager) container).removeView((View) object);
	}
}
