/**
 * 
 */
package com.smartlion.bookread.common;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.whalefin.dushuhui.R;

/**
 * @author niyanshi
 * 
 */
public class BookmarkAdapter extends BaseAdapter {
	private List<BookMarkerShowListInfo>	bookmarks	= null;
	private LayoutInflater	inflater	= null;

	private Context			ctx			= null;

	public BookmarkAdapter(Context context, List<BookMarkerShowListInfo> bookmarks) {
		this.bookmarks = bookmarks;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.ctx = context;
	}

	private static class ViewHolder {
		TextView	bookmarkContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return bookmarks.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		return bookmarks.get(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return bookmarks.get(position).pageIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		BookMarkerShowListInfo mBookMarkerShowListInfo = bookmarks.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.booklib_bookmark_row_view, null);
			holder.bookmarkContent = (TextView) convertView.findViewById(R.id.bookmark_content);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.bookmarkContent.setText(mBookMarkerShowListInfo.content);

		return convertView;
	}

	/**
	 * @return the bookmarks
	 */
	public List<BookMarkerShowListInfo> getBookmarks() {
		return bookmarks;
	}

	/**
	 * @param bookmarks
	 *            the bookmarks to set
	 */
	public void setBookmarks(List<BookMarkerShowListInfo> bookmarks) {
		this.bookmarks = bookmarks;
	}

}
