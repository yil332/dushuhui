package com.smartlion.bookread.common;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.smartlion.bookread.inf.BookReadInterface;
import com.smartlion.bookread.inf.BookReadProxy;
import com.smartlion.bookread.inf.ProgressViewInterface;
import com.whalefin.dushuhui.R;


public class ProgressView extends View implements ProgressViewInterface{

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */

	public float currentx = 0;
	public float currenty = 50;
	public RectF circleRect;
	public RectF boundRect;
	public float circleWidth=60;
	public float boundHeight,boundWidth;
	public float startHeight;
	public float endHeight;
	public int max;
	public int progress;
	private BookReadProxy proxy;
	private Bitmap progressBar,thumb,arrow;
	private View parentView;
	private Context ctx;
	private boolean isShowProgress=false;
	float winHeight=0,windowWidth;

	/**
	 * 
	 * @param context
	 */
	public ProgressView(Context context,float startHeight,float endHeight,int max,View parentView ) {
		super(context);
		ctx=context;
		proxy=new BookReadProxy((BookReadInterface)context);
		Display display = ((Activity)ctx).getWindowManager().getDefaultDisplay();  
		winHeight=display.getHeight();
		windowWidth = display.getWidth();
		this.startHeight=startHeight;
		this.endHeight=endHeight;
		this.max=max;
		this.parentView=parentView;
		
		thumb = BitmapFactory.decodeResource(context.getResources(), R.drawable.nav_sliding_pane_slider_thumb);
		circleRect=new RectF(0,0,0,0);
		
		progressBar = BitmapFactory.decodeResource(context.getResources(), R.drawable.booklib_nav_pane_handle);
		progressBar=adaptationImage(progressBar,thumb.getWidth());
		boundHeight=progressBar.getHeight();
		boundWidth=progressBar.getWidth();
		
		boundRect=new RectF(0, 0, boundWidth*2, boundHeight);
		
		arrow=BitmapFactory.decodeResource(context.getResources(), R.drawable.booklib_nav_pane_handle_arrow);
		
	}
	
	public void reSetBoundRect(){
		boundRect=new RectF(0, 0, boundWidth*2, boundHeight);
	}
	
	public Bitmap adaptationImage(Bitmap mBitmap,float toWidth) {
		Bitmap resizedBitmap=mBitmap;
		if (mBitmap != null) {
			Matrix matrix = new Matrix();
			
			float scaleHeight=winHeight/mBitmap.getHeight();
			float scaleWidth=(toWidth+20) / mBitmap.getWidth();
			matrix.postScale(scaleWidth, scaleHeight);
			resizedBitmap = Bitmap.createBitmap(mBitmap, 0, 0,
					mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
		}
		return resizedBitmap;
	}
	
	public void setProgerss(int progress,int max,boolean isAllLoad){
		isShowProgress=isAllLoad;
		if (isShowProgress) {
			this.max=max;
			this.progress=progress;
			currenty=(endHeight-startHeight)*progress/max+startHeight;
			circleRect=new RectF(currentx-circleWidth, currenty-circleWidth, currentx+(circleWidth*2), currenty+circleWidth);
			invalidate();
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// 创建画笔 ;
		
		canvas.drawBitmap(progressBar, currentx, 0, PaintUtil.getPaint_BREAK(ctx));
		canvas.drawBitmap(arrow, currentx+progressBar.getWidth()/4, winHeight/2, PaintUtil.getPaint_BREAK(ctx));
		if (isShowProgress) {
			canvas.drawBitmap(thumb, currentx+progressBar.getWidth()-thumb.getWidth()-8, currenty, PaintUtil.getPaint_BREAK(ctx));
		}
		Path path=new Path();
		path.moveTo(currentx+3, 0);
		path.lineTo(currentx+3, winHeight);
		path.close();
		canvas.drawPath(path, PaintUtil.getPaint_BREAK(ctx));
		Path path1=new Path();
		path1.moveTo(currentx, 0);
		path1.lineTo(currentx, winHeight);
		path1.close();
		canvas.drawPath(path1, PaintUtil.getPaint_BREAK(ctx));
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		this.setMeasuredDimension(progressBar.getWidth(),progressBar.getHeight());
	}
	
	public boolean isInCircle=false;
	public boolean isInBound=false;
	
	public float startMoveX;
	
	
	long downTime=0;
	
//	@Override
//	public boolean onTouchEvent(MotionEvent event) {
//		switch (event.getAction()) {
//		case MotionEvent.ACTION_DOWN:
//			
//			if (isContains(circleRect,event.getX(), event.getY())) {
//				isInCircle=true;
//				currenty = event.getY();
//				circleRect.top=currenty-circleWidth;
//				circleRect.bottom=currenty+circleWidth;
//				progress=(int)((currenty-startHeight)/(endHeight-startHeight)*max);
//				invalidate();
//				show(progress,(int)event.getY());
//				return true;
//			}
//			else if (isContains(boundRect, event.getX(), event.getY())) {
//				downTime=System.currentTimeMillis();
//				isInBound=true;
//				return true;
//			}
//			
//		case MotionEvent.ACTION_MOVE:
//			
//			if (isInCircle) {
//				if (event.getY()>startHeight&&event.getY()<endHeight) {
//					currenty = event.getY();
//					circleRect.top=currenty-circleWidth;
//					circleRect.bottom=currenty+circleWidth;
//					progress=(int)((currenty-startHeight)/(endHeight-startHeight)*max);
//					invalidate();
//					show(progress,(int)event.getY());
//				}else if(event.getY()<startHeight){
//					currenty = startHeight;
//					circleRect.top=currenty-circleWidth;
//					circleRect.bottom=currenty+circleWidth;
//					progress=1;
//					invalidate();
//					show(progress,(int)event.getY());
//				}else{
//					currenty = endHeight;
//					circleRect.top=currenty-circleWidth;
//					circleRect.bottom=currenty+circleWidth;
//					progress=max;
//					invalidate();
//					show(progress,(int)event.getY());
//				}
//				return true;
//			}
//			else if (isInBound) {
//				if (event.getRawX()<windowWidth*9/10) {
//					proxy.moveView(event.getRawX());
//				}
//				
//				
//				return true;
//			}
//			
//			
//		case MotionEvent.ACTION_UP:
//		default:
//			
//			if (isInCircle) {
//				isInCircle=false;
//				if (event.getY()>=startHeight&&event.getY()<=endHeight) {
//					currenty = event.getY();
//					circleRect.top=currenty-circleWidth;
//					circleRect.bottom=currenty+circleWidth;
//					progress=(int)((currenty-startHeight)/(endHeight-startHeight)*max);
//					invalidate();
//					popupWindow.dismiss();
//				}
//				proxy.changeViewPageItem(progress+PageViewAdapter.centerPageOffset);
//				return true;
//			}else if (isInBound) {
//				isInBound=false;
//				
//				if ((System.currentTimeMillis()-downTime)<100) {
//					if (((BookReadActivity)ctx).isShowDlg) {
//						proxy.moveView(0);
//						((BookReadActivity)ctx).isShowDlg=false;
//						proxy.unSetAdapter();
//					}else{
//						proxy.moveView(windowWidth*9/10);
//						((BookReadActivity)ctx).isShowDlg=true; 
//						proxy.setAdapter();
//					}
//				}
//				else{
//					if (event.getRawX()>windowWidth*2/3) {
//						proxy.moveView(windowWidth*9/10);
//						((BookReadActivity)ctx).isShowDlg=true; 
//						proxy.setAdapter();
//					}else{
//						proxy.moveView(0);
//						((BookReadActivity)ctx).isShowDlg=false;
//						proxy.unSetAdapter();
//					}
//				}
//				
//					
//				return true;
//			}
//			
//		}
//		return false;
//	}
	
	
	
	public boolean isContains(RectF rectF,float x,float y){
		if (rectF.left<=x&&rectF.right>=x&&rectF.top<=y&&rectF.bottom>=y) {
			return true;
		}
		return false;
	}
	private View view=null;
	public PopupWindow popupWindow = null;
	private TextView TextView01;
	
	public void show(int position,int y){
		if (popupWindow==null) {
			LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater
					.inflate(R.layout.booklib_show_pagenum_view, null);
			TextView01=(TextView)view.findViewById(R.id.TextView01);
			popupWindow = new PopupWindow(view, 200,100);
			popupWindow.showAtLocation(parentView,
					Gravity.LEFT | Gravity.TOP, 100, y);
			
		}
		if (popupWindow.isShowing()==false) {
			popupWindow.showAtLocation(parentView,
					Gravity.LEFT | Gravity.TOP, 100, y);
		}
		
		popupWindow.update(100, y, 200, 100);
		TextView01.setText("第"+position+"页");
	}

	@Override
	public void setProgress(int progress) {
		this.progress=progress;
		show(progress+1,(int)currenty);
	}
	
	

	@Override
	public void setCurrenty(float currenty) {
		this.currenty=currenty;
	}

	@Override
	public void setCircleRect(RectF circleRect) {
		this.circleRect=circleRect;
	}

	@Override
	public void setBoundRect(RectF boundRect) {
		this.boundRect=boundRect;
	}

	@Override
	public RectF getCircleRect() {
		// TODO Auto-generated method stub
		return circleRect;
	}

	@Override
	public RectF getBoundRect() {
		// TODO Auto-generated method stub
		return boundRect;
	}

	@Override
	public float getCircleWidth() {
		// TODO Auto-generated method stub
		return circleWidth;
	}

	@Override
	public float getStartHeight() {
		// TODO Auto-generated method stub
		return startHeight;
	}

	@Override
	public float getEndHeight() {
		// TODO Auto-generated method stub
		return endHeight;
	}

	@Override
	public int getMax() {
		// TODO Auto-generated method stub
		return max;
	}

	@Override
	public void toInvalidate() {
		proxy.moveView(0);
		this.invalidate();
	}

	@Override
	public int getProgress() {
		// TODO Auto-generated method stub
		return progress;
	}

	@Override
	public void disShowWind() {
		if (popupWindow!=null&&popupWindow.isShowing()) {
			popupWindow.dismiss();
		}
		
	}

	@Override
	public void changeDirection(int direction) {
		if (direction==0) {
			arrow=BitmapFactory.decodeResource(ctx.getResources(), R.drawable.booklib_nav_pane_handle_arrow);
		}else{
			arrow=BitmapFactory.decodeResource(ctx.getResources(), R.drawable.booklib_nav_pane_handle_direction_arrow);
		}
		this.invalidate();
	}
}