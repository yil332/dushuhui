package com.smartlion.bookread.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.whalefin.dushuhui.util.FileUtil;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.WindowManager;



public class Config {

	public static final String PREFS_NAME = "reading_configuration";
	private static Config config;
	private Context ctx;
	private SharedPreferences settings;
	private Editor editor;

	public static final String BOOK_SAVE_PATH = "book_save_path";
	private static final String PROP_BOOK_SCALE = "book_scale";
	private static final String PROP_BRIGHTNESS = "window_brightness";

	public static Config getInstance() {
		return config;
	}

	public Config(Context ctx) {
		this.ctx = ctx;
		settings = ctx.getSharedPreferences(PREFS_NAME, 0);
		editor = settings.edit();
		config = this;
	}

	public void saveBookSavePath(String path) {
		editor.putString(BOOK_SAVE_PATH, path);
		editor.commit();
	}

	public String loadBookSavePath() {
		return FileUtil.downloadDirectory;
	}

	public String getBooksDir() {
		return loadBookSavePath();
	}

	public String getBookLocation(Long bookId) {
		return loadBookSavePath() + "/" + bookId;
	}

	public float getWindowWidth() {
		WindowManager wm = (WindowManager) ctx
				.getSystemService(Context.WINDOW_SERVICE);
		return wm.getDefaultDisplay().getWidth();
	}

	public float getWindowHeight() {
		WindowManager wm = (WindowManager) ctx
				.getSystemService(Context.WINDOW_SERVICE);
		return wm.getDefaultDisplay().getHeight();
	}

	public int getBookScale() {
		return settings.getInt(PROP_BOOK_SCALE, 1);
	}

	public void setBookScale(int textSize) {
		editor.putInt(PROP_BOOK_SCALE, textSize);
		editor.commit();
	}

	public void setWindowBrightness(int bright) {
		editor.putInt(PROP_BRIGHTNESS, bright);
		editor.commit();
	}

	public int getWindowBrightness() {
		return settings.getInt(PROP_BRIGHTNESS, 50);
	}

	public void setBookProgress(int bookId, int fontSizeIndex, int page) {
		editor.putInt("bookProgress" + "bookId" + bookId + "fontSize"
				+ fontSizeIndex, page);
		editor.commit();
	}

	public int getBookProgress(int bookId, int fontSizeIndex) {
		int bookProgress = settings.getInt("bookProgress" + "bookId" + bookId
				+ "fontSize" + fontSizeIndex, 0);
		return bookProgress;
	}

	public void Unzip(String zipFile, String targetDir) {
		int BUFFER = 4096; // 这里缓冲区我们使用4KB，
		String strEntry; // 保存每个zip的条目名称

		try {
			BufferedOutputStream dest = null; // 缓冲输出流
			FileInputStream fis = new FileInputStream(zipFile);
			ZipInputStream zis = new ZipInputStream(
					new BufferedInputStream(fis));
			ZipEntry entry; // 每个zip条目的实例

			while ((entry = zis.getNextEntry()) != null) {

				try {
					int count;
					byte data[] = new byte[BUFFER];
					strEntry = entry.getName();

					File entryFile = new File(targetDir + strEntry);
					File entryDir = new File(entryFile.getParent());
					if (!entryDir.exists()) {
						entryDir.mkdirs();
					}

					FileOutputStream fos = new FileOutputStream(entryFile);
					dest = new BufferedOutputStream(fos, BUFFER);
					while ((count = zis.read(data, 0, BUFFER)) != -1) {
						dest.write(data, 0, count);
					}
					dest.flush();
					dest.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			zis.close();
		} catch (Exception cwj) {
			cwj.printStackTrace();
		}
	}

}
