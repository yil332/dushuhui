/**
 * 
 */
package com.smartlion.bookread.common;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartlion.bookread.model.parserzip.ContentInfo;
import com.smartlion.bookread.model.parserzip.ManifestInfo;
import com.whalefin.dushuhui.R;



/**
 * 书籍目录显示适配器
 * @author wenjiexue
 *
 */
public class NewBookTocAdapter extends BaseAdapter {
	private ManifestInfo mManifestInfo=null;
	private LayoutInflater		inflater	= null;
	public long					currentSection;

	private Context				ctx			= null;
	

	public NewBookTocAdapter(Context context, ManifestInfo mManifestInfo) {
		this.mManifestInfo = mManifestInfo;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.ctx = context;
	}

	private static class ViewHolder {
		TextView	sectionName;
		ImageView	tocSign;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return mManifestInfo.mContentInfo.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		return mManifestInfo.mContentInfo.get(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		
		return Long.valueOf(mManifestInfo.mContentInfo.get(position).id);
	}
	
	public String getItemTitle(int position) {
		
		return mManifestInfo.mContentInfo.get(position).title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		ContentInfo mContentInfo=mManifestInfo.mContentInfo.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.booklib_toc_row_view, null);

			holder.sectionName = (TextView) convertView.findViewById(R.id.section_name);
			holder.tocSign = (ImageView) convertView.findViewById(R.id.toc_sign);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (mContentInfo.mChildrenInfo.size() > 0) {
			//holder.tocSign.setImageResource(R.drawable.book_toc_sign_big);
			holder.sectionName.setPadding(0, 0, 0, 0);
//			holder.sectionName.setTextAppearance(ctx, R.style.TocTextLarge);
		} else {
			//holder.tocSign.setImageResource(R.drawable.book_toc_sign_small);
			holder.sectionName.setPadding(20, 0, 0, 0);
			//holder.sectionName.setTextAppearance(ctx, R.style.TocTextSmall);
		}
		holder.sectionName.setText(mContentInfo.title);

		if (getItemId(position)==currentSection) {
			convertView.setBackgroundColor(ctx.getResources().getColor(R.color.pink));
		} else {
			convertView.setBackgroundColor(ctx.getResources().getColor(R.color.transparent));
		}

		return convertView;
	}

}
