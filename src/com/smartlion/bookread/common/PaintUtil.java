package com.smartlion.bookread.common;

import com.whalefin.dushuhui.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Typeface;



/**
 * 获取各类画笔的类
 * @author wenjiexue
 *
 */
public class PaintUtil {
	
	static {
		//正文字体大小
		int[] txt_sz_normal = new int[3];
		txt_sz_normal[0] = 18;
		txt_sz_normal[1] = 20;
		txt_sz_normal[2] = 22;
		TEXT_SIZE_NORMAL = txt_sz_normal;

		//标题字体大小
		int[] txt_sz_title = new int[3];
		txt_sz_title[0] = 22;
		txt_sz_title[1] = 24;
		txt_sz_title[2] = 26;
		TEXT_SIZE_TITLE = txt_sz_title;

		//
		int[] txt_sz_block_quote = new int[3];
		txt_sz_block_quote[0] = 16;
		txt_sz_block_quote[1] = 18;
		txt_sz_block_quote[2] = 20;
		TEXT_SIZE_BLOCK_QUOTE = txt_sz_block_quote;

		int[] linespace = new int[3];
		linespace[0] = 12;
		linespace[1] = 14;
		linespace[2] = 16;
		LINE_SPACE = linespace;

		int[] linespace_title = new int[3];
		linespace_title[0] = 18;
		linespace_title[1] = 20;
		linespace_title[2] = 22;
		LINE_SPACE_TITLE = linespace_title;
	}
	
	public static final int[]	TEXT_SIZE_NORMAL;
	public static final int[]	TEXT_SIZE_TITLE;
	public static final int[]	TEXT_SIZE_BLOCK_QUOTE;
	public static final int[]	LINE_SPACE;
	public static final int[]	LINE_SPACE_TITLE;
	public static final int		BOOK_PAGE_PADDING_X							= 30;
	public static final int		BOOK_PAGE_PADDING_TOP						= 60;
	public static final int		BOOK_PAGE_PADDING_CONTENT					= 5;
	
	public static String PARAGRAPHTYPE_PARAGRAPH="p";
	public static String PARAGRAPHTYPE_BLOCKQUOTE="blockquote";
	public static String PARAGRAPHTYPE_H1="h1";
	public static String PARAGRAPHTYPE_H2="h2";
	public static String PARAGRAPHTYPE_H3="h3";
	public static String PARAGRAPHTYPE_H4="h4";
	public static String PARAGRAPHTYPE_H5="h5";
	public static String PARAGRAPHTYPE_H6="h6";
	public static String PARAGRAPHTYPE_LIST="li";
	public static String PARAGRAPHTYPE_IMAGE="img";
	public static String PARAGRAPHTYPE_VIDEO="video";
	public static String PARAGRAPHTYPE_TITLE="title";
	
	public static String TextKind_PLAINTEXT="plaintext";
	public static String TextKind_FOOTNOTE="footnote";
	
	public static String PAGENUMBER="pagenumber";
	
	public static float getBOOK_PAGE_PADDING_X(Context ctx){
		return dp2pixel(ctx, BOOK_PAGE_PADDING_X);
	}
	
	public static float getBOOK_PAGE_PADDING_TOP(Context ctx){
		return dp2pixel(ctx, BOOK_PAGE_PADDING_TOP);
	}
	
	public static float getBOOK_PAGE_PADDING_CONTENT(Context ctx){
		return dp2pixel(ctx, BOOK_PAGE_PADDING_CONTENT);
	}
	
	public static float getLineHeightByType(Context ctx,String type){
		return getTextSizeByType(ctx,type)+getLineSpaceByType(ctx,type);
	}
	
	public static Paint getPaintByType(Context ctx,String type){
		if (type.equals(PARAGRAPHTYPE_PARAGRAPH)) {
			return getPaint_PARAGRAPH(ctx);
		}else if (type.equals(PARAGRAPHTYPE_BLOCKQUOTE)) {
			return getPaint_BLOCKQUOTE(ctx);
		}else if (type.equals(PARAGRAPHTYPE_H1)) {
			return getPaint_H1(ctx);
		}else if (type.equals(PARAGRAPHTYPE_H2)) {
			return getPaint_H2(ctx);
		}else if (type.equals(PARAGRAPHTYPE_H3)) {
			return getPaint_H3(ctx);
		}else if (type.equals(PARAGRAPHTYPE_H4)) {
			return getPaint_H4(ctx);
		}else if (type.equals(PARAGRAPHTYPE_H5)) {
			return getPaint_H5(ctx);
		}else if (type.equals(PARAGRAPHTYPE_H5)) {
			return getPaint_H6(ctx);
		}else if (type.equals(PARAGRAPHTYPE_LIST)) {
			return getPaint_LIST(ctx);
		}else if (type.equals(PARAGRAPHTYPE_TITLE)) {
			return getPaint_TITLE(ctx);
		}else if (type.equals(PAGENUMBER)) {
			return getPaint_PAGENUMBER(ctx);
		}
		return getPaint_PARAGRAPH(ctx);
	}
	
	public static float getTextSizeByType(Context ctx,String type){
		if (type.equals(PARAGRAPHTYPE_PARAGRAPH)) {
			return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_BLOCKQUOTE)) {
			return dp2pixel(ctx, TEXT_SIZE_BLOCK_QUOTE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_H1)) {
			return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()]+4);
		}else if (type.equals(PARAGRAPHTYPE_H2)) {
			return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()]+2);
		}else if (type.equals(PARAGRAPHTYPE_H3)) {
			return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()]+2);
		}else if (type.equals(PARAGRAPHTYPE_H4)) {
			return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()]+2);
		}else if (type.equals(PARAGRAPHTYPE_H5)) {
			return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()]+2);
		}else if (type.equals(PARAGRAPHTYPE_H6)) {
			return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()]+2);
		}else if (type.equals(PARAGRAPHTYPE_LIST)) {
			return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_TITLE)) {
			return dp2pixel(ctx, TEXT_SIZE_TITLE[Config.getInstance().getBookScale()])*3/2;
		}else if (type.equals(PAGENUMBER)) {
			return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()])*2/3;
		}
		return dp2pixel(ctx, TEXT_SIZE_NORMAL[Config.getInstance().getBookScale()]);
	}
	
	public static float getLineSpaceByType(Context ctx,String type){
		if (type.equals(PARAGRAPHTYPE_PARAGRAPH)) {
			return dp2pixel(ctx, LINE_SPACE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_BLOCKQUOTE)) {
			return dp2pixel(ctx, LINE_SPACE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_H1)) {
			return dp2pixel(ctx, LINE_SPACE_TITLE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_H2)) {
			return dp2pixel(ctx, LINE_SPACE_TITLE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_H3)) {
			return dp2pixel(ctx, LINE_SPACE_TITLE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_H4)) {
			return dp2pixel(ctx, LINE_SPACE_TITLE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_H5)) {
			return dp2pixel(ctx, LINE_SPACE_TITLE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_H5)) {
			return dp2pixel(ctx, LINE_SPACE_TITLE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_LIST)) {
			return dp2pixel(ctx, LINE_SPACE_TITLE[Config.getInstance().getBookScale()]);
		}else if (type.equals(PARAGRAPHTYPE_TITLE)) {
			return dp2pixel(ctx, LINE_SPACE_TITLE[Config.getInstance().getBookScale()]);
		}
		return dp2pixel(ctx, LINE_SPACE[Config.getInstance().getBookScale()]);
	}
	
	/**
	 * 获取PARAGRAPH的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_PARAGRAPH(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_PARAGRAPH);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(false);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取BLOCKQUOTE的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_BLOCKQUOTE(Context ctx){
		float textSizeBlockQuote = getTextSizeByType(ctx,PARAGRAPHTYPE_BLOCKQUOTE);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(false);
		mPaint.setTypeface(Typeface.create(Typeface.MONOSPACE, Typeface.ITALIC));
		mPaint.setTextSkewX(-0.2f);
		mPaint.setTextSize(textSizeBlockQuote);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取H1的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_H1(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_H1);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(true);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取H2的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_H2(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_H2);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(true);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取H3的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_H3(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_H3);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(true);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取H4的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_H4(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_H4);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(true);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取H5的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_H5(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_H5);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(true);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取H6的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_H6(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_H6);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(true);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取LIST的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_LIST(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_LIST);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(false);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取FOOTNOTE-Circle的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_LIST_CIRCLE(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_PARAGRAPH);
		Paint mPaint = new Paint();
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取PARAGRAPH的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_TITLE(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_TITLE);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.dark));
		mPaint.setFakeBoldText(true);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取画页数显示的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_PAGENUMBER(Context ctx){
		float textSize = getTextSizeByType(ctx,PAGENUMBER);
		Paint mPaint = new Paint();
		mPaint.setTextAlign(Align.LEFT);
		mPaint.setColor(ctx.getResources().getColor(R.color.light_gray));
		mPaint.setFakeBoldText(false);
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	
	/**
	 * 获取画标记的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_BREAK(Context ctx){
		Paint mPaint = new Paint();  
		mPaint.setAntiAlias(true);  
		mPaint.setColor(Color.BLACK);  
		mPaint.setStyle(Paint.Style.STROKE);  
		mPaint.setStrokeWidth(1);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取画标记的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_PATH(Context ctx){
		Paint mPaint = new Paint();  
		mPaint.setAntiAlias(true);  
		mPaint.setColor(Color.BLUE);  
		mPaint.setStyle(Paint.Style.STROKE);  
		mPaint.setStrokeWidth(3);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	
	/**
	 * 获取FOOTNOTE-Circle的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_FOOTNOTE_CIRCLE(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_PARAGRAPH);
		Paint mPaint = new Paint();
		mPaint.setColor(ctx.getResources().getColor(R.color.footnote_bg));
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	/**
	 * 获取FOOTNOTE-CHAR的画笔
	 * @param ctx
	 * @return
	 */
	public static Paint getPaint_FOOTNOTE_CHAR(Context ctx){
		float textSize = getTextSizeByType(ctx,PARAGRAPHTYPE_PARAGRAPH)* 2 / 3;
		Paint mPaint = new Paint();
		mPaint.setColor(ctx.getResources().getColor(R.color.white));
		mPaint.setTextSize(textSize);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	public static Paint getPaint_NORMAL(Context ctx){
		Paint mPaint = new Paint();
		mPaint.setColor(ctx.getResources().getColor(R.color.myread_bg));
		mPaint.setStyle(Style.FILL);
		mPaint.setAntiAlias(true);
		return mPaint;
	}
	
	public static int dp2pixel(Context context, int i) {
		return (int) (0.5F + context.getResources().getDisplayMetrics().density * (float) i);
	}
	
}
