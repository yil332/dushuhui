package com.smartlion.bookread.common;

import com.smartlion.bookread.BookReadActivity;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;


public class MyViewPager extends ViewPager {

    public static boolean isCanScroll = false;
    private Context ctx;

    public MyViewPager(Context context) {
        super(context);
        ctx = context;
    }

    public MyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        ctx = context;
    }

    @Override
    public boolean onTouchEvent(MotionEvent arg0) {
        if (arg0.getAction() == MotionEvent.ACTION_UP) {
            if (((BookReadActivity) ctx).isShowReaderHub) {
                ((BookReadActivity) ctx).disShowReaderHub();
            }
        }
        return super.onTouchEvent(arg0);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        // TODO Auto-generated method stub
        if (isCanScroll) {
            return false;
        }
        return super.onInterceptTouchEvent(arg0);
    }

}
