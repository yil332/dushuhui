package com.smartlion.bookread.common;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.gson.JsonSyntaxException;
import com.smartlion.bookread.BookReadActivity;
import com.smartlion.bookread.customui.BookLabelView;
import com.smartlion.bookread.customui.BookReadingPageView;
import com.smartlion.bookread.inf.FragmentInterface;
import com.smartlion.bookread.inf.ProgressViewInterface;
import com.smartlion.bookread.model.BookMarkerInfo;
import com.smartlion.bookread.model.BookMarkerInfos;
import com.smartlion.bookread.model.PageShowInfo;
import com.whalefin.dushuhui.R;

public class BookPageFragment extends Fragment implements FragmentInterface {

	private static Context mContext;

	private ImageView bookmarkImage, imageView_mark_disabled,
			imageView_loading;

	private boolean isShowMarker = false;
	
	
	private static ProgressViewInterface mProgressViewInterface;

	static Fragment newInstance(int pageNumber, boolean isTemporary, Context cxt,ProgressViewInterface inf) {
		BookPageFragment pagefragment = new BookPageFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("pageNo", pageNumber);
		bundle.putBoolean("getTemporary", isTemporary);
		pagefragment.setArguments(bundle);
		mContext = cxt;
		mProgressViewInterface=inf;
		return pagefragment;
	}

	@SuppressLint("CutPasteId")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		int pageNo = getArguments().getInt("pageNo");
		boolean getTemporary = getArguments().getBoolean("getTemporary");
		FrameLayout bookReadLayout = (FrameLayout) inflater.inflate(
				R.layout.booklib_read_page, null);
		bookmarkImage = (ImageView) bookReadLayout
				.findViewById(R.id.imageView_mark);
		imageView_mark_disabled = (ImageView) bookReadLayout
				.findViewById(R.id.imageView_mark_disabled);
		imageView_loading = (ImageView) bookReadLayout
				.findViewById(R.id.imageView_loading);
		PageShowInfo pg = null;
		try {
			if (pageNo >= 0) {
				if (mContext != null) {
					if (getTemporary) {

						if (((BookReadActivity) mContext).mBookTemporaryCacheUtil != null) {
							pg = ((BookReadActivity) mContext).mBookTemporaryCacheUtil
									.readCacheObjectByPageIndex(pageNo);
						}

					} else {
						if (((BookReadActivity) mContext).mBookCacheUtil != null) {
							pg = ((BookReadActivity) mContext).mBookCacheUtil
									.readCacheObjectByPageIndex(pageNo);
						}
					}
				}
				if (pg != null) {
					BookMarkerInfos mBookMarkerInfos = ((BookReadActivity) mContext).mBookMarkerCacheUtil
							.loadMarkerList(pg.chapterID);

					for (int i = 0; i < mBookMarkerInfos.mBookMarkerInfosList
							.size(); i++) {
						if (((pg.chapterCharCount - pg.pageCharCount) <= mBookMarkerInfos.mBookMarkerInfosList
								.get(i).markerIndex)
								&& (pg.chapterCharCount > mBookMarkerInfos.mBookMarkerInfosList
										.get(i).markerIndex)) {
							bookmarkImage.setVisibility(View.VISIBLE);
							isShowMarker = true;
							break;
						}
					}

					BookReadingPageView page = new BookReadingPageView(
							mContext, pg, getTemporary, this);
					page.paintByPageNo(pageNo);
					BookLabelView mBookLabelView = new BookLabelView(mContext,
							page, this, isShowMarker,mProgressViewInterface);
					mBookLabelView.setBook(pg, pageNo,
							((BookReadActivity) mContext).mBookParserThread);
					bookReadLayout.addView(page);
					bookReadLayout.addView(mBookLabelView);
				}else{
					imageView_loading.setVisibility(View.GONE);
				}

			}

		} catch (IOException e) {
		} catch (JsonSyntaxException e) {
		}
		imageView_mark_disabled.bringToFront();
		bookmarkImage.bringToFront();
		imageView_loading.bringToFront();
		return bookReadLayout;
	}

	@Override
	public void showBookMarkImage(int contentid, int sectionID,
			Integer markerStartIndex, String content) {
		if (isShowMarker) {
			imageView_mark_disabled.setVisibility(View.GONE);
			bookmarkImage.setVisibility(View.GONE);

			try {
				((BookReadActivity) mContext).mBookMarkerCacheUtil
						.deleteMarker(contentid, markerStartIndex);
			} catch (IOException e) {
			}

			isShowMarker = false;
		} else {
			imageView_mark_disabled.setVisibility(View.GONE);
			bookmarkImage.setVisibility(View.VISIBLE);
			try {
				BookMarkerInfos mBookMarkerInfos = ((BookReadActivity) mContext).mBookMarkerCacheUtil
						.loadMarkerList(contentid);
				boolean isExist = false;
				for (int i = 0; i < mBookMarkerInfos.mBookMarkerInfosList
						.size(); i++) {
					if (mBookMarkerInfos.mBookMarkerInfosList.get(i).markerIndex == markerStartIndex) {
						isExist = true;
					}
				}
				if (!isExist) {
					BookMarkerInfo mBookMarkerInfo = new BookMarkerInfo();
					mBookMarkerInfo.sectionID = sectionID;
					mBookMarkerInfo.chapterID = contentid;
					mBookMarkerInfo.markerIndex = markerStartIndex;
					mBookMarkerInfo.content = content;

					mBookMarkerInfos.mBookMarkerInfosList.add(mBookMarkerInfo);

					((BookReadActivity) mContext).mBookMarkerCacheUtil
							.saveMarkerList(contentid, mBookMarkerInfos);

				}
				isShowMarker = true;
			} catch (IOException e) {
			}
		}

	}

	@Override
	public void showImageViewMarkDisabled(boolean isShow) {
		if (isShow) {
			imageView_mark_disabled.setVisibility(View.VISIBLE);
			bookmarkImage.setVisibility(View.GONE);
		} else {
			imageView_mark_disabled.setVisibility(View.GONE);
			bookmarkImage.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void showBookMarkImage(boolean isShow) {
		if (isShow) {
			imageView_mark_disabled.setVisibility(View.GONE);
			bookmarkImage.setVisibility(View.VISIBLE);
		} else {
			imageView_mark_disabled.setVisibility(View.VISIBLE);
			bookmarkImage.setVisibility(View.GONE);
		}
	}

	@Override
	public void loadCompleted() {
		imageView_loading.setVisibility(View.GONE);
	}

}