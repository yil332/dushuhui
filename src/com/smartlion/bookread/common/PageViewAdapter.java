package com.smartlion.bookread.common;


import com.smartlion.bookread.inf.ProgressViewInterface;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * 翻页适配器
 * @author wenjiexue
 *
 */
public class PageViewAdapter extends FragmentStatePagerAdapter {
	
	private Context mContext;
	
	private int pageCount;
	
	private boolean isTemporary=false;
	
	//设置一个中间页数，用于保证可以往前翻页
	public static int centerPageOffset=40000;
	
	public ProgressViewInterface mProgressViewInterface;
	
	public PageViewAdapter(FragmentManager mgr,Context mContext,ProgressViewInterface mProgressViewInterface) {
		super(mgr);
		this.mContext=mContext;
		this.pageCount=0;
		this.mProgressViewInterface=mProgressViewInterface;
		
	}
	
	public void setIsReadFromTemporary(boolean isTemporary){
		this.isTemporary=isTemporary;
	}
	
	
	public void setPageCount(int pageCount){
		this.pageCount=pageCount;
	}

	@Override
	public Fragment getItem(int position) {
		Fragment fragment=BookPageFragment.newInstance(position-centerPageOffset,isTemporary,mContext,mProgressViewInterface);
		return fragment;
	}

	@Override
	public int getCount() {
		return pageCount+centerPageOffset*2;
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}
	
}


















