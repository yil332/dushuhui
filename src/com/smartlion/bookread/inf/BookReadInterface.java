package com.smartlion.bookread.inf;


import com.smartlion.bookread.customui.BookReadingPageView;
import com.smartlion.bookread.model.BookLabelInfos;

import android.graphics.RectF;
import android.view.MotionEvent;

/**
 * 接口
 * @author wenjiexue
 *
 */
public interface BookReadInterface {
	
	/**
	 * 触摸抬起之后触发
	 * @param event
	 */
	public void doAfterTouchUp(MotionEvent event,boolean isShowReadHub);
	
	/**
	 * 加载特定页数后触发
	 * @param nowTotalPage
	 */
	public void doAfterParserBookUnit(long nowTotalPage);
	
	/**
	 * 前次加载完毕触发
	 * @param totalPage
	 */
	public void doAfterParserEndBefore(long totalPage);
	
	/**
	 * 本次加载完毕触发
	 * @param totalPage
	 */
	public void doAfterParserEnd(long totalPage);
	
	/**
	 * 加载临时文件完毕后触发
	 * @param totalPage
	 */
	public void doAfterParserTemporaryEnd(long totalPage);
	
	/**
	 * 加载临时文件结束后触发跳转到具体页面
	 * @param totalPage
	 */
	public void doAfterParserBookTemporaryToIndex(long totalPage);
	
	public void doGetTotalChapter(int totalChapter);
	
	public void doAfterParserChapter(int totalParserChapter);
	
	/**
	 * 改变字体大小之后触发
	 */
	public void doChangeFontSizeIndex();
	
	/**
	 * 显示脚注
	 * @param content
	 * @param rect
	 */
	public void showFootNote(String content, RectF rect);
	
	
	/**
	 * 显示图片
	 * @param imgSrc
	 */
	public void showImage(String imgSrc);
	
	/**
	 * 显示弹出标记编辑按钮
	 * @param x
	 * @param y
	 * @param content
	 * @param startIndex
	 * @param labelContentID
	 * @param page
	 */
	public void showEditLabelPopWindows(int x,int y,String content,Integer startIndex,int labelContentID,BookReadingPageView page);

	/**
	 * 显示弹出是否确定标记按钮
	 * @param x
	 * @param y
	 * @param content
	 * @param startIndex
	 * @param labelContentID
	 * @param page
	 */
	public void showCheckIsLabelPopWindows(int x,int y,String content,BookLabelInfos mBookLabelInfos,long labelContentID,BookReadingPageView page);

	
	public void changeViewPageItem(int position);
	
	public void moveView(float x);
	
	
	public void setAdapter();
	
	public void unSetAdapter();
	
}
