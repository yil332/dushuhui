package com.smartlion.bookread.inf;

/**
 * 接口
 * @author wenjiexue
 *
 */
public interface FragmentInterface {
	/**
	 * 操作增加标签
	 * @param contentid
	 * @param sectionID
	 * @param markerStartIndex
	 */
	public void showBookMarkImage(int contentid,int sectionID,Integer markerStartIndex,String content);
	
	/**
	 * 显示删除标签图片
	 * @param isShow
	 */
	public void showBookMarkImage(boolean isShow);
	
	/**
	 * 显示增加标签图片
	 * @param isShow
	 */
	public void showImageViewMarkDisabled(boolean isShow);
	
	/**
	 * 加载完毕之后
	 */
	public void loadCompleted();
	
}
