package com.smartlion.bookread.inf;

import java.util.Date;

public class BookLibProxy implements BookLibInterface{

	BookLibInterface inf;
	
	public BookLibProxy(BookLibInterface inf){
		this.inf=inf;
	}

	@Override
	public String getBookSavePath() {
		return inf.getBookSavePath();
	}

	@Override
	public void doShare() {
		inf.doShare();
	}

	@Override
	public boolean doHasWifi() {
		return inf.doHasWifi();
	}

	@Override
	public void doSaveBookProgress(long chapterID,long textInfoIndex,long mBookId,Date date,int percentage) {
		inf.doSaveBookProgress(chapterID,textInfoIndex,mBookId,date,percentage);
	}
}
