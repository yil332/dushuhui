package com.smartlion.bookread.inf;

import android.graphics.RectF;

public interface ProgressViewInterface {
	
	public void setProgress(int progress);
	
	public int getProgress();
	
	public void setCurrenty(float currenty);
	
	public void setCircleRect(RectF circleRect);
	
	public void setBoundRect(RectF circleRect);
	
	public RectF getCircleRect();
	
	public RectF getBoundRect();
	
	public float getCircleWidth();
	
	public float getStartHeight();
	
	public float getEndHeight();
	
	public int getMax();
	
	public void toInvalidate();
	
	public void disShowWind();
	
	public void changeDirection(int direction);
}
