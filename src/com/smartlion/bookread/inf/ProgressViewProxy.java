package com.smartlion.bookread.inf;

import android.graphics.RectF;

public class ProgressViewProxy implements ProgressViewInterface{
	ProgressViewInterface inf;
	
	public ProgressViewProxy(ProgressViewInterface inf) {
		this.inf=inf;
	}

	@Override
	public void setProgress(int progress) {
		inf.setProgress(progress);
	}

	@Override
	public void setCurrenty(float currenty) {
		inf.setCurrenty(currenty);
	}

	@Override
	public void setCircleRect(RectF circleRect) {
		inf.setCircleRect(circleRect);
	}

	@Override
	public void setBoundRect(RectF circleRect) {
		inf.setBoundRect(circleRect);
	}

	@Override
	public RectF getCircleRect() {
		return inf.getCircleRect();
	}

	@Override
	public RectF getBoundRect() {
		return inf.getBoundRect();
	}

	@Override
	public float getCircleWidth() {
		// TODO Auto-generated method stub
		return inf.getCircleWidth();
	}

	@Override
	public float getStartHeight() {
		// TODO Auto-generated method stub
		return inf.getStartHeight();
	}

	@Override
	public float getEndHeight() {
		// TODO Auto-generated method stub
		return inf.getEndHeight();
	}

	@Override
	public int getMax() {
		// TODO Auto-generated method stub
		return inf.getMax();
	}

	@Override
	public void toInvalidate() {
		inf.toInvalidate();
	}

	@Override
	public int getProgress() {
		// TODO Auto-generated method stub
		return inf.getProgress();
	}

	@Override
	public void disShowWind() {
		// TODO Auto-generated method stub
		inf.disShowWind();
	}

	@Override
	public void changeDirection(int direction) {
		inf.changeDirection(direction);
	}

}
