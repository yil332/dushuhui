package com.smartlion.bookread.inf;

/**
 * 代理
 * @author wenjiexue
 *
 */
public class FragmentProxy implements FragmentInterface{
	
	FragmentInterface inf=null;
	
	public FragmentProxy(FragmentInterface inf) {
		this.inf=inf;
	}

	@Override
	public void showBookMarkImage(int contentid,int sectionID,Integer markerStartIndex,String content) {
		inf.showBookMarkImage(contentid,sectionID,markerStartIndex,content);
	}

	@Override
	public void showImageViewMarkDisabled(boolean isShow) {
		inf.showImageViewMarkDisabled(isShow);
	}

	@Override
	public void showBookMarkImage(boolean isShow) {
		inf.showBookMarkImage(isShow);
	}
	
	@Override
	public void loadCompleted() {
		inf.loadCompleted();
	}

}
