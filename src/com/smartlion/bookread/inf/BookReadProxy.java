package com.smartlion.bookread.inf;


import com.smartlion.bookread.customui.BookReadingPageView;
import com.smartlion.bookread.model.BookLabelInfos;

import android.graphics.RectF;
import android.view.MotionEvent;

public class BookReadProxy implements BookReadInterface{

	BookReadInterface inf=null;
	
	public BookReadProxy(BookReadInterface inf) {
		this.inf=inf;
	}
	
	@Override
	public void doAfterTouchUp(MotionEvent event,boolean isShowReadHub) {
		inf.doAfterTouchUp(event,isShowReadHub);
	}

	@Override
	public void doAfterParserBookUnit(long nowTotalPage) {
		inf.doAfterParserBookUnit(nowTotalPage);
	}

	@Override
	public void doAfterParserEnd(long totalPage) {
		inf.doAfterParserEnd(totalPage);
	}

	@Override
	public void doAfterParserTemporaryEnd(long totalPage) {
		inf.doAfterParserTemporaryEnd(totalPage);
	}

	@Override
	public void doAfterParserBookTemporaryToIndex(long totalPage) {
		inf.doAfterParserBookTemporaryToIndex(totalPage);
	}

	@Override
	public void doChangeFontSizeIndex() {
		inf.doChangeFontSizeIndex();
	}
	
	@Override
	public void showFootNote(String content, RectF rect){
		inf.showFootNote(content, rect);
	}

	

	@Override
	public void showImage(String imgSrc) {
		inf.showImage(imgSrc);
	}

	@Override
	public void showEditLabelPopWindows(int x,int y,String content,Integer startIndex,int labelContentID,BookReadingPageView page) {
		inf.showEditLabelPopWindows(x,y,content,startIndex,labelContentID,page);
	}

	@Override
	public void showCheckIsLabelPopWindows(int x, int y, String content,
			BookLabelInfos mBookLabelInfos, long labelContentID,
			BookReadingPageView page) {
		inf.showCheckIsLabelPopWindows(x, y, content, mBookLabelInfos, labelContentID, page);
	}

	@Override
	public void doAfterParserEndBefore(long totalPage) {
		inf.doAfterParserEndBefore(totalPage);
	}

	@Override
	public void doGetTotalChapter(int totalChapter) {
		inf.doGetTotalChapter(totalChapter);
	}

	@Override
	public void doAfterParserChapter(int totalParserChapter) {
		inf.doAfterParserChapter(totalParserChapter);
	}

	@Override
	public void changeViewPageItem(int position) {
		inf.changeViewPageItem(position);
	}

	@Override
	public void moveView(float x) {
		inf.moveView(x);
	}

	@Override
	public void setAdapter() {
		inf.setAdapter();
	}

	@Override
	public void unSetAdapter() {
		inf.unSetAdapter();
	}


}
