package com.smartlion.bookread.inf;

import java.util.Date;

public interface BookLibInterface {

	public String getBookSavePath();
	
	public void doShare();
	
	public boolean doHasWifi();
	
	public void doSaveBookProgress(long chapterID,long textInfoIndex,long mBookId,Date date,int percentage);
}
