package com.smartlion.bookread.model;

import com.google.gson.annotations.SerializedName;

/**
 * 书籍标注的格式
 * 
 * @author wenjiexue
 * 
 */
public class BookLabelInfo {
	/**
	 * 书籍标签的位置
	 */
	@SerializedName("labelStartIndex")
	public Integer labelStartIndex;
	/**
	 * 该位置的内容
	 */
	@SerializedName("labelContent")
	public String labelContent;
	
}
