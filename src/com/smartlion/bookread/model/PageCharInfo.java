package com.smartlion.bookread.model;

import com.google.gson.annotations.SerializedName;

/**
 * 每行显示的字的内容
 * @author wenjiexue
 *
 */
public class PageCharInfo {
	/**
	 * 显示的字的内容
	 */
	@SerializedName("content")
	public String content;
	/**
	 * 显示的左边位置
	 */
	@SerializedName("left")
	public float left;
	/**
	 * 显示的右边位置
	 */
	@SerializedName("right")
	public float right;
	/**
	 * 显示的上边位置
	 */
	@SerializedName("top")
	public float top;
	/**
	 * 显示的下边位置
	 */
	@SerializedName("bottom")
	public float bottom;
	/**
	 * 该行在本章节中的第几个段落
	 */
	@SerializedName("textInfoIndex")
	public long textInfoIndex;
	/**
	 * 该行的类型
	 */
	@SerializedName("kind")
	public String kind;
	/**
	 * 该行是否需要显示li标签
	 */
	@SerializedName("isShowLI")
	public boolean isShowLI=false;
	/**
	 * 显示的时候每一个字当中的间隔
	 */
	@SerializedName("fixWidth")
	public float fixWidth;
	/**
	 * 该行第一字在文章该章节中位置的偏移量，就是是该章节的第几个字
	 */
	@SerializedName("lineInCPIndex")
	public long lineInCPIndex;
}
