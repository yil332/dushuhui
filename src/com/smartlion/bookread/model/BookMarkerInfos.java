package com.smartlion.bookread.model;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

/**
 * 书签的格式的集合
 * @author wenjiexue
 *
 */
public class BookMarkerInfos {
	/**
	 * 书签的格式的集合
	 */
	@SerializedName("mBookMarkerInfosList")
	public ArrayList<BookMarkerInfo> mBookMarkerInfosList=new ArrayList<BookMarkerInfo>();
}
