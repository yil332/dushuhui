package com.smartlion.bookread.model;

import com.google.gson.annotations.SerializedName;

/**
 * 一个页面的数据格式
 * @author wenjiexue
 *
 */
public class PageShowInfo {
	/**
	 * 每行显示的字的内容的集合
	 */
	@SerializedName("mPageCharInfos")
	public PageCharInfos mPageCharInfos;

	/**
	 * 该页在该章节所在的页数
	 */
	@SerializedName("chapterPageIndex")
	public long chapterPageIndex;
	
	/**
	 * 该页面的字体统计
	 */
	@SerializedName("pageCharCount")
	public long pageCharCount;
	
	/**
	 * 该章节的字数统计－统计到当前位置
	 */
	@SerializedName("chapterCharCount")
	public long chapterCharCount;
	
	/**
	 * 文章的字数统计－统计到当前位置
	 */
	@SerializedName("totalCharCount")
	public long totalCharCount;
	
	/**
	 * 该页所属的章节id
	 */
	@SerializedName("chapterID")
	public long chapterID;
	
	/**
	 * 该页所属的章节标题
	 */
	@SerializedName("chapterTitle")
	public String chapterTitle;
	
}
