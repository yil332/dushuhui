package com.smartlion.bookread.model;

import com.google.gson.annotations.SerializedName;

/**
 * 书签的格式
 * @author wenjiexue
 *
 */
public class BookMarkerInfo {
	/**
	 * 该书签的第一个字在本文本章节中的第几个段落
	 */
	@SerializedName("sectionID")
	public int sectionID;
	/**
	 * 该书签的第一个字在本文本中的第几个章节
	 */
	@SerializedName("chapterID")
	public int chapterID;
	/**
	 * 该书签的第一个字是本文本章节中的第几个字
	 */
	@SerializedName("markerIndex")
	public int markerIndex;
	
	/**
	 * 书签粗略的内容
	 */
	@SerializedName("content")
	public String content;
}
