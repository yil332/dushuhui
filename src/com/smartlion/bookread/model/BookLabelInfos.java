package com.smartlion.bookread.model;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

/**
 * 书籍标签的类型集合
 * @author wenjiexue
 *
 */
public class BookLabelInfos {
	/**
	 * 书籍标签的类型集合
	 */
	@SerializedName("mBookLabelInfoList")
	public ArrayList<BookLabelInfo> mBookLabelInfoList = new ArrayList<BookLabelInfo>();
}
