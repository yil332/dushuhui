package com.smartlion.bookread.model;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

/**
 * 章节生成之后，该字段缓存标记该章节的缓存生成成功，并标记一些基础信息
 * @author wenjiexue
 *
 */
public class ContentCacheLoadedInfo {
	
	/**
	 * 该章节的总字数
	 */
	@SerializedName("contentcharsize")
	public long contentcharsize;

	/**
	 * 文章解析到此处的总字数
	 */
	@SerializedName("contenttotalsize")
	public long contenttotalsize;
	
	/**
	 * 在该章节中的页数
	 */
	@SerializedName("page")
	public long page;
	
	/**
	 * 文章解析到此处的总页数
	 */
	@SerializedName("totalPage")
	public long totalPage;
	
	/**
	 * 文章解析到此处的总页数
	 */
	@SerializedName("mBookPageCountInfoList")
	public ArrayList<BookPageCountInfo> mBookPageCountInfoList=new ArrayList<BookPageCountInfo>();
}
