package com.smartlion.bookread.model.parserzip;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

/**
 * 解析书籍原文件
 * @author wenjiexue
 *
 */
public class ManifestInfo {

	@SerializedName("title")
	public String title;
	@SerializedName("description")
	public String description;
	@SerializedName("authors")
	public String authors[];
	@SerializedName("presses")
	public String presses[];
	@SerializedName("isbn")
	public String isbn;
	@SerializedName("published")
	public String published;
	@SerializedName("category")
	public String category;
	@SerializedName("tags")
	public String tags;
	@SerializedName("content")
	public ArrayList<ContentInfo> mContentInfo=new ArrayList<ContentInfo>();
}
