package com.smartlion.bookread.model.parserzip;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

/**
 * 解析书籍原文件
 * @author wenjiexue
 *
 */
public class TargetInfo {
	
	@SerializedName("title")
	public String title;
	
	@SerializedName("content")
	public ArrayList<TargetContentInfo> mTargetContentInfo=new ArrayList<TargetContentInfo>();
}
