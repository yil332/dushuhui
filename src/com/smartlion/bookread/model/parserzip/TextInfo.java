package com.smartlion.bookread.model.parserzip;

import com.google.gson.annotations.SerializedName;

/**
 * 解析书籍原文件
 * @author wenjiexue
 *
 */
public class TextInfo {
	@SerializedName("content")
	public String content;
	@SerializedName("kind")
	public String kind;
}
