package com.smartlion.bookread.model.parserzip;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

/**
 * 解析书籍原文件
 * @author wenjiexue
 *
 */
public class ChildrenInfo {
	@SerializedName("target")
	public String target;
	@SerializedName("title")
	public String title;
	@SerializedName("id")
	public String id;
	@SerializedName("children")
	public ArrayList<ChildrenInfo> mChildrenInfo=new ArrayList<ChildrenInfo>();
}
