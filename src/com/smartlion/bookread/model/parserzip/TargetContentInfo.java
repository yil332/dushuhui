package com.smartlion.bookread.model.parserzip;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

/**
 * 解析书籍原文件
 * @author wenjiexue
 *
 */
public class TargetContentInfo {

	@SerializedName("type")
	public String type;
	
	@SerializedName("tag")
	public String tag;
	
	@SerializedName("text")
	public ArrayList<TextInfo> mTextInfo=new ArrayList<TextInfo>();
	
	@SerializedName("src")
	public String src;
	
	@SerializedName("height")
	public int height;
	
	@SerializedName("width")
	public int width;
	
	@SerializedName("parent_tag")
	public String parent_tag;
}
