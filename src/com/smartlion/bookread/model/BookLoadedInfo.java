package com.smartlion.bookread.model;

import com.google.gson.annotations.SerializedName;

/**
 * 该书已经加载完成的缓存，纪录了该书的总页数
 * @author wenjiexue
 *
 */
public class BookLoadedInfo {
	/**
	 * 纪录了该书的总页数
	 */
	@SerializedName("totalPage")
	public long totalPage;
}
