package com.smartlion.bookread.model;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

/**
 * 每行显示的字的内容的集合
 * @author wenjiexue
 *
 */
public class PageCharInfos {
	/**
	 * 每行显示的字的内容的集合
	 */
	@SerializedName("mPageCharInfo")
	public ArrayList<PageCharInfo> mPageCharInfoList=new ArrayList<PageCharInfo>();

}
