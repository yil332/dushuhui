package com.smartlion.bookread.model;

import com.google.gson.annotations.SerializedName;

public class BookPageCountInfo {
	/**
	 * 页数
	 */
	@SerializedName("pageIndex")
	public int pageIndex;
	/**
	 * 开始字数
	 */
	@SerializedName("startCharCounInChapter")
	public int startCharCounInChapter;
	/**
	 * 结束字数
	 */
	@SerializedName("endCharCountInChapter")
	public int endCharCountInChapter;
	
}
