package com.smartlion.bookread;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nineoldandroids.view.ViewHelper;
import com.smartlion.bookread.common.BookMarkerShowListInfo;
import com.smartlion.bookread.common.BookToCPagerAdapter;
import com.smartlion.bookread.common.BookmarkAdapter;
import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.common.MyViewPager;
import com.smartlion.bookread.common.NewBookTocAdapter;
import com.smartlion.bookread.common.PageViewAdapter;
import com.smartlion.bookread.common.ProgressView;
import com.smartlion.bookread.customui.BookReadingPageView;
import com.smartlion.bookread.customui.CheckIsLabelView;
import com.smartlion.bookread.customui.EditLabelView;
import com.smartlion.bookread.customui.FootRootView;
import com.smartlion.bookread.customui.NewTextSettings;
import com.smartlion.bookread.inf.BookLibInterface;
import com.smartlion.bookread.inf.BookLibProxy;
import com.smartlion.bookread.inf.BookReadInterface;
import com.smartlion.bookread.model.BookLabelInfos;
import com.smartlion.bookread.model.BookMarkerInfos;
import com.smartlion.bookread.model.ContentCacheLoadedInfo;
import com.smartlion.bookread.model.PageShowInfo;
import com.smartlion.bookread.model.parserzip.ManifestInfo;
import com.smartlion.bookread.parserutil.BookContentCacheUtil;
import com.smartlion.bookread.parserutil.BookInfoCacheUtil;
import com.smartlion.bookread.parserutil.BookMarkerCacheUtil;
import com.smartlion.bookread.parserutil.BookParserTemporaryThread;
import com.smartlion.bookread.parserutil.BookParserThread;
import com.smartlion.bookread.parserutil.BookParserUtil;
import com.smartlion.bookread.parserutil.LabelCacheUtil;
import com.whalefin.dushuhui.R;

public class BookReadActivity extends FragmentActivity implements
		BookReadInterface {
	private long mBookId;
	private String mBookTitle;
	private static int fontSizeIndex;
	public int totalPage = -1;
	public boolean isAllLoad = false;
	public BookParserThread mBookParserThread = null;
	public BookParserTemporaryThread mBookParserTemporaryThread = null;
	public BookContentCacheUtil mBookCacheUtil = null;
	public BookContentCacheUtil mBookTemporaryCacheUtil = null;
	public LabelCacheUtil mLabelCacheUtil;
	public BookMarkerCacheUtil mBookMarkerCacheUtil;
	public MyViewPager mViewPager;
	public RelativeLayout parentView = null, RelativeLayout01;
	public boolean isShowDlg = false;
	public boolean isShowReaderHub = false;
	public ProgressView mProgressView1;
	public View toCDlgView = null;
	public ProgressView mProgressView = null;
	public float showAllView = 0;
	public View readHubView = null;
	InitPageViewAdapterHandle initPageViewAdapterHandle = new InitPageViewAdapterHandle();
	// 标记当前是在阅读临时段落
	boolean isReadTemporary = false;
	ViewGroup mViewGroup;
	PageViewAdapter mPageViewAdapter = null;
	ListView mTocListView;
	ListView mBookmarkListview;
	View mTocView;
	float windowWidth, windowHeight;
	Button Button01;
	Button Button02;
	ViewPager pager;
	BookToCPagerAdapter mBookToCPagerAdapter;
	TimerTask task = new TimerTask() {
		public void run() {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			String str = sdf.format(new Date());
			mHandler.sendMessage(mHandler.obtainMessage(100, str));
		}
	};
	private float offSetToGone = -windowWidth;
	private float showViewPart = -windowWidth * 9 / 10;
	// 标记当前阅读的章节
	private long contentID;
	private BookParserUtil mBookParserUtil = null;
	private BookInfoCacheUtil mBookInfoCacheUtil = null;
	// 文章的开始章节
	private long startContentID;
	// 文章的结束章节
	private long endContentID;
	private int totalChapter = 0;
	private int totalParserChapter = 0;
	private ProgressDialog progressDialog = null;
	private NewBookTocAdapter mTocAdapter = null;
	private BookmarkAdapter mBookmarkAdapter = null;
	private NewTextSettings mTextSettings = null;
	private EditLabelView mEditLabelView;
	private CheckIsLabelView mCheckIsLabelView;
	private TextView textview_pagenum;
	private IntentFilter mIntentFilter;
	private ImageView battery_image;
	private TextView textview_time;
	private TextView textview_booktitle;
	
	private Config config;

	public static BookLibProxy mBookLibProxy;
	// 声明消息处理过程－－动态显示电池电量
	private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int status = intent.getIntExtra("status", 0);
			int level = intent.getIntExtra("level", 0);

			switch (status) {

			case BatteryManager.BATTERY_STATUS_CHARGING:
				battery_image
						.setImageResource(R.drawable.booklib_stat_sys_battery_charge);
				battery_image.getDrawable().setLevel(level);
				break;
			case BatteryManager.BATTERY_STATUS_DISCHARGING:
				battery_image.setImageResource(R.drawable.booklib_stat_sys_battery);
				battery_image.getDrawable().setLevel(level);
				break;

			}

		}
	};

	public static void setBookLibProxy(BookLibInterface inf) {
		mBookLibProxy = new BookLibProxy(inf);
	}

	private Handler mHandler = new Handler() {
		// 重写handleMessage()方法，此方法在UI线程运行
		@Override
		public void handleMessage(Message msg) {
			textview_time.setText((String) msg.obj);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		mBookId = getIntent().getLongExtra("bookid", -1);
		mBookTitle = getIntent().getStringExtra("booktitle");
		config=new Config(this);
		
//		String path = mBookLibProxy.getBookSavePath();
//		Config.getInstance().saveBookSavePath(path);

		windowWidth = getWindowManager().getDefaultDisplay().getWidth();

		windowHeight = getWindowManager().getDefaultDisplay().getHeight();

		fontSizeIndex = Config.getInstance().getBookScale();

		mEditLabelView = new EditLabelView(this);
		mCheckIsLabelView = new CheckIsLabelView(this);

		mBookParserThread = new BookParserThread(this, mBookId, fontSizeIndex,
				true);
		mBookCacheUtil = new BookContentCacheUtil(this, mBookId, fontSizeIndex);
		mBookParserUtil = new BookParserUtil(this, mBookId);
		mLabelCacheUtil = new LabelCacheUtil(this, mBookId);
		mBookMarkerCacheUtil = new BookMarkerCacheUtil(this, mBookId);
		mBookInfoCacheUtil = new BookInfoCacheUtil(this, mBookId, fontSizeIndex);

		try {
			startContentID = Long
					.valueOf(mBookParserUtil.getMaifestInfo().mContentInfo
							.get(0).id);
			endContentID = Long
					.valueOf(mBookParserUtil.getMaifestInfo().mContentInfo
							.get(mBookParserUtil.getMaifestInfo().mContentInfo
									.size() - 1).id);
		} catch (NumberFormatException e) {
		} catch (IOException e) {
			e.toString();
		}

		try {
			LayoutInflater inflater = LayoutInflater.from(this);
			mViewGroup = (ViewGroup) inflater.inflate(
					R.layout.booklib_read_view_pager, null);
		} catch (Exception e) {
			e.toString();
		}
		
		mViewPager = (MyViewPager) mViewGroup.findViewById(R.id.viewPager);

		parentView = (RelativeLayout) mViewGroup.findViewById(R.id.parentView);
		RelativeLayout01 = (RelativeLayout) mViewGroup
				.findViewById(R.id.RelativeLayout01);
		RelativeLayout01.bringToFront();
		try {
			initFunction();
		} catch (IOException e) {
		}

		mProgressView1 = new ProgressView(this, windowHeight / 10,
				windowHeight * 8 / 10, totalPage, parentView);

		parentView.addView(mProgressView1);
		mProgressView1.setVisibility(View.GONE);

		mPageViewAdapter = new PageViewAdapter(
				BookReadActivity.this.getSupportFragmentManager(),
				BookReadActivity.this, mProgressView1);
		mViewPager.setAdapter(mPageViewAdapter);
		setContentView(mViewGroup);

		mTextSettings = new NewTextSettings(this, mViewPager, this);

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				if (!isReadTemporary) {
					new RecordReadingProgressTask().execute(arg0);
					// saveBookReadProgress(arg0);
				}
				if (isAllLoad) {
					textview_pagenum.setText("");
				}
				if (!isReadTemporary) {
					if (arg0 - PageViewAdapter.centerPageOffset < 0) {
						mViewPager
								.setCurrentItem(PageViewAdapter.centerPageOffset);
					}
				}

				if (isAllLoad) {
					if (arg0 - PageViewAdapter.centerPageOffset > totalPage - 1) {
						mViewPager
								.setCurrentItem(PageViewAdapter.centerPageOffset
										+ totalPage - 1);
					}
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				Log.e("onPageScrollStateChanged" + arg0, "");
				if (arg0 == 2) {
					if (contentID != startContentID
							&& contentID != endContentID) {
						if (isReadTemporary) {
							if (mViewPager.getCurrentItem() == (PageViewAdapter.centerPageOffset - 1)) {
								contentID -= 1;
								startParserTemporary(contentID, true, -1);
							}
							if (mBookParserTemporaryThread != null) {
								if (mViewPager.getCurrentItem() == (PageViewAdapter.centerPageOffset + mBookParserTemporaryThread
										.getTotalPage())) {
									contentID += 1;
									startParserTemporary(contentID, false, -1);
								}
							}
						}
					}
				}

			}
		});

		textview_pagenum = (TextView) mViewGroup
				.findViewById(R.id.textview_pagenum);
		textview_pagenum.bringToFront();
		battery_image = (ImageView) mViewGroup.findViewById(R.id.battery_image);
		battery_image.bringToFront();
		textview_time = (TextView) mViewGroup.findViewById(R.id.textview_time);
		textview_time.bringToFront();
		mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
		textview_booktitle = (TextView) mViewGroup
				.findViewById(R.id.textview_booktitle);
		textview_booktitle.bringToFront();
		textview_booktitle.setText(mBookTitle);
		// 解析全书
		mBookParserThread.start();

		Timer timer = new Timer(true);
		timer.schedule(task, 0, 30000); // 延时1000ms后执行，1000ms执行一次

		parentView.bringToFront();
	}

	/**
	 * 显示章节目录或书签信息列表
	 * 
	 * @throws IOException
	 */
	private void initFunction() throws IOException {
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		readHubView = layoutInflater.inflate(R.layout.booklib_readhub_book, null);
		parentView.addView(readHubView);
		readHubView.setVisibility(View.GONE);
		ImageButton goHomeButton = (ImageButton) readHubView
				.findViewById(R.id.gohome);
		goHomeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				disShowReaderHub();
				finish();
			}
		});

		ImageButton bookMarkButton = (ImageButton) readHubView
				.findViewById(R.id.readhub_bookmark);
		bookMarkButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					PageShowInfo pg = null;
					if (isReadTemporary) {
						pg = mBookTemporaryCacheUtil
								.readCacheObjectByPageIndex(mViewPager
										.getCurrentItem()
										- PageViewAdapter.centerPageOffset);
					} else {
						pg = mBookCacheUtil
								.readCacheObjectByPageIndex(mViewPager
										.getCurrentItem()
										- PageViewAdapter.centerPageOffset);
					}
					String content = "";
					for (int i = 0; i < pg.mPageCharInfos.mPageCharInfoList
							.size(); i++) {
						content = content
								+ pg.mPageCharInfos.mPageCharInfoList.get(i).content;
					}
					mBookLibProxy.doShare();
				} catch (Exception e) {
				}
			}
		});

		ImageButton textSetButton = (ImageButton) readHubView
				.findViewById(R.id.textset);
		textSetButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isAllLoad) {
					mTextSettings.setBookProgressSeekBarProgress(
							mViewPager.getCurrentItem()
									- PageViewAdapter.centerPageOffset,
							(int) mBookParserThread.getTotalPage(), mViewPager);
					mTextSettings.enableTxtButton();
				} else {
					mTextSettings.disableTxtButton();
				}
				mTextSettings.enableTxtButton();
				mTextSettings.show();
			}
		});

		toCDlgView = layoutInflater.inflate(R.layout.booklib_tcview, null);

		mProgressView = new ProgressView(this, windowHeight / 10,
				windowHeight * 8 / 10, totalPage, parentView);

		offSetToGone = -windowWidth;
		showViewPart = -windowWidth + mProgressView.boundWidth - 4;

		RelativeLayout RelativeLayout01 = (RelativeLayout) toCDlgView
				.findViewById(R.id.RelativeLayout01);
		RelativeLayout01.addView(mProgressView);

		parentView.addView(toCDlgView);
		ViewHelper.setTranslationX(toCDlgView, offSetToGone);

		mTocView = toCDlgView.findViewById(R.id.book_toc);

		if (mTocListView == null) {
			mTocListView = new ListView(BookReadActivity.this);
			mTocListView.setSelector(R.color.transparent);
			setupTocListView();
		}
		if (mBookmarkListview == null) {
			mBookmarkListview = new ListView(BookReadActivity.this);
			setupTomBookmarkListview();
		}

		pager = (ViewPager) mTocView.findViewById(R.id.toc_pager);
		mBookToCPagerAdapter = new BookToCPagerAdapter(mTocListView,
				mBookmarkListview);
		// pager.setAdapter(mBookToCPagerAdapter);
		// pager.setAdapter(null);

		Button01 = (Button) mTocView.findViewById(R.id.Button01);
		Button02 = (Button) mTocView.findViewById(R.id.Button02);
		Button01.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pager.setCurrentItem(0);
				Button01.setBackgroundResource(R.drawable.booklib_left_bg_licked);
				Button02.setBackgroundResource(R.drawable.booklib_right_bg);
			}
		});
		Button02.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pager.setCurrentItem(1);
				Button01.setBackgroundResource(R.drawable.booklib_left_bg);
				Button02.setBackgroundResource(R.drawable.booklib_right_bg_chicked);
			}
		});

		pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				if (arg0 == 0) {
					Button01.setBackgroundResource(R.drawable.booklib_left_bg_licked);
					Button02.setBackgroundResource(R.drawable.booklib_right_bg);
				} else {
					Button01.setBackgroundResource(R.drawable.booklib_left_bg);
					Button02.setBackgroundResource(R.drawable.booklib_right_bg_chicked);
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		toCDlgView.bringToFront();

		if (mTocAdapter == null) {
			mTocAdapter = new NewBookTocAdapter(getApplicationContext(),
					mBookParserUtil.getMaifestInfo());
			mTocListView.setAdapter(mTocAdapter);
		} else {
			mTocAdapter.notifyDataSetChanged();
		}
	}

	@Override
	protected void onResume() {
		registerReceiver(mIntentReceiver, mIntentFilter);
		super.onResume();
	}

	@Override
	protected void onPause() {
		unregisterReceiver(mIntentReceiver);
		super.onPause();
	}

	/**
	 * 开始解析临时章节
	 * 
	 * @param contentid
	 * @param isToEndPage
	 * @param appointIndex
	 */
	private void startParserTemporary(long contentid, boolean isToEndPage,
			long appointIndex) {
		showProgressDialog();
		isAllLoad = false;
		isReadTemporary = true;
		Log.e("isReadTemporary" + isReadTemporary, "1");
		// 暂停全书解析
		if (mBookParserThread != null) {
			mBookParserThread.callStop();
		}
		mBookTemporaryCacheUtil = new BookContentCacheUtil(
				BookReadActivity.this, mBookId, fontSizeIndex);
		mBookTemporaryCacheUtil.setCacheForTemporary(contentid);

		if (appointIndex == -1) {
			mBookParserTemporaryThread = new BookParserTemporaryThread(
					BookReadActivity.this, mBookId, fontSizeIndex, contentID);
			if (isToEndPage) {
				mBookParserTemporaryThread.setToEndPage(isToEndPage);
			}
		} else {
			mBookParserTemporaryThread = new BookParserTemporaryThread(
					BookReadActivity.this, mBookId, fontSizeIndex, contentID,
					appointIndex);
		}

		mBookParserTemporaryThread.start();
	}

	public void reParserAllBook() {
		if (mBookParserThread != null) {
			mBookParserThread.callStop();
		}
		mBookParserThread = new BookParserThread(BookReadActivity.this,
				mBookId, fontSizeIndex, false);
		mBookCacheUtil = new BookContentCacheUtil(this, mBookId, fontSizeIndex);
		mBookInfoCacheUtil = new BookInfoCacheUtil(this, mBookId, fontSizeIndex);
		mBookParserThread.start();
	}

	@Override
	public void finish() {
		if (mBookParserThread != null) {
			mBookParserThread.callStop();
		}
		super.finish();
	}

	// 解析到部分位置的时候 告诉ui可以开始看书了
	@Override
	public void doAfterParserBookUnit(long nowTotalPage) {
		Message msg = new Message();
		msg.what = 0;
		initPageViewAdapterHandle.sendMessage(msg);
	}

	// 整本书解析完毕之后，告诉ui整本书解析好了
	@Override
	public void doAfterParserEnd(long totalPage) {
		isAllLoad = true;
		this.totalPage = (int) totalPage;
		// 如果此时读的是临时缓存，就重新加载下
		if (isReadTemporary) {

			Message msg = new Message();
			msg.what = 1;
			initPageViewAdapterHandle.sendMessage(msg);
		}
	}

	@Override
	public void doAfterParserEndBefore(long totalPage) {
		isAllLoad = true;
		this.totalPage = (int) totalPage;
		Message msg = new Message();
		msg.what = 2;
		initPageViewAdapterHandle.sendMessage(msg);
	}

	// 临时章节解析好了，告诉ui可以开始看这章节了
	@Override
	public void doAfterParserTemporaryEnd(long totalPage) {
		Message msg = new Message();
		msg.what = 3;
		initPageViewAdapterHandle.sendMessage(msg);
	}

	@Override
	public void doAfterParserBookTemporaryToIndex(long totalPage) {
		Message msg = new Message();
		msg.what = 4;
		msg.arg1 = (int) totalPage;
		initPageViewAdapterHandle.sendMessage(msg);
	}

	@Override
	public void doGetTotalChapter(int totalChapter) {
		this.totalChapter = totalChapter;
	}

	@Override
	public void doAfterParserChapter(int totalParserChapter) {
		this.totalParserChapter = totalParserChapter;
		Message msg = new Message();
		msg.what = 5;
		initPageViewAdapterHandle.sendMessage(msg);
	}

	@Override
	public void doChangeFontSizeIndex() {
		if (fontSizeIndex != Config.getInstance().getBookScale()) {
			try {

				if (isReadTemporary) {
					mBookTemporaryCacheUtil = new BookContentCacheUtil(this,
							mBookId, fontSizeIndex);
					mBookTemporaryCacheUtil.setCacheForTemporary(contentID);
					fontSizeIndex = Config.getInstance().getBookScale();
					PageShowInfo mPageShowInfo = mBookTemporaryCacheUtil
							.readCacheObjectByPageIndex(mViewPager
									.getCurrentItem()
									- PageViewAdapter.centerPageOffset);
					startParserTemporary(contentID, false,
							mPageShowInfo.chapterCharCount
									- mPageShowInfo.pageCharCount);
				} else {
					mBookCacheUtil = new BookContentCacheUtil(this, mBookId,
							fontSizeIndex);
					fontSizeIndex = Config.getInstance().getBookScale();
					PageShowInfo mPageShowInfo = mBookCacheUtil
							.readCacheObjectByPageIndex(mViewPager
									.getCurrentItem()
									- PageViewAdapter.centerPageOffset);
					contentID = mPageShowInfo.chapterID;

					startParserTemporary(contentID, false,
							mPageShowInfo.chapterCharCount
									- mPageShowInfo.pageCharCount);
				}

			} catch (IOException e) {
			}

		}

	}

	@Override
	public void showEditLabelPopWindows(int x, int y, String content,
			Integer startIndex, int labelContentID, BookReadingPageView page) {
		mEditLabelView.Show(x, y, content, startIndex, labelContentID, page,
				mLabelCacheUtil, parentView);
	}

	@Override
	public void showFootNote(String content, RectF rect) {
		FootRootView mFootRootView = new FootRootView(this);
		mFootRootView
				.Show((int) rect.left, (int) rect.top, content, parentView);

	}

	@Override
	public void showImage(String imgSrc) {
		Intent intent = new Intent(this, ImageActivity.class);
		intent.putExtra("src", imgSrc);
		startActivity(intent);
	}

	@Override
	public void showCheckIsLabelPopWindows(int x, int y, String content,
			BookLabelInfos mBookLabelInfos, long labelContentID,
			BookReadingPageView page) {
		mCheckIsLabelView.Show(x, y, content, mBookLabelInfos, labelContentID,
				page, mLabelCacheUtil, parentView);
	}

	@Override
	public void doAfterTouchUp(MotionEvent e, boolean isShowReadHub) {
		float x = e.getRawX();

		if (x < windowWidth / 3) {
			if (mViewPager.getCurrentItem() - PageViewAdapter.centerPageOffset != 0) {
				mViewPager
						.setCurrentItem(mViewPager.getCurrentItem() - 1, true);
			}
			if (isShowReaderHub) {
				disShowReaderHub();
			}

		} else if (x > windowWidth * 2 / 3) {
			if (isAllLoad) {
				if (mViewPager.getCurrentItem()
						- PageViewAdapter.centerPageOffset != totalPage - 1) {
					mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1,
							true);
				}
			} else {
				mViewPager
						.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
			}
			if (isShowReaderHub) {
				disShowReaderHub();
			}
		} else {
			if (!isShowReaderHub) {
				showReaderHub();
			} else {
				disShowReaderHub();
			}

		}
	}

	@Override
	public void changeViewPageItem(int position) {
		mViewPager.setCurrentItem(position, false);
	}

	/**
	 * 保存读取进度
	 */
	private void saveBookReadProgress(int position) {
		if (!isReadTemporary) {
			Config.getInstance().setBookProgress((int) mBookId, fontSizeIndex,
					position - PageViewAdapter.centerPageOffset);
		}
	}

	/**
	 * 加载读书进度
	 * 
	 * @return
	 */
	private int loadBookReadProgress() {
		return Config.getInstance().getBookProgress((int) mBookId,
				fontSizeIndex);
	}

	/**
	 * 书籍章节点击跳转
	 */
	private void setupTocListView() {
		mTocListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// 跳章节阅读
				try {
					contentID = mTocAdapter.getItemId(position);
					// 该章节已经解析
					int startPageID = mBookInfoCacheUtil
							.getTargetStartPageID(mTocAdapter
									.getItemId(position));
					if (startPageID != -1) {
						// 当前阅读的是临时文件
						if (isReadTemporary) {
							isReadTemporary = false;
							mPageViewAdapter.setIsReadFromTemporary(false);
							mPageViewAdapter.notifyDataSetChanged();
							mViewPager.setCurrentItem(startPageID
									+ PageViewAdapter.centerPageOffset, false);
						} else
							mViewPager.setCurrentItem(startPageID
									+ PageViewAdapter.centerPageOffset, false);
					}
					// 该章节没有解析
					else {
						// 解析临时章节
						startParserTemporary(contentID, false, -1);
					}
				} catch (IOException e) {
				}
				disShowReaderHub();
				pager.setAdapter(null);
				mProgressView1.reSetBoundRect();
				// ViewHelper.setTranslationX(toCDlgView, offSetToGone);
				// mTocView.startAnimation(AnimationUtils.loadAnimation(
				// getBaseContext(), R.anim.out_to_right));
				// mTocView.setVisibility(View.GONE);
				isShowDlg = false;
			}
		});

	}

	/*************** 以下是菜单功能键功能 ************************************/

	/**
	 * 书籍章节点击跳转
	 */
	private void setupTomBookmarkListview() {

		mBookmarkListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				mViewPager.setCurrentItem(
						(int) (mBookmarkAdapter.getItemId(arg2) + PageViewAdapter.centerPageOffset),
						false);
				disShowReaderHub();
				pager.setAdapter(null);
				mProgressView1.reSetBoundRect();
				isShowDlg = false;
			}
		});
	}

	public void showReaderHub() {
		isShowReaderHub = true;
		readHubView.setVisibility(View.VISIBLE);
		ViewHelper.setTranslationX(toCDlgView, showViewPart);
		mProgressView.setProgerss(mViewPager.getCurrentItem()
				- PageViewAdapter.centerPageOffset, totalPage, isAllLoad);
		mProgressView1.setVisibility(View.VISIBLE);
		mProgressView1.reSetBoundRect();
		mProgressView1.setProgerss(mViewPager.getCurrentItem()
				- PageViewAdapter.centerPageOffset, totalPage, isAllLoad);
		Button01.setBackgroundResource(R.drawable.booklib_left_bg_licked);
		Button02.setBackgroundResource(R.drawable.booklib_right_bg);
	}

	public void disShowReaderHub() {
		isShowReaderHub = false;
		isShowDlg = false;
		mProgressView1.reSetBoundRect();
		mProgressView1.setVisibility(View.GONE);
		readHubView.setVisibility(View.GONE);
		ViewHelper.setTranslationX(toCDlgView, offSetToGone);
	}

	public void showProgressDialog() {
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("加载章节中"); // 标题
		progressDialog.setMessage("正在加载,请稍等"); // 正在加载
		progressDialog.setCancelable(false);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.show();
	}

	public void disShowProgressDialog() {
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			if (isShowDlg) {
				isShowDlg = false;
				moveView(0);
				return false;
			}
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

			if (isShowDlg) {
				isShowDlg = false;
				moveView(0);
				return false;
			} else if (!isShowReaderHub) {
				showReaderHub();
			} else {
				disShowReaderHub();
			}
		}
		return super.onKeyDown(keyCode, event);

	}

	@Override
	public void moveView(float x) {
		ViewHelper.setTranslationX(toCDlgView, showViewPart + x);
	}

	@Override
	public void setAdapter() {
		try {
			if (!isReadTemporary) {
				PageShowInfo pg = mBookCacheUtil
						.readCacheObjectByPageIndex(mViewPager.getCurrentItem()
								- PageViewAdapter.centerPageOffset);
				mTocAdapter.currentSection = pg.chapterID;
			} else {
				mTocAdapter.currentSection = contentID;
			}

		} catch (Exception e) {

		}

		pager.setAdapter(mBookToCPagerAdapter);
		new AdatperBookMarkTask().execute();
	}

	@Override
	public void unSetAdapter() {
		pager.setAdapter(null);
	}

	@SuppressLint("HandlerLeak")
	public class InitPageViewAdapterHandle extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				mPageViewAdapter.setIsReadFromTemporary(false);
				mPageViewAdapter.notifyDataSetChanged();
				mViewPager.setCurrentItem(loadBookReadProgress()
						+ PageViewAdapter.centerPageOffset, false);
				break;
			case 1:
				try {
					mPageViewAdapter.setIsReadFromTemporary(false);
					mPageViewAdapter.notifyDataSetChanged();
					mViewPager.setCurrentItem(
							mBookInfoCacheUtil.getTargetStartPageID(contentID)
									+ mViewPager.getCurrentItem(), false);
					isReadTemporary = false;
				} catch (IOException e) {
				}
				break;
			case 2:
				mPageViewAdapter.setIsReadFromTemporary(false);
				mPageViewAdapter.notifyDataSetChanged();
				mViewPager.setCurrentItem(loadBookReadProgress()
						+ PageViewAdapter.centerPageOffset, false);
				isReadTemporary = false;
				break;
			case 3:
				mPageViewAdapter.setIsReadFromTemporary(true);
				mPageViewAdapter.notifyDataSetChanged();
				mViewPager.setCurrentItem(PageViewAdapter.centerPageOffset,
						false);
				disShowProgressDialog();
				reParserAllBook();
				break;
			case 4:
				mPageViewAdapter.setIsReadFromTemporary(true);
				mPageViewAdapter.notifyDataSetChanged();
				mViewPager.setCurrentItem(PageViewAdapter.centerPageOffset
						+ msg.arg1, false);
				disShowProgressDialog();
				reParserAllBook();
				break;
			case 5:
				if (totalChapter == totalParserChapter) {
					textview_pagenum.setText("分页完成");
				} else {
					if (totalChapter != 0) {
						int per = (int) ((totalParserChapter * 1.0f)
								/ (totalChapter * 1.0f) * 100);
						textview_pagenum.setText("正在分页" + per + "%");
					} else {
						textview_pagenum.setText("正在分页");
					}
				}

				break;
			case 6:
				mBookmarkListview.setAdapter(mBookmarkAdapter);
				mBookmarkAdapter.notifyDataSetChanged();
				break;

			}

		}
	}

	public class AdatperBookMarkTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			if (isAllLoad) {
				ArrayList<BookMarkerShowListInfo> mBookMarkerShowListInfoList = new ArrayList<BookMarkerShowListInfo>();
				try {
					ManifestInfo mMaifestInfo = mBookParserUtil
							.getMaifestInfo();
					Gson gson = new Gson();
					for (int i = 0; i < mMaifestInfo.mContentInfo.size(); i++) {
						String json = mBookCacheUtil
								.readCacheByTargetContentID(Long
										.valueOf(mMaifestInfo.mContentInfo
												.get(i).id));
						ContentCacheLoadedInfo mContentCacheLoadedInfo = gson
								.fromJson(json, ContentCacheLoadedInfo.class);
						BookMarkerInfos mBookMarkerInfos = mBookMarkerCacheUtil
								.loadMarkerList(Long
										.valueOf(mMaifestInfo.mContentInfo
												.get(i).id));
						for (int j = 0; j < mContentCacheLoadedInfo.mBookPageCountInfoList
								.size(); j++) {
							for (int j2 = 0; j2 < mBookMarkerInfos.mBookMarkerInfosList
									.size(); j2++) {
								if ((mContentCacheLoadedInfo.mBookPageCountInfoList
										.get(j).startCharCounInChapter <= mBookMarkerInfos.mBookMarkerInfosList
										.get(j2).markerIndex)
										&& (mContentCacheLoadedInfo.mBookPageCountInfoList
												.get(j).endCharCountInChapter > mBookMarkerInfos.mBookMarkerInfosList
												.get(j2).markerIndex)) {
									BookMarkerShowListInfo mBookMarkerShowListInfo = new BookMarkerShowListInfo();
									mBookMarkerShowListInfo.pageIndex = mContentCacheLoadedInfo.mBookPageCountInfoList
											.get(j).pageIndex;
									mBookMarkerShowListInfo.content = mBookMarkerInfos.mBookMarkerInfosList
											.get(j2).content;
									mBookMarkerShowListInfoList
											.add(mBookMarkerShowListInfo);

								}
							}
						}
					}
				} catch (IOException e) {
				}

				mBookmarkAdapter = new BookmarkAdapter(getApplicationContext(),
						mBookMarkerShowListInfoList);
				Message msg = new Message();
				msg.what = 6;
				initPageViewAdapterHandle.sendMessage(msg);
			}
			return null;
		}

	}

	private class RecordReadingProgressTask extends
			AsyncTask<Integer, Void, Void> {

		@Override
		protected Void doInBackground(Integer... params) {

			try {
				int position = params[0];
				saveBookReadProgress(position);
				int percentage = 1;
				if (totalPage != 0) {
					percentage = (position - PageViewAdapter.centerPageOffset)
							/ totalPage;
				}
				if (mBookLibProxy.doHasWifi()) {
					if (isAllLoad) {

						PageShowInfo pg = mBookCacheUtil
								.readCacheObjectByPageIndex(position
										- PageViewAdapter.centerPageOffset);
						if (pg != null
								&& pg.mPageCharInfos.mPageCharInfoList.size() > 0) {
							mBookLibProxy
									.doSaveBookProgress(pg.chapterID,
											pg.mPageCharInfos.mPageCharInfoList
													.get(0).textInfoIndex,
											mBookId, new Date(), percentage);
						}

					}

				}
			} catch (Exception e) {
				// TODO: handle exception
			}

			return null;
		}
	}

}
