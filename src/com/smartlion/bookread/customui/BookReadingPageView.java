package com.smartlion.bookread.customui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.google.gson.JsonSyntaxException;
import com.smartlion.bookread.BookReadActivity;
import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.common.PaintUtil;
import com.smartlion.bookread.inf.FragmentInterface;
import com.smartlion.bookread.inf.FragmentProxy;
import com.smartlion.bookread.model.BookLabelInfos;
import com.smartlion.bookread.model.PageShowInfo;

@SuppressLint("ViewConstructor")
public class BookReadingPageView extends View {

	private Context mContext;

	private int mPageWidth;
	private int mPageHeight;

	public PageShowInfo pg = null;

	int pageNo;

	boolean isLoad = false;

	Bitmap mBitmap;

	ViewPagerChangeHandler handle = new ViewPagerChangeHandler();

	public List<ButtonRect> rects = new ArrayList<ButtonRect>();

	public List<ButtonRect> charRects = new ArrayList<ButtonRect>();

	public List<ButtonRect> pathRects = new ArrayList<ButtonRect>();

	public boolean isDrawPath = false;

	FragmentProxy mFragmentProxy;

	public boolean isNowDrawIng = false;
	
	private LoadPageTask mLoadPageTask=new LoadPageTask();

	public ButtonRect InRects(float f, float g) {
		for (ButtonRect br : rects) {
			if (br.getRect().contains(f, g)) {
				return br;
			}
		}
		return null;
	}

	public ButtonRect InCharRects(float f, float g) {
		for (ButtonRect br : charRects) {
			if (br.getRect().contains(f, g)) {
				return br;
			}
		}
		return null;
	}
	
	@Override
	protected void onDetachedFromWindow() {
		if (mLoadPageTask!=null) {
			mLoadPageTask.cancel(true);
			mLoadPageTask=null;
		}
		if (mBitmap!=null) {
			mBitmap.recycle();
			mBitmap=null;
		}
		super.onDetachedFromWindow();
	}

	public ButtonRect InPathRects(float f, float g) {
		for (ButtonRect br : pathRects) {
			if (br.getRect().contains(f, g)) {
				return br;
			}
		}
		return null;
	}

	public BookReadingPageView(Context context, PageShowInfo pg,
			boolean isGetTemporary, FragmentInterface inf) {
		super(context);
		mContext = context;
		this.pg = pg;
		mPageWidth = (int) Config.getInstance().getWindowWidth();
		mPageHeight = (int) Config.getInstance().getWindowHeight();
		mFragmentProxy = new FragmentProxy(inf);
	}

	public void paintByPageNo(int pageNo) throws JsonSyntaxException,
			IOException {
		this.pageNo = pageNo;
		if (!isNowDrawIng) {
			isNowDrawIng = true;
			mLoadPageTask.execute(pageNo);
		}

	}

	@SuppressLint("HandlerLeak")
	public class ViewPagerChangeHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			
			invalidate();
			mFragmentProxy.loadCompleted();
		}
	}

	private Bitmap toPaint(int pageNumber) throws JsonSyntaxException,
			IOException {
		Bitmap mBitmap1 = null;
		try {
			charRects.clear();
			pathRects.clear();

			mBitmap1 = Bitmap.createBitmap(mPageWidth, mPageHeight,
					android.graphics.Bitmap.Config.RGB_565);
			Canvas mCanvas;
			mCanvas = new Canvas(mBitmap1);

			mCanvas.drawPaint(PaintUtil.getPaint_NORMAL(mContext));

			if (pg.mPageCharInfos != null) {

				for (int j = 0; j < pg.mPageCharInfos.mPageCharInfoList.size(); j++) {
					if (pg.mPageCharInfos.mPageCharInfoList.get(j).kind
							.equals("img")) {
						Bitmap bmp = BitmapFactory
								.decodeFile(pg.mPageCharInfos.mPageCharInfoList
										.get(j).content);
						mCanvas.drawBitmap(
								bmp,
								pg.mPageCharInfos.mPageCharInfoList.get(j).left,
								pg.mPageCharInfos.mPageCharInfoList.get(j).top,
								null);
						if (!pg.mPageCharInfos.mPageCharInfoList.get(j).content
								.contains("cover.jpg")) {
							rects.add(new ButtonRect(
									new RectF(
											pg.mPageCharInfos.mPageCharInfoList
													.get(j).left,
											pg.mPageCharInfos.mPageCharInfoList
													.get(j).top,
											pg.mPageCharInfos.mPageCharInfoList
													.get(j).left
													+ bmp.getWidth(),
											pg.mPageCharInfos.mPageCharInfoList
													.get(j).top
													+ bmp.getHeight()),
									pg.mPageCharInfos.mPageCharInfoList.get(j).content,
									ButtonRect.TYPE_IMAGE));
						}

					} else if (pg.mPageCharInfos.mPageCharInfoList.get(j).kind
							.equals("footnote")) {

						Paint paint = PaintUtil
								.getPaint_FOOTNOTE_CIRCLE(mContext);

						mCanvas.drawCircle(
								pg.mPageCharInfos.mPageCharInfoList.get(j).left
										+ paint.getTextSize() / 2,
								pg.mPageCharInfos.mPageCharInfoList.get(j).top
										- paint.getTextSize() / 2,
								paint.getTextSize() * 3 / 8, paint);

						mCanvas.drawText(
								"注",
								pg.mPageCharInfos.mPageCharInfoList.get(j).left
										+ PaintUtil.dp2pixel(mContext, 2),
								pg.mPageCharInfos.mPageCharInfoList.get(j).top
										- PaintUtil.dp2pixel(mContext, 4), PaintUtil
										.getPaint_FOOTNOTE_CHAR(mContext));

						rects.add(new ButtonRect(
								new RectF(pg.mPageCharInfos.mPageCharInfoList
										.get(j).left - 20,
										pg.mPageCharInfos.mPageCharInfoList
												.get(j).top-20,
										pg.mPageCharInfos.mPageCharInfoList
												.get(j).right + 20,
										pg.mPageCharInfos.mPageCharInfoList
												.get(j).bottom+10),
								pg.mPageCharInfos.mPageCharInfoList.get(j).content,
								ButtonRect.TYPE_FOOTNOTE));

					}

					else {
						if (pg.mPageCharInfos.mPageCharInfoList.get(j).isShowLI) {
							Paint paint = PaintUtil
									.getPaint_LIST_CIRCLE(mContext);

							mCanvas.drawCircle(
									pg.mPageCharInfos.mPageCharInfoList.get(j).left
											- paint.getTextSize() / 2,
									pg.mPageCharInfos.mPageCharInfoList.get(j).top
											- paint.getTextSize() / 4,
									paint.getTextSize() * 3 / 16, paint);
						}
						float left = pg.mPageCharInfos.mPageCharInfoList.get(j).left;
						Paint paint = PaintUtil
								.getPaintByType(mContext,
										pg.mPageCharInfos.mPageCharInfoList
												.get(j).kind);
						for (int i = 0; i < pg.mPageCharInfos.mPageCharInfoList
								.get(j).content.length(); i++) {
							String str = pg.mPageCharInfos.mPageCharInfoList
									.get(j).content.substring(i, i + 1);
							float charWidth = paint.measureText(str)
									+ pg.mPageCharInfos.mPageCharInfoList
											.get(j).fixWidth;
							float sectionLine = PaintUtil.getLineHeightByType(
									mContext, "p")
									+ PaintUtil
											.getBOOK_PAGE_PADDING_CONTENT(mContext);
							if (pg.mPageCharInfos.mPageCharInfoList
									.get(j).kind.equals("p")) {
								charRects.add(new ButtonRect(
										new RectF(left,
												pg.mPageCharInfos.mPageCharInfoList
														.get(j).top - sectionLine,
												left + charWidth,
												pg.mPageCharInfos.mPageCharInfoList
														.get(j).top
														+ paint.getTextSize()),
										str, ButtonRect.TYPE_CHAR,
										(int) pg.mPageCharInfos.mPageCharInfoList
												.get(j).lineInCPIndex + i, paint
												.getTextSize() / 2));
							}
							
							mCanvas.drawText(
									str,
									left,
									pg.mPageCharInfos.mPageCharInfoList.get(j).top,
									paint);
							left = left + charWidth;
						}

					}
				}

				int startIndex = -1;

				BookLabelInfos mBookLabelInfos = null;
				try {
					mBookLabelInfos = ((BookReadActivity) mContext).mLabelCacheUtil
							.loadLabelList(pg.chapterID);
				} catch (IOException e) {
				}
				if (mBookLabelInfos != null) {

					Integer savePreIndex = -1;
					for (int i = 0; i < mBookLabelInfos.mBookLabelInfoList
							.size(); i++) {
						for (int j = 0; j < charRects.size(); j++) {
							if (charRects.get(j).charIndex
									.equals(mBookLabelInfos.mBookLabelInfoList
											.get(i).labelStartIndex)) {
								if (startIndex == -1) {
									startIndex = charRects.get(j).charIndex;
								}

								if (savePreIndex
										.equals(charRects.get(j).charIndex - 1)) {
									savePreIndex = charRects.get(j).charIndex;
								} else {
									savePreIndex = charRects.get(j).charIndex;
									startIndex = charRects.get(j).charIndex;
								}

								ButtonRect rect = charRects.get(j);
								rect.startIndex = startIndex;
								pathRects.add(rect);
								Path path = new Path();
								path.moveTo(charRects.get(j).getRect().left,
										charRects.get(j).getRect().bottom
												- charRects.get(j).fontSize);
								path.lineTo(charRects.get(j).getRect().right,
										charRects.get(j).getRect().bottom
												- charRects.get(j).fontSize);
								path.close();
								mCanvas.drawPath(path,
										PaintUtil.getPaint_PATH(mContext));
							}
						}
					}

				}

				Paint pageTitlePaint = PaintUtil.getPaintByType(mContext,
						"pagenumber");
				String title = pg.chapterTitle;
				mCanvas.drawText(
						title,
						PaintUtil.getBOOK_PAGE_PADDING_X(mContext),
						pageTitlePaint.getTextSize()
								+ PaintUtil.dp2pixel(mContext, 10),
						pageTitlePaint);

				if (((BookReadActivity) mContext).isAllLoad) {
					Paint pageNumberPaint = PaintUtil.getPaintByType(mContext,
							"pagenumber");
					String pageNumberStr = String.valueOf(pageNumber + 1) + "/"
							+ ((BookReadActivity) mContext).totalPage;
					mCanvas.drawText(
							pageNumberStr,
							mPageWidth - pageNumberPaint.getTextSize()
									* (pageNumberStr.length() - 1),
							pageTitlePaint.getTextSize()
									+ PaintUtil.dp2pixel(mContext, 10),
							pageNumberPaint);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return mBitmap1;
	}

	public Integer pageNumber;

	private class LoadPageTask extends AsyncTask<Integer, Void, Void> {

		@Override
		protected Void doInBackground(Integer... pageNo) {
			try {

				pageNumber = pageNo[0];
				mBitmap = toPaint(pageNo[0]);

				Message msg = new Message();

				handle.sendMessage(msg);
				isNowDrawIng = false;
			} catch (JsonSyntaxException e) {
			} catch (IOException e) {
			}
			isLoad = false;
			return null;
		}
	}

	@SuppressLint("DrawAllocation")
	protected void onDraw(Canvas canvas) {
		if (mBitmap != null)
			canvas.drawBitmap(mBitmap, 0.0F, 0.0F,
					PaintUtil.getPaint_NORMAL(mContext));

		if (isDrawPath) {
			isDrawPath = false;
			for (int i = 0; i < charRects.size(); i++) {
				if (charRects.get(i).isSelected) {
					Path path = new Path();
					path.moveTo(
							charRects.get(i).getRect().left,
							charRects.get(i).getRect().bottom
									- charRects.get(i).fontSize);
					path.lineTo(charRects.get(i).getRect().right, charRects
							.get(i).getRect().bottom
							- charRects.get(i).fontSize);
					path.close();
					canvas.drawPath(path, PaintUtil.getPaint_PATH(mContext));
				}
			}
		}
	}

	public void toDrawPath() {
		try {
			mBitmap = toPaint(pageNumber);
		} catch (JsonSyntaxException e) {
		} catch (IOException e) {
		}
		invalidate();
	}

	public void toDraw() {
		isDrawPath = true;
		invalidate();
	}

}
