package com.smartlion.bookread.customui;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;

import com.smartlion.bookread.BookReadActivity;
import com.smartlion.bookread.common.PaintUtil;
import com.smartlion.bookread.parserutil.LabelCacheUtil;
import com.whalefin.dushuhui.R;


/**
 * 显示标记编辑框
 * @author wenjiexue
 *
 */
public class EditLabelView {
	private Context ctx;
	
	private String str;
	private Integer index;
	private int deleteLabelContentid;
	private BookReadingPageView labelPage;
	private LabelCacheUtil mDeleteLabelCacheUtil;
		
	private PopupWindow popupWindow = null;
	
	public EditLabelView(Context ctx) {
		this.ctx=ctx;
	}
	
	@SuppressWarnings("deprecation")
	public void Show(int x, int y, String content,
			Integer startIndex, int labelContentID, BookReadingPageView page,LabelCacheUtil mLabelCacheUtil,View parentView){
		str = content;
		index = startIndex;
		deleteLabelContentid = labelContentID;
		labelPage = page;
		mDeleteLabelCacheUtil=mLabelCacheUtil;
		if (popupWindow == null) {
			LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = layoutInflater
					.inflate(R.layout.booklib_editlabel, null);
			Button Button01 = (Button) view.findViewById(R.id.Button01);
			Button Button02 = (Button) view.findViewById(R.id.Button02);
			Button Button03 = (Button) view.findViewById(R.id.Button03);
			Button01.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						mDeleteLabelCacheUtil.deleteLabelListByStartIndex(
								deleteLabelContentid, index, str.length());
					} catch (IOException e) {
					}
					labelPage.toDrawPath();
					popupWindow.dismiss();
				}
			});

			Button02.setOnClickListener(new OnClickListener() {

				@SuppressLint("NewApi")
				@Override
				public void onClick(View v) {
					ClipboardManager clip = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
					clip.setText(str); // 复制
					popupWindow.dismiss();
				}
			});

			Button03.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					BookReadActivity.mBookLibProxy.doShare();
//                    StartUtil.startAutoMakeScratch((Activity) ctx,
//                            mBook.coverUrl, mBook.title, str, "book", mBook.id);
					popupWindow.dismiss();
				}
			});
			popupWindow = new PopupWindow(view, PaintUtil.dp2pixel(ctx, 270), PaintUtil.dp2pixel(ctx, 40));
		}
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow
				.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		// 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		
		popupWindow.showAtLocation(parentView,
				Gravity.LEFT | Gravity.TOP, x, y);
	}
}
