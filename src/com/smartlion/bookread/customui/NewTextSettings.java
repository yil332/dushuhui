package com.smartlion.bookread.customui;

import android.app.Activity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;

import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.common.PageViewAdapter;
import com.smartlion.bookread.inf.BookReadInterface;
import com.smartlion.bookread.inf.BookReadProxy;
import com.whalefin.dushuhui.R;



public class NewTextSettings {

	private View				textSettingContentView	= null;

	private Activity			context					= null;
	

	private Button				textSizeOptionSmall;
	private Button				textSizeOptionNormal;
	private Button				textSizeOptionLarge;
	private Button				currentSelectedTextSizeOptionButton;

	private PopupWindow			textSettingPopup		= null;

	private Config				config					= Config.getInstance();

	private View				view;
	
	BookReadProxy proxy =null;

	public NewTextSettings(Activity activity, View view, BookReadInterface inf) {
		this.context = activity;
		this.textSettingContentView = context.getLayoutInflater().inflate(R.layout.booklib_page_txt_quick_setting1, null);
		proxy=new BookReadProxy(inf);
		this.view = view;
		init();
	}


	private void init() {
		textSizeOptionSmall = (Button) textSettingContentView.findViewById(R.id.small_text_size_option);
		textSizeOptionNormal = (Button) textSettingContentView.findViewById(R.id.middle_text_size_option);
		textSizeOptionLarge = (Button) textSettingContentView.findViewById(R.id.large_text_size_option);

		textSizeOptionSmall.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (currentSelectedTextSizeOptionButton != textSizeOptionSmall) {
					if (currentSelectedTextSizeOptionButton != null) {
						currentSelectedTextSizeOptionButton
								.setBackgroundResource(currentSelectedTextSizeOptionButton == textSizeOptionNormal ? R.drawable.booklib_button_bg_middle_default
										: R.drawable.booklib_button_bg_last_default);
					}
					currentSelectedTextSizeOptionButton = textSizeOptionSmall;
					textSizeOptionSmall.setBackgroundResource(R.drawable.booklib_button_bg_first_pressed);
					config.setBookScale(0);
					proxy.doChangeFontSizeIndex();
					dismissPopupWindows();
				}
			}
		});

		textSizeOptionNormal.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (currentSelectedTextSizeOptionButton != textSizeOptionNormal) {
					if (currentSelectedTextSizeOptionButton != null) {
						currentSelectedTextSizeOptionButton
								.setBackgroundResource(currentSelectedTextSizeOptionButton == textSizeOptionSmall ? R.drawable.booklib_button_bg_first_default
										: R.drawable.booklib_button_bg_last_default);
					}
					currentSelectedTextSizeOptionButton = textSizeOptionNormal;
					textSizeOptionNormal.setBackgroundResource(R.drawable.booklib_button_bg_middle_pressed);
					config.setBookScale(1);
					proxy.doChangeFontSizeIndex();
					dismissPopupWindows();
				}
			}
		});

		textSizeOptionLarge.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (currentSelectedTextSizeOptionButton != textSizeOptionLarge) {
					if (currentSelectedTextSizeOptionButton != null) {
						currentSelectedTextSizeOptionButton
								.setBackgroundResource(currentSelectedTextSizeOptionButton == textSizeOptionSmall ? R.drawable.booklib_button_bg_first_default
										: R.drawable.booklib_button_bg_middle_default);
					}
					currentSelectedTextSizeOptionButton = textSizeOptionLarge;
					textSizeOptionLarge.setBackgroundResource(R.drawable.booklib_button_bg_last_pressed);
					config.setBookScale(2);
					proxy.doChangeFontSizeIndex();
					dismissPopupWindows();
				}
			}
		});

		final SeekBar adjustBrightNessViewSeekBar = (SeekBar) textSettingContentView.findViewById(R.id.brightness);
		adjustBrightNessViewSeekBar.setProgress(50);
		adjustBrightNessViewSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				setScreenBrightness(progress);
				config.setWindowBrightness(progress);
			}
		});
		
		
		
	}
	
	public void setBookProgressSeekBarProgress(int progress,int max,final ViewPager mViewPager){
		final TextView bookPageNum =(TextView) textSettingContentView.findViewById(R.id.bookPageNum);
		bookPageNum.setText(progress+"/"+(max-1));
		
		SeekBar bookProgressSeekBar = (SeekBar) textSettingContentView.findViewById(R.id.bookProgress);
		
		bookProgressSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				mViewPager.setCurrentItem(seekBar.getProgress()+PageViewAdapter.centerPageOffset,false);
				bookPageNum.setText(seekBar.getProgress()+"/"+seekBar.getMax());
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				bookPageNum.setText(progress+"/"+seekBar.getMax());
			}
		});
		
		bookProgressSeekBar.setMax(max-1);
		bookProgressSeekBar.setProgress(progress);
	}

	private void dismissPopupWindows() {
		if (textSettingPopup != null && textSettingPopup.isShowing()) {
			textSettingPopup.dismiss();
		}
	}

	public void show() {

		dismissPopupWindows();

		if (config.getBookScale() == 0) {
			textSizeOptionSmall.setBackgroundResource(R.drawable.booklib_button_bg_first_pressed);
			currentSelectedTextSizeOptionButton = textSizeOptionSmall;
		}
		if (config.getBookScale() == 1) {
			textSizeOptionNormal.setBackgroundResource(R.drawable.booklib_button_bg_middle_pressed);
			currentSelectedTextSizeOptionButton = textSizeOptionNormal;
		}
		if (config.getBookScale() == 2) {
			textSizeOptionLarge.setBackgroundResource(R.drawable.booklib_button_bg_last_pressed);
			currentSelectedTextSizeOptionButton = textSizeOptionLarge;
		}

		if (textSettingPopup == null) {
			@SuppressWarnings("deprecation")
			int width = context.getWindowManager().getDefaultDisplay().getWidth();
			textSettingPopup = new PopupWindow(textSettingContentView, width, width / 3*2, true);
			textSettingPopup.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.booklib_popup));
			textSettingPopup.setAnimationStyle(R.style.PopupMenuAnimation);
			textSettingPopup.setClippingEnabled(false);
		}
		textSettingPopup.showAtLocation(view, 81, 0, 40);
	}

	public final void setScreenBrightness(int percent) {
		if (percent < 1) {
			percent = 1;
		} else if (percent > 100) {
			percent = 100;
		}
		final WindowManager.LayoutParams attrs = context.getWindow().getAttributes();
		attrs.screenBrightness = percent / 100.0f;
		context.getWindow().setAttributes(attrs);
	}

	public void disableTxtButton() {
		textSizeOptionSmall.setEnabled(false);
		textSizeOptionNormal.setEnabled(false);
		textSizeOptionLarge.setEnabled(false);
	}

	public void enableTxtButton() {
		textSizeOptionSmall.setEnabled(true);
		textSizeOptionNormal.setEnabled(true);
		textSizeOptionLarge.setEnabled(true);
	}

	

}
