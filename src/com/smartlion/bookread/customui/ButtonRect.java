package com.smartlion.bookread.customui;

import android.graphics.RectF;

public class ButtonRect {
	public static final int	TYPE_FOOTNOTE	= 0;
	public static final int	TYPE_IMAGE		= 1;
	public static final int	TYPE_CHAR		= 1;
	private RectF			rect;
	private int				type;
	private String			content;
	public Integer			charIndex;
	public boolean			isSelected=false;
	public float fontSize;
	public int startIndex;

	public RectF getRect() {
		return rect;
	}

	public void setRect(RectF rect) {
		this.rect = rect;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public ButtonRect(RectF rectF, String content, int type) {
		super();
		this.rect = rectF;
		this.content = content;
		this.type = type;
	}
	
	public ButtonRect(RectF rectF, String content, int type,int charIndex) {
		super();
		this.rect = rectF;
		this.content = content;
		this.type = type;
		this.charIndex=charIndex;
	}
	
	public ButtonRect(RectF rectF, String content, int type,int charIndex,float fontSize) {
		super();
		this.rect = rectF;
		this.content = content;
		this.type = type;
		this.charIndex=charIndex;
		this.fontSize=fontSize;
	}
}