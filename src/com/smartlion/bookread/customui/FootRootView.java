package com.smartlion.bookread.customui;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartlion.bookread.common.Config;
import com.smartlion.bookread.common.PaintUtil;
import com.whalefin.dushuhui.R;


/**
 * 显示脚注
 * @author wenjiexue
 *
 */
public class FootRootView {
	private Context ctx;
	
	private String str;
	
	public FootRootView(Context ctx) {
		this.ctx=ctx;
	}
	
	private PopupWindow popupWindow = null;
	
	@SuppressWarnings("deprecation")
	public void Show(int x, int y,String content,View parentView){
		str=content;
		int showWidth=PaintUtil.dp2pixel(ctx, 100);
		WindowManager windowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
		if (popupWindow == null) {
			LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			
			if (Config.getInstance().getWindowHeight() / 2 <= y){
				y=y-showWidth-20;
				View view = layoutInflater
						.inflate(R.layout.booklib_footroot_down, null);
				TextView footnote_down=(TextView)view.findViewById(R.id.footnote_down);
				footnote_down.setText(str);
				footnote_down.setMovementMethod(ScrollingMovementMethod.getInstance());
				ImageView downArrow = (ImageView) view.findViewById(R.id.imageview_head_down);
				downArrow.setVisibility(View.VISIBLE);
				RelativeLayout.LayoutParams arrowPosition = (RelativeLayout.LayoutParams) downArrow
				.getLayoutParams();
				arrowPosition.leftMargin = x;
				downArrow.setLayoutParams(arrowPosition);
				popupWindow = new PopupWindow(view, windowManager.getDefaultDisplay().getWidth(), showWidth);
			}else{
				View view = layoutInflater
						.inflate(R.layout.booklib_footroot_up, null);
				TextView footnote_down=(TextView)view.findViewById(R.id.footnote_up);
				footnote_down.setText(str);
				footnote_down.setMovementMethod(ScrollingMovementMethod.getInstance());
				ImageView upArrow = (ImageView) view.findViewById(R.id.imageview_head_up);
				upArrow.setVisibility(View.VISIBLE);
				RelativeLayout.LayoutParams arrowPosition = (RelativeLayout.LayoutParams) upArrow
						.getLayoutParams();
				arrowPosition.leftMargin = x;
				upArrow.setLayoutParams(arrowPosition);
				popupWindow = new PopupWindow(view, windowManager.getDefaultDisplay().getWidth(), showWidth);
			}
			
			
		}
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow
				.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		// 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		
		
		popupWindow.showAtLocation(parentView,
				Gravity.LEFT | Gravity.TOP, x, y);
	}
}
