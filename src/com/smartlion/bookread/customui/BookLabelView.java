package com.smartlion.bookread.customui;

import com.smartlion.bookread.BookReadActivity;
import com.smartlion.bookread.common.MyViewPager;
import com.smartlion.bookread.common.PageViewAdapter;
import com.smartlion.bookread.inf.BookReadInterface;
import com.smartlion.bookread.inf.BookReadProxy;
import com.smartlion.bookread.inf.FragmentInterface;
import com.smartlion.bookread.inf.FragmentProxy;
import com.smartlion.bookread.inf.ProgressViewInterface;
import com.smartlion.bookread.inf.ProgressViewProxy;
import com.smartlion.bookread.model.BookLabelInfo;
import com.smartlion.bookread.model.BookLabelInfos;
import com.smartlion.bookread.model.PageShowInfo;
import com.smartlion.bookread.parserutil.BookParserThread;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Vibrator;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;


@SuppressLint("ViewConstructor")
public class BookLabelView extends View {
	ProgressViewProxy mProgressViewProxy;

	// Path path;
	BookReadingPageView page;

	// public boolean isMove=false;
	private PointF downPoint;
	private long currentMS;

	// 0pause 1上下移动 2左右移动
	public int moveStatus = -1;

	public PageShowInfo mPageShowInfo = null;

	public int pageno = -1;

	private Context mContext;

	private BookReadProxy proxy;

	ButtonRect start = null;
	ButtonRect end = null;

	ButtonRect start1 = null;
	ButtonRect end1 = null;

	FragmentProxy mFragmentProxy;

	boolean isShowMarker;
	
	float winHeight=0,windowWidth;

	public BookLabelView(Context context, BookReadingPageView page,
			FragmentInterface inf, boolean isShowMarker,ProgressViewInterface mProgressViewInterface) {
		super(context);
		mContext = context;
		proxy = new BookReadProxy((BookReadInterface) mContext);
		mFragmentProxy = new FragmentProxy(inf);
		this.page = page;
		this.isShowMarker = isShowMarker;
		this.mProgressViewProxy=new ProgressViewProxy(mProgressViewInterface);
		Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();  
		winHeight=display.getHeight();
		windowWidth = display.getWidth();
		
	}

	public BookLabelView(Context context, AttributeSet attrs,
			BookReadingPageView page, FragmentInterface inf,
			boolean isShowMarker,ProgressViewInterface mProgressViewInterface) {
		super(context, attrs);
		mContext = context;
		proxy = new BookReadProxy((BookReadInterface) mContext);
		mFragmentProxy = new FragmentProxy(inf);
		this.page = page;
		this.isShowMarker = isShowMarker;
		this.mProgressViewProxy=new ProgressViewProxy(mProgressViewInterface);
		Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();  
		winHeight=display.getHeight();
		windowWidth = display.getWidth();
		
	}

	FragmentPagerAdapter mFragmentPagerAdapter;

	public void setBook(PageShowInfo mPageShowInfo, int pageno,
			BookParserThread mBookParserThread) {
		this.mPageShowInfo = mPageShowInfo;
		this.pageno = pageno;
	}

	public boolean isEditStatus = false;

	public long preMoveTime = -1;

	public PointF preMovePoint = null;

	public boolean firstMoveY = false;

	public boolean secondMoveY = false;

	// public boolean ifScrollProgress=false;

	public boolean isInBound;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		try {
			boundRect=mProgressViewProxy.getBoundRect();
			circleRect=mProgressViewProxy.getCircleRect();
			
//			&&(isContains(boundRect, event.getX(), event.getY())||isContains(circleRect, event.getX(), event.getY()))
			if (((BookReadActivity) mContext).isShowDlg||(((BookReadActivity) mContext).isShowReaderHub)) {
				return touchForInReadHub(event);
			}else {
				if (isEditStatus) {
					return touchForInEditStatus(event);
				} else {
					return touchForInSimple(event);
				}
			}
		} catch (Exception e) {
		}

		return false;
	}
	
	boolean isInCircle=false;
	long downTime;
	float currenty;
	RectF circleRect;
	RectF boundRect;
	
	public boolean isContains(RectF rectF,float x,float y){
		if (rectF.left<=x&&rectF.right>=x&&rectF.top<=y&&rectF.bottom>=y) {
			return true;
		}
		return false;
	}

	public boolean touchForInReadHub(MotionEvent event) {
		if (isContains(boundRect, event.getX(), event.getY())||isContains(circleRect, event.getX(), event.getY())) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				MyViewPager.isCanScroll = true;
				
				if (isContains(circleRect,event.getX(), event.getY())&&!((BookReadActivity)mContext).isShowDlg) {
					
					isInCircle=true;
					currenty=event.getY();
					mProgressViewProxy.setCurrenty(currenty);
					circleRect=mProgressViewProxy.getCircleRect();
					circleRect.top=currenty-mProgressViewProxy.getCircleWidth();
					circleRect.bottom=currenty+mProgressViewProxy.getCircleWidth();
					mProgressViewProxy.setCircleRect(circleRect);
					mProgressViewProxy.setProgress((int)((currenty-mProgressViewProxy.getStartHeight())/(mProgressViewProxy.getEndHeight()-mProgressViewProxy.getStartHeight())*mProgressViewProxy.getMax()));
					mProgressViewProxy.toInvalidate();
					
					
					return true;
				}
				else if (isContains(boundRect, event.getX(), event.getY())) {
					((BookReadActivity)mContext).mProgressView1.setVisibility(View.GONE);
					downTime=System.currentTimeMillis();
					isInBound=true;
					return true;
				}else if (((BookReadActivity)mContext).isShowDlg) {
					return true;
				}
				
			case MotionEvent.ACTION_MOVE:
				
				if (isInCircle) {
					if (event.getY()>mProgressViewProxy.getStartHeight()&&event.getY()<mProgressViewProxy.getEndHeight()) {
						currenty = event.getY();
						mProgressViewProxy.setCurrenty(currenty);
						circleRect=mProgressViewProxy.getCircleRect();
						circleRect.top=currenty-mProgressViewProxy.getCircleWidth();
						circleRect.bottom=currenty+mProgressViewProxy.getCircleWidth();
						mProgressViewProxy.setCircleRect(circleRect);
						mProgressViewProxy.setProgress((int)((currenty-mProgressViewProxy.getStartHeight())/(mProgressViewProxy.getEndHeight()-mProgressViewProxy.getStartHeight())*mProgressViewProxy.getMax()));
						mProgressViewProxy.toInvalidate();
					}else if(event.getY()<mProgressViewProxy.getStartHeight()){
						currenty = mProgressViewProxy.getStartHeight();
						mProgressViewProxy.setCurrenty(currenty);
						circleRect=mProgressViewProxy.getCircleRect();
						circleRect.top=currenty-mProgressViewProxy.getCircleWidth();
						circleRect.bottom=currenty+mProgressViewProxy.getCircleWidth();
						mProgressViewProxy.setCircleRect(circleRect);
						mProgressViewProxy.setProgress(0);
						mProgressViewProxy.toInvalidate();
					}else{
						currenty = mProgressViewProxy.getEndHeight();
						mProgressViewProxy.setCurrenty(currenty);
						circleRect=mProgressViewProxy.getCircleRect();
						circleRect.top=currenty-mProgressViewProxy.getCircleWidth();
						circleRect.bottom=currenty+mProgressViewProxy.getCircleWidth();
						mProgressViewProxy.setCircleRect(circleRect);
						mProgressViewProxy.setProgress(mProgressViewProxy.getMax()-1);
						mProgressViewProxy.toInvalidate();
					}
					return true;
				}
				else if (isInBound) {
					boundRect=mProgressViewProxy.getBoundRect();
					boundRect.left=event.getX()-mProgressViewProxy.getCircleWidth();
					boundRect.right=event.getX()+mProgressViewProxy.getCircleWidth();
					mProgressViewProxy.setBoundRect(boundRect);
					if (event.getX()<windowWidth*9/10) {
						proxy.moveView(event.getX());
					}
					
					
					return true;
				}
				
				
			case MotionEvent.ACTION_UP:
			default:
				
				mProgressViewProxy.disShowWind();
				if (isInCircle) {
					isInCircle=false;
					proxy.changeViewPageItem(mProgressViewProxy.getProgress()+PageViewAdapter.centerPageOffset);
					((BookReadActivity)mContext).mProgressView.setProgerss(mProgressViewProxy.getProgress(), mProgressViewProxy.getMax(), true);
					return true;
				}else if (isInBound) {
					isInBound=false;
					
					if ((System.currentTimeMillis()-downTime)<100) {
						if (((BookReadActivity)mContext).isShowDlg) {
							((BookReadActivity)mContext).mProgressView1.setVisibility(View.VISIBLE);
							((BookReadActivity)mContext).mProgressView.changeDirection(0);
							proxy.moveView(0);
							((BookReadActivity)mContext).isShowDlg=false;
							proxy.unSetAdapter();
							boundRect=mProgressViewProxy.getBoundRect();
							boundRect.left=-mProgressViewProxy.getCircleWidth();
							boundRect.right=mProgressViewProxy.getCircleWidth();
							mProgressViewProxy.setBoundRect(boundRect);
						}else{
							((BookReadActivity)mContext).mProgressView.changeDirection(1);
							proxy.moveView(windowWidth*9/10);
							((BookReadActivity)mContext).isShowDlg=true; 
							proxy.setAdapter();
							boundRect=mProgressViewProxy.getBoundRect();
							boundRect.left=windowWidth*9/10-mProgressViewProxy.getCircleWidth();
							boundRect.right=windowWidth*9/10+mProgressViewProxy.getCircleWidth();
							mProgressViewProxy.setBoundRect(boundRect);
						}
					}
					else{
						if (event.getX()>windowWidth*2/3) {
							((BookReadActivity)mContext).mProgressView.changeDirection(1);
							proxy.moveView(windowWidth*9/10);
							((BookReadActivity)mContext).isShowDlg=true; 
							proxy.setAdapter();
							boundRect=mProgressViewProxy.getBoundRect();
							boundRect.left=windowWidth*9/10-mProgressViewProxy.getCircleWidth();
							boundRect.right=windowWidth*9/10+mProgressViewProxy.getCircleWidth();
							mProgressViewProxy.setBoundRect(boundRect);
						}else{
							((BookReadActivity)mContext).mProgressView.changeDirection(0);
							((BookReadActivity)mContext).mProgressView1.setVisibility(View.VISIBLE);
							proxy.moveView(0);
							((BookReadActivity)mContext).isShowDlg=false;
							proxy.unSetAdapter();
							boundRect=mProgressViewProxy.getBoundRect();
							boundRect.left=-mProgressViewProxy.getCircleWidth();
							boundRect.right=mProgressViewProxy.getCircleWidth();
							mProgressViewProxy.setBoundRect(boundRect);
						}
					}
					
					MyViewPager.isCanScroll = false;
					return true;
				}
				
			}
		}else{
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (((BookReadActivity) mContext).isShowDlg) {
					MyViewPager.isCanScroll = true;
					return true;
				}
				return false;
			case MotionEvent.ACTION_UP:
				mProgressViewProxy.disShowWind();
				if (isInBound) {
					isInBound=false;
					
					if ((System.currentTimeMillis()-downTime)<100) {
						if (((BookReadActivity)mContext).isShowDlg) {
							((BookReadActivity)mContext).mProgressView.changeDirection(0);
							((BookReadActivity)mContext).mProgressView1.setVisibility(View.VISIBLE);
							proxy.moveView(0);
							((BookReadActivity)mContext).isShowDlg=false;
							proxy.unSetAdapter();
							boundRect=mProgressViewProxy.getBoundRect();
							boundRect.left=-mProgressViewProxy.getCircleWidth();
							boundRect.right=mProgressViewProxy.getCircleWidth();
							mProgressViewProxy.setBoundRect(boundRect);
						}else{
							((BookReadActivity)mContext).mProgressView.changeDirection(1);
							proxy.moveView(windowWidth*9/10);
							((BookReadActivity)mContext).isShowDlg=true; 
							proxy.setAdapter();
							boundRect=mProgressViewProxy.getBoundRect();
							boundRect.left=windowWidth*9/10-mProgressViewProxy.getCircleWidth();
							boundRect.right=windowWidth*9/10+mProgressViewProxy.getCircleWidth();
							mProgressViewProxy.setBoundRect(boundRect);
						}
					}
					else{
						if (event.getX()>windowWidth*2/3) {
							((BookReadActivity)mContext).mProgressView.changeDirection(1);
							proxy.moveView(windowWidth*9/10);
							((BookReadActivity)mContext).isShowDlg=true; 
							proxy.setAdapter();
							boundRect=mProgressViewProxy.getBoundRect();
							boundRect.left=windowWidth*9/10-mProgressViewProxy.getCircleWidth();
							boundRect.right=windowWidth*9/10+mProgressViewProxy.getCircleWidth();
							mProgressViewProxy.setBoundRect(boundRect);
						}else{
							((BookReadActivity)mContext).mProgressView.changeDirection(0);
							((BookReadActivity)mContext).mProgressView1.setVisibility(View.VISIBLE);
							proxy.moveView(0);
							((BookReadActivity)mContext).isShowDlg=false;
							proxy.unSetAdapter();
							boundRect=mProgressViewProxy.getBoundRect();
							boundRect.left=-mProgressViewProxy.getCircleWidth();
							boundRect.right=mProgressViewProxy.getCircleWidth();
							mProgressViewProxy.setBoundRect(boundRect);
						}
					}
					MyViewPager.isCanScroll = false;
				}else if (!((BookReadActivity) mContext).isShowDlg) {
					proxy.doAfterTouchUp(event, true);
				}
				return true;
			}
		}
		
		return false;
	}

	public boolean touchForInEditStatus(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			break;

		case MotionEvent.ACTION_MOVE:

			long nowMS = System.currentTimeMillis();// long currentMS
													// 获取系统时间
			PointF nowMovePoint = new PointF(event.getX(), event.getY());
			if (nowMS - preMoveTime > 100) {
				if (preMovePoint == null
						|| Math.abs(preMovePoint.x - nowMovePoint.x) > 20.0f
						|| Math.abs(preMovePoint.y - nowMovePoint.y) > 20.0f) {
					preMoveTime = nowMS;
					if (start == null) {
						start = page.InCharRects(event.getX(), event.getY());
					}
					end = page.InCharRects(event.getX(), event.getY());
					if (start != null && end != null) {
						if (start != start1 || end != end1) {
							start1 = start;
							end1 = end;
							if (end.charIndex > start.charIndex) {
								for (int i = 0; i < page.charRects.size(); i++) {
									page.charRects.get(i).isSelected = false;
									
									if (page.charRects.get(i).charIndex >= start.charIndex
											&& page.charRects.get(i).charIndex <= end.charIndex) {
										page.charRects.get(i).isSelected = true;
									}
								}
							} else {
								for (int i = 0; i < page.charRects.size(); i++) {
									page.charRects.get(i).isSelected = false;
									if (page.charRects.get(i).charIndex <= start.charIndex
											&& page.charRects.get(i).charIndex >= end.charIndex) {
										page.charRects.get(i).isSelected = true;
									}
								}
							}
							page.toDraw();
						}
					}
				}

			}

			break;

		case MotionEvent.ACTION_UP:
		default:
			MyViewPager.isCanScroll = false;
			String content = "";

			BookLabelInfos mBookLabelInfos = ((BookReadActivity) mContext).mLabelCacheUtil.mBookLabelInfoMap
					.get(page.pg.chapterID);
			// 保存路径
			for (int i = 0; i < page.charRects.size(); i++) {
				if (page.charRects.get(i).isSelected) {
					page.charRects.get(i).isSelected = false;
					content = content + page.charRects.get(i).getContent();
					boolean isExist = false;
					for (int j = 0; j < mBookLabelInfos.mBookLabelInfoList
							.size(); j++) {
						if (mBookLabelInfos.mBookLabelInfoList.get(j).labelStartIndex
								.equals(page.charRects.get(i).charIndex)) {
							isExist = true;
						}
					}
					if (!isExist) {
						BookLabelInfo mBookLabelInfo = new BookLabelInfo();
						mBookLabelInfo.labelContent = page.charRects.get(i)
								.getContent();
						mBookLabelInfo.labelStartIndex = page.charRects.get(i).charIndex;

						mBookLabelInfos.mBookLabelInfoList.add(mBookLabelInfo);
					}
				}
			}
			proxy.showCheckIsLabelPopWindows((int) event.getX(),
					(int) event.getY(), content, mBookLabelInfos,
					(int) page.pg.chapterID, page);

			isEditStatus = false;
			break;
		}
		return true;
	}

	public boolean touchForInSimple(MotionEvent event) {
		MotionEvent downevent = null;

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:

			downevent = event;
			firstMoveY = true;
			secondMoveY = true;
			// isMove=false;
			moveStatus = -1;
			downPoint = new PointF(event.getX(), event.getY());// float
																// DownX
			currentMS = System.currentTimeMillis();// long currentMS
													// 获取系统时间
			start = page.InCharRects(event.getX(), event.getY());
			return true;
		case MotionEvent.ACTION_MOVE:

			PointF movePoint = new PointF(event.getX(), event.getY());
			float moveX = Math.abs(downPoint.x - movePoint.x);// X轴距离
			float moveY = Math.abs(downPoint.y - movePoint.y);// X轴距离

			long moveTime = System.currentTimeMillis() - currentMS;// 移动时间
			if (moveStatus == -1) {
				if (moveTime > 1000&&!((BookReadActivity) mContext).isShowReaderHub) {
					MyViewPager.isCanScroll = true;
					// ((BookReadActivity)mContext).mViewPager.setScanScroll(false);
					moveStatus = 0;
					isEditStatus = true;
					Vibrator vibrator = (Vibrator) mContext
							.getSystemService(Context.VIBRATOR_SERVICE);
					long[] pattern = { 0, 200 }; // 停止 开启 停止 开启
					vibrator.vibrate(pattern, -1); // 重复两次上面的pattern

					// 如果只想震动一次，index设为-1

				}
			}
			if (moveStatus == -1) {
				if (moveX > 30 && moveX > moveY) {
					isEditStatus = false;
					moveStatus = 2;
					super.onTouchEvent(downevent);
				}
			}

			if (moveStatus == -1) {
				if (moveY > 30) {
					MyViewPager.isCanScroll = true;
					isEditStatus = false;
					moveStatus = 1;

				}
			}
			if (moveStatus == 1) {
				if ((downPoint.y - movePoint.y) < 0) {
					if (moveY > 400 && firstMoveY) {
						firstMoveY = false;
						if (isShowMarker) {
							mFragmentProxy.showImageViewMarkDisabled(true);
						} else
							mFragmentProxy.showBookMarkImage(true);
					}
					page.scrollTo(0, (int) (downPoint.y - movePoint.y) / 2);

				}

			}

			return true;
		case MotionEvent.ACTION_UP:

			MyViewPager.isCanScroll = false;
			if (moveStatus == 1) {
				if (downPoint.y - event.getY() < -400) {
					String content="";
					for (int i = 0; i < page.pg.mPageCharInfos.mPageCharInfoList.size(); i++) {
						if (i==5) {
							break;
						}
						content=content+page.pg.mPageCharInfos.mPageCharInfoList
								.get(i).content;
					}
					mFragmentProxy
							.showBookMarkImage(
									(int) page.pg.chapterID,
									(int) page.pg.mPageCharInfos.mPageCharInfoList
											.get(0).textInfoIndex,
									(int) (page.pg.chapterCharCount - page.pg.pageCharCount),
									content);
					isShowMarker = !isShowMarker;
				}
				page.scrollTo(0, 0);
				return true;
			} else {

				ButtonRect result = page.InRects(event.getX(), event.getY());
				if (result != null) {
					switch (result.getType()) {

					case ButtonRect.TYPE_FOOTNOTE:
						proxy.showFootNote(result.getContent(),
								result.getRect());
						break;
					case ButtonRect.TYPE_IMAGE:
						proxy.showImage(result.getContent());
						break;
					}

					return true;
				}
				ButtonRect pathResult = page.InPathRects(event.getX(),
						event.getY());
				if (pathResult != null) {
					String content = "";
					for (int i = 0; i < page.charRects.size(); i++) {
						if (page.charRects.get(i).startIndex == pathResult.startIndex) {
							content = content
									+ page.charRects.get(i).getContent();
						}
					}
					proxy.showEditLabelPopWindows(
							(int) pathResult.getRect().left,
							(int) pathResult.getRect().bottom, content,
							pathResult.startIndex, (int) page.pg.chapterID,
							page);

					return true;
				}

				if (moveStatus == -1) {
					proxy.doAfterTouchUp(event, true);
				}

			}

			break;
		
		}
		return false;
	}

}
