package com.smartlion.bookread.customui;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;

import com.smartlion.bookread.BookReadActivity;
import com.smartlion.bookread.common.PaintUtil;
import com.smartlion.bookread.model.BookLabelInfos;
import com.smartlion.bookread.parserutil.LabelCacheUtil;
import com.whalefin.dushuhui.R;


/**
 * 显示标记确定框
 * @author wenjiexue
 *
 */
public class CheckIsLabelView {
	private Context ctx;
	
	private String str;
	private BookReadingPageView labelPage;
	BookLabelInfos mSaveBookLabelInfos;
	private LabelCacheUtil mSaveLabelCacheUtil;
	long saveLabelContentID;
	
	PopupWindow popupWindow1 = null;
	
	public CheckIsLabelView(Context ctx) {
		this.ctx=ctx;
	}
	
	@SuppressWarnings("deprecation")
	public void Show(int x, int y, String content,
			BookLabelInfos mBookLabelInfos, long labelContentID,
			BookReadingPageView page,LabelCacheUtil mLabelCacheUtil,View parentView){
		str = content;
		mSaveBookLabelInfos=mBookLabelInfos;
		saveLabelContentID = labelContentID;
		mSaveLabelCacheUtil=mLabelCacheUtil;
		labelPage = page;
		
		if (popupWindow1 == null) {
			LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = layoutInflater
					.inflate(R.layout.booklib_editlabel, null);
			Button Button01 = (Button) view.findViewById(R.id.Button01);
			Button Button02 = (Button) view.findViewById(R.id.Button02);
			Button Button03 = (Button) view.findViewById(R.id.Button03);
			Button01.setText("标注");
			Button01.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						mSaveLabelCacheUtil
						.saveLabelList(saveLabelContentID, mSaveBookLabelInfos);
					} catch (IOException e) {
					}
					labelPage.toDrawPath();
					popupWindow1.dismiss();
				}
			});

			Button02.setOnClickListener(new OnClickListener() {

				@SuppressLint("NewApi")
				@Override
				public void onClick(View v) {
					ClipboardManager clip = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
					clip.setText(str); // 复制
					popupWindow1.dismiss();
				}
			});

			Button03.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					BookReadActivity.mBookLibProxy.doShare();
					popupWindow1.dismiss();
				}
			});
			popupWindow1 = new PopupWindow(view, PaintUtil.dp2pixel(ctx, 270), PaintUtil.dp2pixel(ctx, 40));
		}
		
		popupWindow1.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss() {
				labelPage.toDraw();
			}
		});
		
		popupWindow1.setFocusable(true);
		popupWindow1.setOutsideTouchable(true);
		popupWindow1
				.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		// 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景
		popupWindow1.setBackgroundDrawable(new BitmapDrawable());
		popupWindow1.showAtLocation(parentView,
				Gravity.LEFT | Gravity.TOP, x, y);
	}
}
